
# number of events to be processed, -1 for all
events = -1 
#events = 100

# inputs path/name, ['file1', 'file2',..] 
lists = "test.list"
f = open( lists, 'r')
inputList=f.read().splitlines()

#inputs = ['/gpfs/fs7001/ktaniguc/outputfile/test_trigT1/202010121552/_gpfs_fs7001_ktaniguc_HITS_mc16_13TeV.300901.ParticleGunEvtGen_Jpsi_mu3p5mu3p5_prompt.simul.HITS.e7406_e5984_a875_HITS.17636029._000001.pool.root.1/processing/ESD.pool.root']
#inputs = ['/gpfs/fs7001/ktaniguc/ESD/fromSiomi/user.shiomi.19931576.EXT1._000010.ESD.pool.root']
#inputs = ['/gpfs/fs7001/ktaniguc/ESD/fromSiomi/user.shiomi.20861007.EXT0._000010.ESD.pool.root']
#inputs = ['/gpfs/fs7001/ktaniguc/data/ESD/data18_13TeV.00349335.physics_EnhancedBias.recon.ESD.r10795_r10792/data18_13TeV/ESD.15316519._002185.pool.root.1']
#inputs = ['/gpfs/fs7001/youhei/L2MuonSA/dataset_aod_official/data17_13TeV.00325713.physics_Main.merge.AOD.r10260_p3399/AOD.13196434._000486.pool.root.1']
#inputs = ['data18_13TeV.00348154.physics_Main.merge.DESDM_TILEMU.f920_m1951._0001.1']


#output path/name
output = 'L1TGCNtuple.close-by.pool.root'

#-------------------------------------------------------------
# misc.
#-------------------------------------------------------------

from AthenaCommon.AthenaCommonFlags import jobproperties as jp
jp.AthenaCommonFlags.EvtMax = tkEvtMax if 'tkEvtMax' in locals() else events

#import glob
#directory = "/home/tomoe/maxi183/SingleMuon/20.1.3.3/ESD/"
#inputs = glob.glob(directory + "*")
jp.AthenaCommonFlags.FilesInput = tkInput if 'tkInput' in locals() else inputList
#jp.AthenaCommonFlags.FilesInput = tkInput if 'tkInput' in locals() else inputs

from AthenaCommon.AppMgr import ServiceMgr
from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
OutputName = tkOutput if 'tkOutput' in locals() else output
ServiceMgr.THistSvc.Output = ["L1TGCNtuple DATAFILE='%s' OPT='RECREATE'" % OutputName]


from RecExConfig.RecFlags import rec
rec.OutputLevel = INFO

from AnalysisExamples.AnalysisFlags import AnalysisFlags

if AnalysisFlags.DoNavigation:
  include("RecExCommon/AllDet_detDescr.py")
  ServiceMgr.EventSelector.BackNavigation = True

#rec.doTrigger.set_Value_and_Lock(False)
from TriggerJobOpts.TriggerFlags import TriggerFlags
TriggerFlags.doTriggerConfigOnly.set_Value_and_Lock(True)

rec.doAOD.set_Value_and_Lock(False)
rec.doCBNT.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doHist.set_Value_and_Lock(False)

#rec.doPerfMon = False
rec.doPerfMon.set_Value_and_Lock(False)
rec.doDetailedPerfMon.set_Value_and_Lock(False)
rec.doSemiDetailedPerfMon.set_Value_and_Lock(False)
from PerfMonComps.PerfMonFlags import jobproperties
jobproperties.PerfMonFlags.doMonitoring.set_Value_and_Lock(False)

#from InDetRecExample.InDetJobProperties import InDetFlags # for x311 
#InDetFlags.useDCS.set_Value_and_Lock(False)

#-------------------------------------------------------------
# user algorithm
#-------------------------------------------------------------
rec.UserAlgs = ["L1TGCNtuple/L1TGCNtuple_options.py"]

include("RecExCommon/RecExCommon_topOptions.py")

# temporary fix, will remove
#from IOVDbSvc.CondDB import conddb
#conddb.addOverride("/Indet/Beampos","IndetBeampos-RUN2-ES1-UPD2-13")
