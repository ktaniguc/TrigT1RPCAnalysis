
#include "makeChain.h"
#include "TSystem.h"
#include "iostream"

using namespace std;

void
process(int nEvent=10000000, string file="")
{
  gSystem->SetIncludePath("-I./Include -I./Include/selection -I./Include/utility -I./Include/histogram");

  TChain* chain = makeChain(file);
  const int entries = chain->GetEntries();
  std::cout << "\n# of events = " << entries << std::endl;

  chain->Process("L1TGCAnalysis.cpp++", "", nEvent);
  std::cout << "done" << std::endl;
}
