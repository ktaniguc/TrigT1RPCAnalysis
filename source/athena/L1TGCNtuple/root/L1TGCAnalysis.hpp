#ifndef D3PDAnalysis_hpp
#define D3PDAnalysis_hpp
/*====================================================================*
 * declarations
 *====================================================================*/
#define NDEBUG
#define _CONST_ const

#include "data.h"

/*--------------------------------------------------------------------*
 * headers
 *--------------------------------------------------------------------*/
#include "TH1F.h"
#include "TH2F.h"
#include "TLorentzVector.h"

#include "physics.h"
#include "tmUtil.hpp"

#include <fstream>
#include <sstream>
#include <iostream>
#include <utility>

#ifndef __GNUC__
#define __attribute__(x)
#endif

/*--------------------------------------------------------------------*
 * constants
 *--------------------------------------------------------------------*/
#include "tmConstant.hpp"
#include "tmTool.hpp"

/*--------------------------------------------------------------------*
 * classes
 *--------------------------------------------------------------------*/


/**
 *  This class implements ...
 */
class L1TGCAnalysis : public physics
{
  public:
    // -----------------------------------------------------------------
    // Constructors and Destructor
    // -----------------------------------------------------------------
    L1TGCAnalysis(TTree* tree __attribute__((unused)) = 0) { };
    virtual ~L1TGCAnalysis() { };

    // -----------------------------------------------------------------
    // interface
    // -----------------------------------------------------------------
    virtual void Begin(TTree *tree);
    virtual void SlaveBegin(TTree *tree);
    virtual Bool_t Process(Long64_t entry);
    virtual void SlaveTerminate();
    virtual void Terminate();

  public:
    // -----------------------------------------------------------------
    // Miscellaneous methods
    // -----------------------------------------------------------------
    void bookHistograms();
    void fillHistograms();
    void dumpHistograms();
#include "selection.h"
#include "utilities.h"

    // -----------------------------------------------------------------
    // attributes
    // -----------------------------------------------------------------
    int eventCounter_;    
    TList* histograms_;

    // -----------------------------------------------------------------
    // histograms
    // -----------------------------------------------------------------
//#include "histogram_fakeRate.h"
//#include "histogram_timing.h"
#include "histogram_phaseScan.h"

    ClassDef(L1TGCAnalysis,0);
};

#endif // D3PDAnalysis_hpp
/* eof */
