#ifndef makeChain_h
#define makeChain_h

#include "TChain.h"


TChain*
makeChain(string file)
{
  std::vector<std::string> list;

  if (file == "") {
    // path to directory containing L1TGCNtuples
    list.push_back("/home/tomoe/maxi183/L1TGCNtuple/group.det-muon.data15_13TeV.00267638.physics_Main.recon.NTUP.1.f598.00-00-10_L1TGCNtuple/");
    list.push_back("/home/tomoe/maxi183/L1TGCNtuple/group.det-muon.data15_13TeV.00267639.physics_Main.recon.NTUP.1.f598.00-00-10_L1TGCNtuple/");
  
  } else {
    list.push_back(file);
  }


  TChain* chain = new TChain("physics");
  //TChain* chain = new TChain("truth");

  for (size_t ii = 0; ii < list.size(); ii++) {
    chain->Add((list.at(ii) + "*.root*").c_str());
  }

  return chain;
}
#endif
// eof
