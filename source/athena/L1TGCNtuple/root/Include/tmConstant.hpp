#ifndef tmConstant_hpp
#define tmConstant_hpp

// constants
const double PI = 3.1415926535897932384;
const double Z_MASS = 91187.6;
const double MUON_MASS = 105.658367;
const double JPSI_MASS = 3096.916;
const double ELECTRON_MASS = 0.510998910;

// unit conversion
const double GeV = 1.e-3;

//trigger
enum {
  L1_mu10 = 0,
  L1_mu11,

  L2_MU18,
  L2_MU18_medium,

  EF_MU18,
  EF_MU18_medium,

  NUM_TRIGGER
};

enum {
  INVALID = -9999,

  ELECTRON = 11,
  MUON = 13
};


#endif // tmConstant_hpp
/* eof */
