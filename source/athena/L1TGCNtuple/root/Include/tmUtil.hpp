#ifndef tmUtil_hpp
#define tmUtil_hpp

#include <stdlib.h>
#include <iostream>

/** a stringize macro; private use */
#define TM_STR_(_x) #_x

/** a stringize macro; public use */
#define TM_STR(_x) TM_STR_(_x)

/** string concatenate macro */
#define TM_CONCATENATE(_x,_y) _x##_y

/** a macro for setter method */
#define TM_SET_VECTOR(_type, _x) \
    private: std::vector<_type> *_x; \
    public:  void TM_CONCATENATE(set_, _x)(_type _x) {this->_x->push_back(_x);}

/** a macro for setter method */
#define TM_SET_STRING_VECTOR(_x) \
    private: std::vector<std::string> *_x; \
    public:  void TM_CONCATENATE(set_, _x)(std::string& _x) {this->x->push_back(_x);}

/** a macro for setter method */
#define TM_SET_SCALER(_type, _x) \
    private: type _x; \
    public:  void TM_CONCATENATE(set_, _x)(_type _x) {this->_x = _x;}

/** a macro to add bracn to TTree */
#define TM_BRANCH_VECTOR(_tree, _name, _x) (_tree->Branch((_name + TM_STR(_##_x)).c_str(), &_x))
#define TM_BRANCH_BOOL(_tree, _name, _x) (_tree->Branch((_name + TM_STR(_##_x)).c_str(), &_x, (_name + TM_STR(_##_x) + "/O").c_str()))
#define TM_BRANCH_INT(_tree, _name, _x) (_tree->Branch((_name + TM_STR(_##_x)).c_str(), &_x, (_name + TM_STR(_##_x) + "/i").c_str()))
#define TM_BRANCH_DOUBLE(_tree, _name, _x) (_tree->Branch((_name + TM_STR(_##_x)).c_str(), &_x, (_name + TM_STR(_##_x) + "/d").c_str()))

/** a macro to define 1d histogram */
#define TM_1DH(_name, _bin, _min, _max)\
  do { \
    TM_CONCATENATE(_name, _) = new TH1F(TM_STR(_name), "", _bin, _min, _max); \
    list->Add(TM_CONCATENATE(_name,_)); \
  } while (0)

#define TM_2DH(_name, _xbin, _xmin, _xmax, _ybin, _ymin, _ymax)\
  do { \
    TM_CONCATENATE(_name, _) = new TH2F(TM_STR(_name), "", \
                                         _xbin, _xmin, _xmax,\
                                         _ybin, _ymin, _ymax);\
    list->Add(TM_CONCATENATE(_name,_));\
  } while (0)


/** a macro that displays message then exits a program */
#define TM_FATAL_ERROR(x) (std::cerr << "fat> " << x << " [" \
                                     << __FILE__ << ":" \
                                     << std::dec << __LINE__ << "]" \
                                     << std::endl, \
                           exit(EXIT_FAILURE))

/** a macro to be used for incomplete implementation */
#define TM_NOT_IMPLEMENTED_ERROR() TM_FATAL_ERROR("not implemented")

/** a macro that displays run-time information message */
#define TM_LOG_INF(x) (std::cout << "inf> " << x << " [" << __FILE__ << ":" << std::dec << __LINE__ << "]\n") 

/** a macro that displays run-time warning message */
#define TM_LOG_WAR(x) (std::cerr << "war> " << x << " [" << __FILE__ << ":" << std::dec << __LINE__ << "]\n")

/** a macro that displays run-time error message */
#define TM_LOG_ERR(x) (std::cerr << "err> " << x << " [" << __FILE__ << ":" << std::dec << __LINE__ << "]" << std::endl)

/** a macro that displays run-time debug message,
    can be disabled by defining NDEBUG macro */
#ifndef NDEBUG
#define TM_LOG_DBG(x) (std::cout << "dbg> " << x  << " [" << __FILE__ \
                                 << ":" << std::dec << __LINE__ << "]\n")
#else
#define TM_LOG_DBG(x) ;
#endif

#endif // tmUtil_hpp
/* eof */
