#ifndef getL1Index_h
#define getL1Index_h

int
getL1Index(int mu_index)
{
  double dr = 9999;
  int l1_index = -1;

  for (int ii = 0; ii < (int)trig_L1_mu_eta->size(); ii++) {

    if (trig_L1_mu_source->at(ii) == 0) continue;

    double l1_eta = trig_L1_mu_eta->at(ii);
    double l1_phi = trig_L1_mu_phi->at(ii);
    double tmp_dr = tmTool::getEtaPhiDistance(l1_eta, l1_phi,
    mu_eta->at(mu_index), mu_phi->at(mu_index));

    if (tmp_dr < dr) {
      dr = tmp_dr;
      l1_index = ii;
    }
  }

  if (dr > 0.1) return -1;
  else return l1_index;
}

#endif
// eof
