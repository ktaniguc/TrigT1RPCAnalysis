#ifndef getBcidDiff_h
#define getBcidDiff_h

int
getBcidDiff(const int index) _CONST_
{
  const int eventBcid = (bcid & 0x7);
  const int muctpiBcid = muctpi_dw_bcid->at(index);
  int diff = muctpiBcid - eventBcid;

  switch (bcid) {
    case 0:
      if (muctpiBcid == 3) {
        diff = -1;
      } else if (muctpiBcid == 2) {
        diff = -2;
      } else if (muctpiBcid == 1) {
        diff = -3;
      } break;
    case 1:
      if (muctpiBcid == 3) {
        diff = -2;
      } else if (muctpiBcid == 2) {
        diff = -3;
      } break;
    case 2:
      if (muctpiBcid == 3) {
        diff = -3;
      } break; 
    default:
      break; 
  }

  if (abs(diff) > 4) {
    diff = (8 - abs(diff)) * ((diff < 0) ? 1 : -1);
  }

  return diff;
}

#endif
// eof
