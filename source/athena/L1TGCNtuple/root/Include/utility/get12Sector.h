#ifndef get12Sector_h
#define get12Sector_h

int
get12Sector(int index)
{
  int phi = TGC_prd_phi->at(index);
  bool isForward = TGC_prd_station->at(index) % 2;

  if (!isForward) {
    
    if (phi == 47 || phi == 48) 
      return 1;
    else
      return (phi + 5) / 4;
  
  } else {
    
    if (phi == 24) 
      return 1;
    else
      return (phi + 2) / 2;
  } 
}

#endif
// eof
