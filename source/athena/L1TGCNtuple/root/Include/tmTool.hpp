#ifndef tmTool_hpp
#define tmTool_hpp
/*====================================================================*
 * declarations
 *====================================================================*/
#include "TLorentzVector.h"

namespace tmTool {
  struct tmEtaPhi {
    double eta;
    double phi;
  };

  struct tmPtEtaPhiM {
    double pt;
    double eta;
    double phi;
    double m;
  };

  static double getDeltaPhi(const double phi1,
                            const double phi2);
  static double getMass(const tmPtEtaPhiM& x1,
                        const tmPtEtaPhiM& x2);
  //static double getEtaPhiDistance(const tmEtaPhi& x1,
  //                                const tmEtaPhi& x2);
  static double getEtaPhiDistance(const double eta1,
                                  const double phi1,
                                  const double eta2,
                                  const double phi2);

  struct Lepton {
    TLorentzVector tlv;
    int index;
    int flavour;
    double mass;
    int parent;
    double sf;
    double sfErr;

    Lepton(const TLorentzVector& tlv_, int index_=-1, int flavour_=-1, double mass_=-1., int parent_=0, double sf_=1., double sfErr_=0.) :
              tlv(tlv_), index(index_), flavour(flavour_), mass(mass_), parent(parent_), sf(sf_), sfErr(sfErr_) {}
  };
};




#endif // tmTool_hpp
/* eof */
