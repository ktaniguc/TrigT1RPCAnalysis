#ifndef histogram_phaseScan_h
#define histogram_phaseScan_h
/*--------------------------------------------------------------------*
 * declare histograms
 *--------------------------------------------------------------------*/

TH1F* phaseScan_prd_[3];                // 0 all 1 strip 2 wire 
TH1F* phaseScan_prd_split_[3];   
TH1F* phaseScan_prd_den_[3];   
TH1F* phaseScan_prd_num_[3][3];   

TH1F* phaseScan_prd_split_M1_[3];   
TH1F* phaseScan_prd_split_M2_[3];   
TH1F* phaseScan_prd_split_M3_[3];   
TH1F* phaseScan_prd_split_EI_[3];   
TH1F* phaseScan_prd_split_FI_[3];   

TH2F* phaseScan_prd_all_M1_;   
TH2F* phaseScan_prd_all_M2_;   
TH2F* phaseScan_prd_all_M3_;   
TH2F* phaseScan_prd_all_EI_;   
TH2F* phaseScan_prd_all_FI_;   
TH2F* phaseScan_prd_next_M1_;   
TH2F* phaseScan_prd_next_M2_;   
TH2F* phaseScan_prd_next_M3_;   
TH2F* phaseScan_prd_next_EI_;   
TH2F* phaseScan_prd_next_FI_;   

/*--------------------------------------------------------------------*
 * book histograms
 *--------------------------------------------------------------------*/
void
bookPhaseScan(TList* list)
{
  for (int ii = 0; ii < 3; ii++) {

    char buf[BUFSIZ];
    char prd[10];

    if (ii == 0) sprintf(prd, "all");
    if (ii == 1) sprintf(prd, "strip");
    if (ii == 2) sprintf(prd, "wire");

    sprintf(buf, "phaseScan_prd_%s", prd);
    phaseScan_prd_[ii] = new TH1F(buf, "", 3, -1.5, 1.5);
    list->Add(phaseScan_prd_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_%s", prd);
    phaseScan_prd_split_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_[ii]);

    sprintf(buf, "phaseScan_prd_den_%s", prd);
    phaseScan_prd_den_[ii] = new TH1F(buf, "", 11, -7.5, 3.5);
    list->Add(phaseScan_prd_den_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_M1_%s", prd);
    phaseScan_prd_split_M1_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_M1_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_M2_%s", prd);
    phaseScan_prd_split_M2_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_M2_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_M3_%s", prd);
    phaseScan_prd_split_M3_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_M3_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_EI_%s", prd);
    phaseScan_prd_split_EI_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_EI_[ii]);
    
    sprintf(buf, "phaseScan_prd_split_FI_%s", prd);
    phaseScan_prd_split_FI_[ii] = new TH1F(buf, "", 5, -2.5, 2.5);
    list->Add(phaseScan_prd_split_FI_[ii]);

    for (int jj = 0; jj < 3; jj++) {
      
      char bc[10];
    
      if (jj == 0) sprintf(bc, "previous");
      if (jj == 1) sprintf(bc, "current");
      if (jj == 2) sprintf(bc, "next");
      
      sprintf(buf, "phaseScan_prd_num_%s_%s", prd, bc);
      phaseScan_prd_num_[ii][jj] = new TH1F(buf, "", 11, -7.5, 3.5);
      list->Add(phaseScan_prd_num_[ii][jj]);
    }
  }
      
  phaseScan_prd_next_M1_ = new TH2F("phaseScan_prd_next_M1", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_next_M1_);
  phaseScan_prd_next_M2_ = new TH2F("phaseScan_prd_next_M2", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_next_M2_);
  phaseScan_prd_next_M3_ = new TH2F("phaseScan_prd_next_M3", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_next_M3_);
  phaseScan_prd_next_EI_ = new TH2F("phaseScan_prd_next_EI", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_next_EI_);
  phaseScan_prd_next_FI_ = new TH2F("phaseScan_prd_next_FI", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_next_FI_);
  
  phaseScan_prd_all_M1_ = new TH2F("phaseScan_prd_all_M1", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_all_M1_);
  phaseScan_prd_all_M2_ = new TH2F("phaseScan_prd_all_M2", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_all_M2_);
  phaseScan_prd_all_M3_ = new TH2F("phaseScan_prd_all_M3", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_all_M3_);
  phaseScan_prd_all_EI_ = new TH2F("phaseScan_prd_all_EI", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_all_EI_);
  phaseScan_prd_all_FI_ = new TH2F("phaseScan_prd_all_FI", "", 200, -15000, 15000, 200, -15000, 15000);
  list->Add(phaseScan_prd_all_FI_);
}

/*--------------------------------------------------------------------*
 * fill histograms
 *--------------------------------------------------------------------*/
void
fillPhaseScan()
{ 
  //if (!(isTriggered("HLT_noalg_L1MU4") || isTriggered("HLT_noalg_L1MU10"))) return;

  std::vector<int> prd_indices;  
  
  // get prds matched to offline muon
  for (int ii = 0; ii < (int)mu_eta->size(); ii++) {

    if (mu_muonType->at(ii) != 0) continue;
    if (mu_author->at(ii) > 2) continue;
    if (mu_pt->at(ii) < 5000) continue;
    if (fabs(mu_eta->at(ii)) < 1.05) continue;

    cout << "mu: " << mu_author->at(ii) << " pt: " << mu_pt->at(ii) << " eta: "<< mu_eta->at(ii) << " phi: " << mu_phi->at(ii) << endl;
    int l1_index = getL1Index(ii);
    //if (l1_index == -1) continue;

    double muon_eta = mu_eta->at(ii);

    for (int jj = 0; jj < ext_mu_ubias_size->at(ii); jj++) {

      double ext_eta = ext_mu_ubias_targetEtaVec->at(ii)[jj];
      double ext_phi = ext_mu_ubias_targetPhiVec->at(ii)[jj];
      double distance = ext_mu_ubias_targetDistanceVec->at(ii)[jj];

      if (distance * ext_eta < -1) continue;
      if (distance * muon_eta < -1) continue;
      if (ext_eta < -3 || ext_phi < -4) continue;

      bool isM1 = (fabs(distance) == 13605.0);
      bool isM2 = (fabs(distance) == 14860.0);
      bool isM3 = (fabs(distance) == 15280.0);
      bool isEI = (fabs(distance) == 7425.0);
      bool isFI = (fabs(distance) == 7030.0);

      if (! (isM1 || isM2 || isM3 || isEI || isFI)) continue;

      for (int kk = 0; kk < (int)TGC_prd_z->size(); kk++) {
        
        int station = TGC_prd_station->at(kk);
        if (isM1) {
          if ((station != 41) and (station != 42)) continue;
        } else if (isM2) {
          if ((station != 43) and (station != 44)) continue;
        } else if (isM3) {
          if ((station != 45) and (station != 46)) continue;
        } else if (isEI) {
          if ((station != 48)) continue;
        } else if (isFI) {
          if ((station != 47)) continue;
        } else continue;

        double prd_x = TGC_prd_x->at(kk);
        double prd_y = TGC_prd_y->at(kk);
        double prd_z = TGC_prd_z->at(kk);

        TVector3 v(prd_x, prd_y, prd_z);
        double prd_eta = v.PseudoRapidity();
        double prd_phi = v.Phi();

        bool isStrip = TGC_prd_isStrip->at(kk);

        double deta = prd_eta - ext_eta;
        double dphi = tmTool::getDeltaPhi(prd_phi, ext_phi);

        /*if (isStrip) {
        cout << "prd_eta: "<< prd_eta << " prd_phi: " << prd_phi << endl;
        cout << "ext_eta: "<< ext_eta << " ext_phi: " << ext_phi << endl;
        cout << "deta: "<< deta << " dphi: " << dphi << endl;
        }*/
        bool matched = false;
        if (isStrip && (fabs(dphi) < 0.05) && (fabs(deta) < 0.35))  matched = true;
        if (!isStrip && (fabs(deta) < 0.05) && (fabs(dphi) < 0.20)) matched = true;
        
        cout << kk << " isStrip: " << isStrip << " deta: " << deta <<  " dphi: " << dphi << endl; 
        if (!matched) continue;

        prd_indices.push_back(kk);
       
      }
    }
  }

  // fill
  for (int ii = 0; ii < (int)prd_indices.size(); ii++) {

    int prd_index = prd_indices.at(ii);
    int prd_bunch = TGC_prd_bunch->at(prd_index);
    int sector    = get12Sector(prd_index); 

    phaseScan_prd_[0]->Fill(prd_bunch -2);
    phaseScan_prd_den_[0]->Fill(getNSec(sector));
    phaseScan_prd_num_[0][prd_bunch-1]->Fill(getNSec(sector));

    if (TGC_prd_isStrip->at(prd_index)) {
      phaseScan_prd_[1]->Fill(prd_bunch -2);
      phaseScan_prd_den_[1]->Fill(getNSec(sector));
      phaseScan_prd_num_[1][prd_bunch-1]->Fill(getNSec(sector));
    
    } else  {
      phaseScan_prd_[2]->Fill(prd_bunch -2);
      phaseScan_prd_den_[2]->Fill(getNSec(sector));
      phaseScan_prd_num_[2][prd_bunch-1]->Fill(getNSec(sector));
    }
  }

  std::vector<int> bunch_types;
  std::vector<int> bunch_indices;
  std::vector<int> pair;
      cout << "---" << endl;
  for (int ii = 0; ii < (int)prd_indices.size(); ii++) {

    int prd_index1 = prd_indices.at(ii);
    int prd_bunch1 = TGC_prd_bunch->at(prd_index1);
    
    double prd_x1 = TGC_prd_x->at(prd_index1);
    double prd_y1 = TGC_prd_y->at(prd_index1);
    double prd_z1 = TGC_prd_z->at(prd_index1);
      cout << prd_index1 << " station: " << TGC_prd_station->at(prd_index1) << " layer: " << TGC_prd_gasGap->at(prd_index1) << " channel: " << TGC_prd_channel->at(prd_index1)<< " bunch: " <<  prd_bunch1 << " isStrip: "<< TGC_prd_isStrip->at(prd_index1) << " x: " << prd_x1 << " y:" <<  prd_y1 << endl;

    int counter = 0; 
    int jj_index = -1;
    for (int jj = 0; jj < (int)prd_indices.size(); jj++) {
        
      if (ii == jj) continue;

      int prd_index2 = prd_indices.at(jj);
      double prd_x2 = TGC_prd_x->at(prd_index2);
      double prd_y2 = TGC_prd_y->at(prd_index2);
      double prd_z2 = TGC_prd_z->at(prd_index2);

      if (prd_x1 != prd_x2) continue;
      if (prd_y1 != prd_y2) continue;
      if (prd_z1 != prd_z2) continue;

      counter++;
      jj_index = jj;
    }

    if (counter == 0) {
      if (prd_bunch1 == 1) {
        bunch_indices.push_back(prd_index1);
        bunch_types.push_back(-2);
      }
      if (prd_bunch1 == 2) {
        bunch_indices.push_back(prd_index1);
        bunch_types.push_back(0);
      }
      if (prd_bunch1 == 3) {
        bool nex = false;
        for (int nn = 0; nn < (int)prd_indices.size(); nn++) {
          int tmp_index2 = prd_indices.at(nn);
          if (ii == nn) continue;
          if (TGC_prd_station->at(prd_index1) != TGC_prd_station->at(tmp_index2)) continue;
          if (TGC_prd_gasGap->at(prd_index1) != TGC_prd_gasGap->at(tmp_index2)) continue;
          if (fabs(TGC_prd_channel->at(prd_index1) - TGC_prd_channel->at(tmp_index2)) != 1) continue;
          if (TGC_prd_bunch->at(tmp_index2) != 2) continue;
          nex = true;
        }
        if (!nex) {
          bunch_indices.push_back(prd_index1);
          cout << "Only 3" << endl;
          bunch_types.push_back(2);
        }


      }
    
    } else if (counter == 1) {
      
      bool dumped = false;
      for (int kk = 0; kk < (int)pair.size(); kk++) {
        if (pair.at(kk) == jj_index) dumped = true;
      }

      if (!dumped) {
        int prd_bunch2 = TGC_prd_bunch->at(prd_indices.at(jj_index));

        if ((prd_bunch1 == 1 && prd_bunch2 == 2) || 
            (prd_bunch1 == 2 && prd_bunch2 == 1)) {
          bunch_types.push_back(-1);
          bunch_indices.push_back(prd_index1);
        } else if ((prd_bunch1 == 2 && prd_bunch2 == 3) || 
            (prd_bunch1 == 3 && prd_bunch2 == 2)) {
          bunch_types.push_back(1);
          bunch_indices.push_back(prd_index1);
        }else 
          cout << "inf> Not sequential hits " 
            <<  prd_bunch1 << " " <<prd_bunch2 << endl;

        pair.push_back(ii);
        pair.push_back(jj_index);
      }

    } else {
      cout << "inf> more than three hits" << endl;
    }
  } 

  for (int ii = 0; ii < (int)bunch_indices.size(); ii++) {

    int prd_index = bunch_indices.at(ii);
    int prd_bunch = bunch_types.at(ii);
    int station   = TGC_prd_station->at(prd_index);

    phaseScan_prd_split_[0]->Fill(prd_bunch);

    if (station == 41 || station == 42) phaseScan_prd_split_M1_[0]->Fill(prd_bunch);
    if (station == 43 || station == 44) phaseScan_prd_split_M2_[0]->Fill(prd_bunch);
    if (station == 45 || station == 46) phaseScan_prd_split_M3_[0]->Fill(prd_bunch);
    if (station == 48) phaseScan_prd_split_EI_[0]->Fill(prd_bunch);
    if (station == 47) phaseScan_prd_split_FI_[0]->Fill(prd_bunch);

    if (station == 41 || station == 42) phaseScan_prd_all_M1_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    if (station == 43 || station == 44) phaseScan_prd_all_M2_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    if (station == 45 || station == 46) phaseScan_prd_all_M3_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    if (station == 48) phaseScan_prd_all_EI_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    if (station == 47) phaseScan_prd_all_FI_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    if (prd_bunch == 2) {
      if (station == 41 || station == 42) phaseScan_prd_next_M1_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
      if (station == 43 || station == 44) phaseScan_prd_next_M2_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
      if (station == 45 || station == 46) phaseScan_prd_next_M3_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
      if (station == 48) phaseScan_prd_next_EI_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
      if (station == 47)phaseScan_prd_next_FI_->Fill(TGC_prd_x->at(prd_index), TGC_prd_y->at(prd_index));
    }


    if (TGC_prd_isStrip->at(prd_index)) {
      phaseScan_prd_split_[1]->Fill(prd_bunch);
      if (station == 41 || station == 42) phaseScan_prd_split_M1_[1]->Fill(prd_bunch);
      if (station == 43 || station == 44) phaseScan_prd_split_M2_[1]->Fill(prd_bunch);
      if (station == 45 || station == 46) phaseScan_prd_split_M3_[1]->Fill(prd_bunch);
      if (station == 48) phaseScan_prd_split_EI_[1]->Fill(prd_bunch);
      if (station == 47) phaseScan_prd_split_FI_[1]->Fill(prd_bunch);
    } else { 
      phaseScan_prd_split_[2]->Fill(prd_bunch);
      if (station == 41 || station == 42) phaseScan_prd_split_M1_[2]->Fill(prd_bunch);
      if (station == 43 || station == 44) phaseScan_prd_split_M2_[2]->Fill(prd_bunch);
      if (station == 45 || station == 46) phaseScan_prd_split_M3_[2]->Fill(prd_bunch);
      if (station == 48) phaseScan_prd_split_EI_[2]->Fill(prd_bunch);
      if (station == 47) phaseScan_prd_split_FI_[2]->Fill(prd_bunch);
    }
  }
}

int 
getNSec(int sector) 
{
  if (sector ==1)  return -7;
  if (sector ==2)  return -6;
  if (sector ==3)  return -5;
  if (sector ==4)  return -4;
  if (sector ==5)  return -4;
  if (sector ==6)  return -3;
  if (sector ==7)  return -2;
  if (sector ==8)  return -1;
  if (sector ==9)  return 0;
  if (sector ==10) return 1;
  if (sector ==11) return 2;
  if (sector ==12) return 3;

  return 100;
}


void
terminatePhaseScan()
{
}
#endif // histogram_phaseScan_h



