#ifndef histogram_timing_h
#define histogram_timing_h
/*--------------------------------------------------------------------*
 * declare histograms
 *--------------------------------------------------------------------*/

TH1F* timing_muctpi_; 
TH1F* timing_coin_sl_; 
TH1F* timing_coin_hi_; 
TH1F* timing_coin_lo_; 

/*--------------------------------------------------------------------*
 * book histograms
 *--------------------------------------------------------------------*/
void
bookTiming(TList* list)
{
  timing_muctpi_ = new TH1F("timing_muctpi", "", 7, -3.5, 3.5);
  list->Add(timing_muctpi_);

  timing_coin_sl_ = new TH1F("timing_coin_sl", "", 7, -3.5, 3.5);
  list->Add(timing_coin_sl_);

  timing_coin_hi_ = new TH1F("timing_coin_hi", "", 7, -3.5, 3.5);
  list->Add(timing_coin_hi_);
  
  timing_coin_lo_ = new TH1F("timing_coin_lo", "", 7, -3.5, 3.5);
  list->Add(timing_coin_lo_);
}

/*--------------------------------------------------------------------*
 * fill histograms
 *--------------------------------------------------------------------*/
void
fillTiming()
{ 

  for (int ii = 0; ii < (int)muctpi_dw_eta->size(); ii++) {
    
    int source_muctpi = muctpi_dw_source->at(ii);
    if (source_muctpi == 0) continue;

    int diff = getBcidDiff(ii);
    timing_muctpi_->Fill(diff);

    int side_muctpi = muctpi_dw_hemisphere->at(ii);
    int roi_muctpi  = muctpi_dw_roi->at(ii);
    int pt_muctpi   = muctpi_dw_thrNumber->at(ii);
    int phi_muctpi  = muctpi_dw_sectorID->at(ii);

    int track1 = -1;
    int track2 = -1;

    for (int jj = 0; jj < (int)TGC_coin_bunch->size(); jj++ ) {

      int side_coin = TGC_coin_isAside->at(jj);
      int roi_coin  = TGC_coin_roi->at(jj);
      int pt_coin   = TGC_coin_pt->at(jj);
      int phi_coin  = TGC_coin_phi->at(ii);
      
      if (side_coin != side_muctpi) continue;
      if (roi_coin != roi_muctpi) continue;
      if (pt_coin != pt_muctpi) continue;

      if (source_muctpi == 1) {
        int tmp_phi = (TGC_coin_phi->at(jj)+1)%48; 
        if (tmp_phi != phi_muctpi) continue;
      
      } else {
        if (phi_coin != phi_muctpi) continue;
      }

      if (TGC_coin_type->at(jj) != 2) continue;
        
      timing_coin_sl_->Fill(TGC_coin_bunch->at(jj)-2);

      track1 = TGC_coin_trackletId->at(jj);
      track2 = TGC_coin_trackletIdStrip->at(jj);
    }

    for (int jj = 0; jj < (int)TGC_coin_bunch->size(); jj++ ) {
      
      int track = TGC_coin_trackletId->at(jj);
      int type = TGC_coin_type->at(jj);
      int bunch = TGC_coin_bunch->at(jj);

      if (track != track1 && track != track2) continue;

      if (type == 0) timing_coin_lo_->Fill(bunch-2);
      if (type == 1) timing_coin_hi_->Fill(bunch-2);
    }
  }
}

void
terminateTiming()
{
}
#endif // histogram_timing_h



