#ifndef histogram_fakeRate_h
#define histogram_fakeRate_h
/*--------------------------------------------------------------------*
 * declare histograms
 *--------------------------------------------------------------------*/

TH1F* fakeRate_eta_cond1_; // Raw RoIs
TH1F* fakeRate_eta_cond2_; // RoIs with offline muons
TH1F* fakeRate_eta_cond3_; // RoIs with pT>20 GeV offline muons

/*--------------------------------------------------------------------*
 * book histograms
 *--------------------------------------------------------------------*/
void
bookFakeRate(TList* list)
{
  fakeRate_eta_cond1_ = new TH1F("fakeRate_eta_cond1", "", 100, -2.5, 2.5);
  list->Add(fakeRate_eta_cond1_);

  fakeRate_eta_cond2_ = new TH1F("fakeRate_eta_cond2", "", 100, -2.5, 2.5);
  list->Add(fakeRate_eta_cond2_);

  fakeRate_eta_cond3_ = new TH1F("fakeRate_eta_cond3", "", 100, -2.5, 2.5);
  list->Add(fakeRate_eta_cond3_);
}

/*--------------------------------------------------------------------*
 * fill histograms
 *--------------------------------------------------------------------*/
void
fillFakeRate()
{ 
  //if (!isTriggered("HLT_noalg_L1MU20")) return; // disable for now

  for (int ii = 0; ii < (int)trig_L1_mu_eta->size(); ii++) {
    
    if (trig_L1_mu_thrNumber->at(ii) != 6) continue;

    double eta_roi = trig_L1_mu_eta->at(ii);
    double phi_roi = trig_L1_mu_phi->at(ii);

    fakeRate_eta_cond1_->Fill(eta_roi);

    bool matched1 = false; 
    bool matched2 = false;

    for (int jj = 0; jj < (int)mu_eta->size(); jj++) {

      if (mu_muonType->at(jj) != 0) continue;

      double eta_mu = mu_eta->at(jj);
      double phi_mu = mu_phi->at(jj);
      double pt_mu  = mu_pt->at(jj);

      double dr = tmTool::getEtaPhiDistance(eta_roi, phi_roi,
                                            eta_mu, phi_mu);

      if (dr > 0.1) continue;
      matched1 = true;

      if (pt_mu < 20000.) continue;
      matched2 = true;
    }

    if (matched1) fakeRate_eta_cond2_->Fill(eta_roi);
    if (matched2) fakeRate_eta_cond3_->Fill(eta_roi);
  }
}

void
terminateFakeRate()
{
}
#endif // histogram_fakeRate_h



