#ifndef isTriggered_h
#define isTriggered_h

bool
isTriggered(std::string chain) _CONST_
{

  bool passed = false;
  for (int ii = 0; ii < (int)trigger_info_chain->size(); ii++) {

    if (trigger_info_chain->at(ii) != chain) continue;
    if (!trigger_info_isPassed->at(ii)) continue;

    passed = true;
  }

  return passed;
}
#endif
// eof
