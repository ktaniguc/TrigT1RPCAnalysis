/*====================================================================*
 * declarations
 *====================================================================*/
/*--------------------------------------------------------------------*
 * headers
 *--------------------------------------------------------------------*/
#include "tmTool.hpp"
#include "tmConstant.hpp"

/*====================================================================*
 * implementation
 *====================================================================*/
/*--------------------------------------------------------------------*
 * tmTool
 *--------------------------------------------------------------------*/
// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------
double
tmTool::getDeltaPhi(const double phi1,
                    const double phi2)
{
  double dPhi = phi1 - phi2;
  if (dPhi > M_PI) {
    dPhi = 2.*M_PI - dPhi;
  } else if (dPhi < -M_PI) {
    dPhi = 2.*M_PI + dPhi;
  }
  return dPhi;
}



double
tmTool::getMass(const tmTool::tmPtEtaPhiM& x1,
                const tmTool::tmPtEtaPhiM& x2)
{
  TLorentzVector p1, p2, p3;
  p1.SetPtEtaPhiM(x1.pt, x1.eta, x1.phi, x1.m);
  p2.SetPtEtaPhiM(x2.pt, x2.eta, x2.phi, x2.m);
  p3 = p1 + p2;
  return p3.M();
}


/*double
tmTool::getEtaPhiDistance(const tmTool::tmEtaPhi& x1,
                          const tmTool::tmEtaPhi& x2)
{
  double dEta = fabs(x2.eta - x1.eta);
  double dPhi = fabs(x2.phi - x1.phi);
  if (dPhi > M_PI) dPhi = 2*M_PI - dPhi;
  double dR2 = dEta*dEta + dPhi*dPhi;
  return sqrt(dR2);
}*/



double
tmTool::getEtaPhiDistance(const double eta1,
                          const double phi1,
                          const double eta2,
                          const double phi2)
{
  double dEta = fabs(eta2 - eta1);
  double dPhi = fabs(phi2 - phi1);
  if (dPhi > M_PI) dPhi = 2*M_PI - dPhi;
  double dR2 = dEta*dEta + dPhi*dPhi;
  return sqrt(dR2);
}

/* eof */
