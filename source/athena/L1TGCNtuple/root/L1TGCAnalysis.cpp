/*====================================================================
 * declarations
 *====================================================================*/
/*--------------------------------------------------------------------*
 * headers
 *--------------------------------------------------------------------*/
#include "L1TGCAnalysis.hpp"

/*====================================================================*
 * implementation
 *====================================================================*/
// ---------------------------------------------------------------------
// Constructors and Destructor
// ---------------------------------------------------------------------

// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------
void
L1TGCAnalysis::Begin(TTree* /* tree */)
{
  eventCounter_ = 0;
  histograms_ = new TList();
  bookHistograms();

  return;
}



void
L1TGCAnalysis::SlaveBegin(TTree* /* tree */)
{
}



Bool_t
L1TGCAnalysis::Process(Long64_t entry)
{
  fChain->GetTree()->GetEntry(entry);

  std::string header = "L1TGCAnalysis::Process: ";

  fillHistograms();

  if ((++eventCounter_ % 1000) == 0) {
    printf("inf> %s%05d events processed\n", header.c_str(), eventCounter_);
  }

  return kTRUE;
}



void
L1TGCAnalysis::SlaveTerminate()
{
}



void
L1TGCAnalysis::Terminate()
{
  dumpHistograms();

#ifdef histogram_fakeRate_h
  terminateFakeRate();
#endif

#ifdef histogram_timing_h
  terminateTiming();
#endif

#ifdef histogram_phaseScan_h
  terminatePhaseScan();
#endif
}



void
L1TGCAnalysis::bookHistograms()
{
  TM_LOG_DBG("bookHistograms: -");

#ifdef histogram_fakeRate_h
  bookFakeRate(histograms_);
#endif

#ifdef histogram_timing_h
  bookTiming(histograms_);
#endif

#ifdef histogram_phaseScan_h
  bookPhaseScan(histograms_);
#endif

  TM_LOG_DBG("bookHistograms: +");

  return;
}



void
L1TGCAnalysis::fillHistograms()
{
  TM_LOG_DBG("fillHistograms: -");

#ifdef histogram_fakeRate_h
  fillFakeRate();
  TM_LOG_DBG("fillHistograms: fillFakeRate: +");
#endif

#ifdef histogram_timing_h
  fillTiming();
  TM_LOG_DBG("fillHistograms: fillTiming: +");
#endif

#ifdef histogram_phaseScan_h
  fillPhaseScan();
  TM_LOG_DBG("fillHistograms: fillPhaseScan: +");
#endif

  return;
}



void
L1TGCAnalysis::dumpHistograms()
{
  TFile *file = new TFile(TM_STR(ROOT_FILE_NAME), "RECREATE");
  histograms_->Write();

  file->Close();
}



#include "physics.C"
#include "tmTool.cpp"

#ifdef __MAKECINT__
#pragma link C++ class physics+;
#pragma link C++ class L1TGCAnalysis+;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<float> >+;
#endif // __MAKECINT__

/* eof */
