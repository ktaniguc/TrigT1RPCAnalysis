#ifndef L1TGCNTUPLE_HIERARCHY_h
#define L1TGCNTUPLE_HIERARCHY_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

namespace Trigger { class IMuonRoiChainSvc; }
namespace Trigger { class ITgcCoinHierarchySvc; }

class L1TGCNtuple_hierarchy {
  public:
    L1TGCNtuple_hierarchy(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_hierarchy();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    uint32_t             m_hierarchy_n;
    std::vector<int>     *m_hierarchy_index;
    std::vector<int>     *m_hierarchy_bcTag_SL;
    std::vector<int>     *m_hierarchy_dR_hiPt;
    std::vector<int>     *m_hierarchy_dPhi_hiPt;
    std::vector<int>     *m_hierarchy_dR_tracklet;
    std::vector<int>     *m_hierarchy_dPhi_tracklet;
    std::vector<int>     *m_hierarchy_isChamberBoundary;

    std::string m_muRoi;
    ServiceHandle<Trigger::IMuonRoiChainSvc> m_muonRoiChainSvc;
    ServiceHandle<Trigger::ITgcCoinHierarchySvc> m_tgcCoinHierarchySvc;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_hierarchy_h
/* eof */
