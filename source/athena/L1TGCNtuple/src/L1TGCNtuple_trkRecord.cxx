#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "TrackRecord/TrackRecordCollection.h"
#include "TrackRecord/TrackRecord.h"

#include "TTree.h"

#include "src/L1TGCNtuple_trkRecord.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_trkRecord::L1TGCNtuple_trkRecord(IMessageSvc* svc,
                                 const std::string& name)
  : m_rec_mc_n(0),
    m_rec_mc_pt(new std::vector<float>),
    m_rec_mc_eta(new std::vector<float>),
    m_rec_mc_phi(new std::vector<float>),
    m_rec_mc_e(new std::vector<float>),
    m_rec_mc_x(new std::vector<float>),
    m_rec_mc_y(new std::vector<float>),
    m_rec_mc_z(new std::vector<float>),
    m_rec_mc_time(new std::vector<float>),
    m_rec_mc_barcode(new std::vector<int>),
    m_rec_mc_pdgId(new std::vector<int>),

    m_muonEntryLayerFilter("MuonEntryLayerFilter"),
    m_muonExitLayerFilter("MuonExitLayerFilter"),
    
    msg(svc, name) {
}

L1TGCNtuple_trkRecord::~L1TGCNtuple_trkRecord() {

  delete m_rec_mc_pt;
  delete m_rec_mc_eta;
  delete m_rec_mc_phi;
  delete m_rec_mc_e;
  delete m_rec_mc_x;
  delete m_rec_mc_y;
  delete m_rec_mc_z;
  delete m_rec_mc_time;
  delete m_rec_mc_barcode;
  delete m_rec_mc_pdgId;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_trkRecord::book(TTree* ttree) {
 
  ttree->Branch("rec_mc_n", &m_rec_mc_n, "rec_mc_n/I");
  ttree->Branch("rec_mc_pt", &m_rec_mc_pt);
  ttree->Branch("rec_mc_eta", &m_rec_mc_eta);
  ttree->Branch("rec_mc_phi", &m_rec_mc_phi);
  ttree->Branch("rec_mc_e", &m_rec_mc_e);
  ttree->Branch("rec_mc_x", &m_rec_mc_x);
  ttree->Branch("rec_mc_y", &m_rec_mc_y);
  ttree->Branch("rec_mc_z", &m_rec_mc_z);
  ttree->Branch("rec_mc_time", &m_rec_mc_time);
  ttree->Branch("rec_mc_barcode", &m_rec_mc_barcode);
  ttree->Branch("rec_mc_pdgId", &m_rec_mc_pdgId);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_trkRecord::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  StatusCode sc;

  const TrackRecordCollection* container = 0;

  // entrance layer
  sc = storeGateSvc->retrieve(container, m_muonEntryLayerFilter);
  if (sc.isFailure()) return StatusCode::SUCCESS;

  for (TrackRecordCollection::const_iterator rec_it = container->begin(); 
      rec_it != container->end(); rec_it++) {

    CHECK(fillTrackRecord(*rec_it));
  }
  // exit layer
  sc = storeGateSvc->retrieve(container, m_muonExitLayerFilter);
  if (sc.isFailure()) return StatusCode::SUCCESS;

  for (TrackRecordCollection::const_iterator rec_it = container->begin(); 
      rec_it != container->end(); rec_it++) {

    CHECK(fillTrackRecord(*rec_it));
  }

  return StatusCode::SUCCESS;
}


StatusCode 
L1TGCNtuple_trkRecord::fillTrackRecord(const TrackRecord data) {

  m_rec_mc_n++;
  m_rec_mc_pt->push_back(data.GetMomentum().rho());
  m_rec_mc_eta->push_back(data.GetMomentum().eta());
  m_rec_mc_phi->push_back(data.GetMomentum().phi());
  m_rec_mc_e->push_back(data.GetEnergy());
  m_rec_mc_x->push_back(data.GetPosition().x());
  m_rec_mc_y->push_back(data.GetPosition().y());
  m_rec_mc_z->push_back(data.GetPosition().z());
  m_rec_mc_time->push_back(data.GetTime());
  m_rec_mc_barcode->push_back(data.GetBarCode());
  m_rec_mc_pdgId->push_back(data.GetPDGCode());
  
  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_trkRecord::clear() {
  
  m_rec_mc_n = 0;
  m_rec_mc_pt->clear();
  m_rec_mc_eta->clear();
  m_rec_mc_phi->clear();
  m_rec_mc_e->clear();
  m_rec_mc_x->clear();
  m_rec_mc_y->clear();
  m_rec_mc_z->clear();
  m_rec_mc_time->clear();
  m_rec_mc_barcode->clear();
  m_rec_mc_pdgId->clear();

  return;
}

/* eof */
