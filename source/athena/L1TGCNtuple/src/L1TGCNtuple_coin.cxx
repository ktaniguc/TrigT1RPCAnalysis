#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "MuonTrigCoinData/TgcCoinDataCollection.h"
#include "MuonTrigCoinData/TgcCoinDataContainer.h"
#include "MuonTrigCoinData/TgcCoinData.h"
#include "MuonDigitContainer/TgcDigit.h"

#include "TTree.h"

#include "src/L1TGCNtuple_coin.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_coin::L1TGCNtuple_coin(IMessageSvc* svc,
                                 const std::string& name)
  : m_coin_n(0),
    m_coin_x_In(new std::vector<float>),
    m_coin_y_In(new std::vector<float>),
    m_coin_z_In(new std::vector<float>),
    m_coin_x_Out(new std::vector<float>),
    m_coin_y_Out(new std::vector<float>),
    m_coin_z_Out(new std::vector<float>),
    m_coin_width_In(new std::vector<float>),
    m_coin_width_Out(new std::vector<float>),
    m_coin_width_R(new std::vector<float>),
    m_coin_width_Phi(new std::vector<float>),

    m_coin_isAside(new std::vector<int>),
    m_coin_isForward(new std::vector<int>),
    m_coin_isStrip(new std::vector<int>),
    m_coin_isInner(new std::vector<int>),
    m_coin_isPositiveDeltaR(new std::vector<int>),

    m_coin_type(new std::vector<int>),
    m_coin_trackletId(new std::vector<int>),
    m_coin_trackletIdStrip(new std::vector<int>),
    m_coin_phi(new std::vector<int>),
    m_coin_roi(new std::vector<int>),
    m_coin_pt(new std::vector<int>),
    m_coin_delta(new std::vector<int>),
    m_coin_sub(new std::vector<int>),
    m_coin_veto(new std::vector<int>),
    m_coin_bunch(new std::vector<int>),
    m_coin_inner(new std::vector<int>),
    
    m_tgcCoinData("TrigT1CoinDataCollection"),
    m_tgcCoinDataPriorBC("TrigT1CoinDataCollectionPriorBC"),
    m_tgcCoinDataNextBC("TrigT1CoinDataCollectionNextBC"),
    msg(svc, name) {
}

L1TGCNtuple_coin::~L1TGCNtuple_coin() {

  delete  m_coin_x_In;
  delete  m_coin_y_In;
  delete  m_coin_z_In;
  delete  m_coin_x_Out;
  delete  m_coin_y_Out;
  delete  m_coin_z_Out;
  delete  m_coin_width_In;
  delete  m_coin_width_Out;
  delete  m_coin_width_R;
  delete  m_coin_width_Phi;

  delete  m_coin_isAside;
  delete  m_coin_isForward;
  delete  m_coin_isStrip;
  delete  m_coin_isInner;
  delete  m_coin_isPositiveDeltaR;

  delete  m_coin_type;
  delete  m_coin_trackletId;
  delete  m_coin_trackletIdStrip;
  delete  m_coin_phi;
  delete  m_coin_roi;
  delete  m_coin_pt;
  delete  m_coin_delta;
  delete  m_coin_sub;
  delete  m_coin_bunch;
  delete  m_coin_inner;
    
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_coin::book(TTree* ttree) {

  ttree->Branch("TGC_coin_n", &m_coin_n, "TGC_coin_n/I"); 
  ttree->Branch("TGC_coin_x_In", &m_coin_x_In);
  ttree->Branch("TGC_coin_y_In", &m_coin_y_In);
  ttree->Branch("TGC_coin_z_In", &m_coin_z_In);
  ttree->Branch("TGC_coin_x_Out", &m_coin_x_Out);
  ttree->Branch("TGC_coin_y_Out", &m_coin_y_Out);
  ttree->Branch("TGC_coin_z_Out", &m_coin_z_Out);
  ttree->Branch("TGC_coin_width_In", &m_coin_width_In);
  ttree->Branch("TGC_coin_width_Out", &m_coin_width_Out);
  ttree->Branch("TGC_coin_width_R", &m_coin_width_R);
  ttree->Branch("TGC_coin_width_Phi", &m_coin_width_Phi);

  ttree->Branch("TGC_coin_isAside", &m_coin_isAside);
  ttree->Branch("TGC_coin_isForward", &m_coin_isForward);
  ttree->Branch("TGC_coin_isStrip", &m_coin_isStrip);
  ttree->Branch("TGC_coin_isInner", &m_coin_isInner);
  ttree->Branch("TGC_coin_isPositiveDeltaR", &m_coin_isPositiveDeltaR);

  ttree->Branch("TGC_coin_type", &m_coin_type);
  ttree->Branch("TGC_coin_trackletId", &m_coin_trackletId);
  ttree->Branch("TGC_coin_trackletIdStrip", &m_coin_trackletIdStrip);
  ttree->Branch("TGC_coin_phi", &m_coin_phi);
  ttree->Branch("TGC_coin_roi", &m_coin_roi);
  ttree->Branch("TGC_coin_pt", &m_coin_pt);
  ttree->Branch("TGC_coin_delta", &m_coin_delta);
  ttree->Branch("TGC_coin_sub", &m_coin_sub);
  ttree->Branch("TGC_coin_veto", &m_coin_veto);
  ttree->Branch("TGC_coin_bunch", &m_coin_bunch);
  ttree->Branch("TGC_coin_inner", &m_coin_inner);
  
  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/


StatusCode
L1TGCNtuple_coin::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  Muon::TgcCoinDataContainer::const_iterator tcdc_it;
  Muon::TgcCoinDataCollection::const_iterator cit;
  
  // current
  const Muon::TgcCoinDataContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, m_tgcCoinData));

  for (tcdc_it = container->begin(); tcdc_it != container->end(); tcdc_it++) {
    for (cit = (*tcdc_it)->begin(); cit != (*tcdc_it)->end(); cit++) {
      const Muon::TgcCoinData *data = *cit;
      CHECK(fillTgcCoinData(data, (int)TgcDigit::BC_CURRENT));
    }
  }
  
  // previous
  const Muon::TgcCoinDataContainer* containerPriorBC = 0;
  CHECK(storeGateSvc->retrieve(containerPriorBC, m_tgcCoinDataPriorBC));
  for (tcdc_it = containerPriorBC->begin(); tcdc_it != containerPriorBC->end(); tcdc_it++) {
    for (cit = (*tcdc_it)->begin(); cit != (*tcdc_it)->end(); cit++) {
      const Muon::TgcCoinData *data = *cit;
      CHECK(fillTgcCoinData(data, (int)TgcDigit::BC_PREVIOUS));
    }
  }
  
  // next
  const Muon::TgcCoinDataContainer* containerNextBC = 0;
  CHECK(storeGateSvc->retrieve(containerNextBC, m_tgcCoinDataNextBC));
  for (tcdc_it = containerNextBC->begin(); tcdc_it != containerNextBC->end(); tcdc_it++) {
    for (cit = (*tcdc_it)->begin(); cit != (*tcdc_it)->end(); cit++) {
      const Muon::TgcCoinData *data = *cit;
      CHECK(fillTgcCoinData(data, (int)TgcDigit::BC_NEXT));
    }
  }
  

  return StatusCode::SUCCESS;
}

StatusCode
L1TGCNtuple_coin::fillTgcCoinData(const Muon::TgcCoinData* data, const int bunch) {

  const int type = data->type();

  const Amg::Vector3D& posIn = data->globalposIn();
  m_coin_x_In->push_back(posIn[0]);
  m_coin_y_In->push_back(posIn[1]);
  m_coin_z_In->push_back(posIn[2]);
      
  const Amg::Vector3D& posOut = data->globalposOut();
  m_coin_x_Out->push_back(posOut[0]);
  m_coin_y_Out->push_back(posOut[1]);
  m_coin_z_Out->push_back(posOut[2]);

  m_coin_width_In->push_back(data->widthIn());
  m_coin_width_Out->push_back(data->widthOut());

  if (type == Muon::TgcCoinData::TYPE_SL) {
    const Amg::MatrixX& matrix = data->errMat();
    //m_coin_width_R->push_back(Amg::error(matrix, Trk::locR));
     //m_coin_width_Phi->push_back(Amg::error(matrix, Trk::locPhi));
    m_coin_width_R->push_back(matrix(0,0));
    m_coin_width_Phi->push_back(matrix(1,1));
  } else {
    m_coin_width_R->push_back(0.);
    m_coin_width_Phi->push_back(0.);
  }

  m_coin_isAside->push_back(data->isAside());
  m_coin_isForward->push_back(data->isForward());
  m_coin_isStrip->push_back(data->isStrip());
  m_coin_isInner->push_back(data->isInner());
  m_coin_isPositiveDeltaR->push_back(data->isPositiveDeltaR());

  m_coin_type->push_back(type);
  m_coin_trackletId->push_back(data->trackletId());
  m_coin_trackletIdStrip->push_back(data->trackletIdStrip());
  m_coin_phi->push_back(data->phi());
  m_coin_roi->push_back(data->roi());
  m_coin_pt->push_back(data->pt());
  m_coin_delta->push_back(data->delta());
  m_coin_sub->push_back(data->sub());
  m_coin_veto->push_back(data->veto());

  m_coin_bunch->push_back(bunch);
  m_coin_inner->push_back(data->inner());
  
  m_coin_n++;

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_coin::clear() {
  
  m_coin_n = 0;

  m_coin_x_In->clear();
  m_coin_y_In->clear();
  m_coin_z_In->clear();
  m_coin_x_Out->clear();
  m_coin_y_Out->clear();
  m_coin_z_Out->clear();
  m_coin_width_In->clear();
  m_coin_width_Out->clear();
  m_coin_width_R->clear();
  m_coin_width_Phi->clear();

  m_coin_isAside->clear();
  m_coin_isForward->clear();
  m_coin_isStrip->clear();
  m_coin_isInner->clear();
  m_coin_isPositiveDeltaR->clear();

  m_coin_type->clear();
  m_coin_trackletId->clear();
  m_coin_trackletIdStrip->clear();
  m_coin_phi->clear();
  m_coin_roi->clear();
  m_coin_pt->clear();
  m_coin_delta->clear();
  m_coin_sub->clear();
  m_coin_veto->clear();
  m_coin_bunch->clear();
  m_coin_inner->clear();

  return;
}

/* eof */
