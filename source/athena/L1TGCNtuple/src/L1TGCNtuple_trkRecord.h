#ifndef L1TGCNTUPLE_TRKRECORD_h
#define L1TGCNTUPLE_TRKRECORD_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class TrackRecord;

class L1TGCNtuple_trkRecord {
  public:
    L1TGCNtuple_trkRecord(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_trkRecord();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    StatusCode fillTrackRecord(const TrackRecord data);

    uint32_t             m_rec_mc_n;
    std::vector<float>   *m_rec_mc_pt;
    std::vector<float>   *m_rec_mc_eta;
    std::vector<float>   *m_rec_mc_phi;
    std::vector<float>   *m_rec_mc_e;
    std::vector<float>   *m_rec_mc_x;
    std::vector<float>   *m_rec_mc_y;
    std::vector<float>   *m_rec_mc_z;
    std::vector<float>   *m_rec_mc_time;
    std::vector<int>   *m_rec_mc_barcode;
    std::vector<int>   *m_rec_mc_pdgId;
    
    std::string m_muonEntryLayerFilter;
    std::string m_muonExitLayerFilter;
    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_truth_h
/* eof */
