#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "TTree.h"

#include "src/L1TGCNtuple_truth.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_truth::L1TGCNtuple_truth(IMessageSvc* svc,
                                 const std::string& name)
  : m_mc_n(0),
    m_mc_pt(new std::vector<float>),
    m_mc_eta(new std::vector<float>),
    m_mc_phi(new std::vector<float>),
    m_mc_m(new std::vector<float>),
    m_mc_charge(new std::vector<int>),

    m_mc_pdgId(new std::vector<int>),
    m_mc_barcode(new std::vector<int>),
    m_mc_status(new std::vector<int>),
    
    m_mc_prodVtx_x(new std::vector<float>),
    m_mc_prodVtx_y(new std::vector<float>),
    m_mc_prodVtx_z(new std::vector<float>),
    
    m_truthParticles("MuonTruthParticles"),
    
    msg(svc, name) {
}

L1TGCNtuple_truth::~L1TGCNtuple_truth() {

  delete m_mc_pt;
  delete m_mc_eta;
  delete m_mc_phi;
  delete m_mc_m;
  delete m_mc_charge;

  delete m_mc_pdgId;
  delete m_mc_barcode;
  delete m_mc_status;
  
  delete m_mc_prodVtx_x;
  delete m_mc_prodVtx_y;
  delete m_mc_prodVtx_z;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_truth::book(TTree* ttree) {

  ttree->Branch("mc_n", &m_mc_n, "mc_n/I"); 
  ttree->Branch("mc_pt", &m_mc_pt);
  ttree->Branch("mc_eta", &m_mc_eta);
  ttree->Branch("mc_phi", &m_mc_phi);
  ttree->Branch("mc_m", &m_mc_m);
  ttree->Branch("mc_charge", &m_mc_charge);
  ttree->Branch("mc_pdgId", &m_mc_pdgId);
  ttree->Branch("mc_barcode", &m_mc_barcode);
  ttree->Branch("mc_status", &m_mc_status);
  ttree->Branch("mc_prodVtx_x", &m_mc_prodVtx_x);
  ttree->Branch("mc_prodVtx_y", &m_mc_prodVtx_y);
  ttree->Branch("mc_prodVtx_z", &m_mc_prodVtx_z);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_truth::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::TruthParticleContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, m_truthParticles);

  if (sc.isFailure()) return StatusCode::SUCCESS;

  xAOD::TruthParticleContainer::const_iterator truth_it;

  for (truth_it = container->begin(); truth_it != container->end(); truth_it++) {
    const xAOD::TruthParticle* truth = *truth_it;

    m_mc_n++;
    m_mc_pt->push_back(truth->pt());
    m_mc_eta->push_back(truth->eta());
    m_mc_phi->push_back(truth->phi());
    m_mc_m->push_back(truth->m());
    m_mc_charge->push_back(truth->charge());
    m_mc_pdgId->push_back(truth->pdgId());
    m_mc_barcode->push_back(truth->barcode());
    m_mc_status->push_back(truth->status());
    
    if (truth->hasProdVtx()) {
      const ElementLink< xAOD::TruthVertexContainer >& el = truth->prodVtxLink();
      const xAOD::TruthVertex* vertex = *el;

      m_mc_prodVtx_x->push_back(vertex->x());
      m_mc_prodVtx_y->push_back(vertex->y());
      m_mc_prodVtx_z->push_back(vertex->z());

    } else {
      m_mc_prodVtx_x->push_back(-9999);
      m_mc_prodVtx_y->push_back(-9999);
      m_mc_prodVtx_z->push_back(-9999);
    }
  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_truth::clear() {
  
  m_mc_n = 0;
  m_mc_pt->clear();
  m_mc_eta->clear();
  m_mc_phi->clear();
  m_mc_m->clear();
  m_mc_charge->clear();

  m_mc_pdgId->clear();
  m_mc_barcode->clear();
  m_mc_status->clear();

  m_mc_prodVtx_x->clear();
  m_mc_prodVtx_y->clear();
  m_mc_prodVtx_z->clear();

  return;
}

/* eof */
