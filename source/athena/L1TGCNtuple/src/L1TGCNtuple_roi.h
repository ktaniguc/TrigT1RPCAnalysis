#ifndef L1TGCNTUPLE_ROI_h
#define L1TGCNTUPLE_ROI_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class L1TGCNtuple_roi {
  public:
    L1TGCNtuple_roi(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_roi();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    
    uint32_t             m_roi_n;
    std::vector<float>   *m_roi_pt;
    std::vector<float>   *m_roi_eta;
    std::vector<float>   *m_roi_phi;
    std::vector<std::string>  *m_roi_thrName;
    std::vector<short>   *m_roi_thrNumber;
    std::vector<short>   *m_roi_RoINumber;
    std::vector<short>   *m_roi_sectorAddress;
    std::vector<int>     *m_roi_firstCandidate;
    std::vector<int>     *m_roi_moreCandInRoI;
    std::vector<int>     *m_roi_moreCandInSector;
    std::vector<short>   *m_roi_source;
    std::vector<short>   *m_roi_hemisphere;
    std::vector<short>   *m_roi_charge;
    std::vector<int>     *m_roi_vetoed;

    std::string m_muRoi;
    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_roi_h
/* eof */
