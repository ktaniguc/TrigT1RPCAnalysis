#ifndef L1TGCNTUPLE_MURCV_h
#define L1TGCNTUPLE_MURCV_h

#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class MuonIdHelper;

class TileHWID;

class L1TGCNtuple_murcv {
  public:
    L1TGCNtuple_murcv(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_murcv();
    
    StatusCode book(TTree* ttree,
        ServiceHandle<StoreGateSvc>& detector);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate,
                    const bool isData = true);

    StatusCode setConstants(const std::string name);
   
    void clear();

  private:
    enum {SIDE_SIZE = 2, MODULE_SIZE = 64, CHANNEL_SIZE = 4};
    
    float m_raw_const_a[SIDE_SIZE][MODULE_SIZE][CHANNEL_SIZE];
    float m_raw_const_b[SIDE_SIZE][MODULE_SIZE][CHANNEL_SIZE];
    
    int               m_murcv_trig_n; 
    std::vector<int>  *m_murcv_trig_mod; 
    std::vector<int>  *m_murcv_trig_part; 
    std::vector<bool> *m_murcv_trig_bit0; 
    std::vector<bool> *m_murcv_trig_bit1; 
    std::vector<bool> *m_murcv_trig_bit2; 
    std::vector<bool> *m_murcv_trig_bit3; 

    int                m_murcv_raw_n; 
    std::vector<float> *m_murcv_raw_count;
    std::vector<float> *m_murcv_raw_energy;
    std::vector<int>   *m_murcv_raw_ros;
    std::vector<int>   *m_murcv_raw_drawer;
    std::vector<int>   *m_murcv_raw_channel;

    int                m_murcv_digit_n; 
    std::vector<int>   *m_murcv_digit_nSamples;
    std::vector<int>   *m_murcv_digit_ros;
    std::vector<int>   *m_murcv_digit_drawer;
    std::vector<int>   *m_murcv_digit_channel;
    std::vector<std::vector<float> > *m_murcv_digit_sampleVec;

    std::string        m_constants;
    
    const TileHWID* m_tileHWID;

    mutable MsgStream msg;

};

#endif  // L1TGCNtuple_murcv_h
/* eof */
