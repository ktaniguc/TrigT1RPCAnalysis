#include "GaudiKernel/DeclareFactoryEntries.h"
#include "L1TGCNtuple/L1TGCNtuple.h"
#include "../L1TgcSkim.h"

DECLARE_ALGORITHM_FACTORY(L1TGCNtuple)
DECLARE_TOOL_FACTORY(L1TgcSkim)

DECLARE_FACTORY_ENTRIES(L1TGCNtuple) {
  DECLARE_ALGORITHM(L1TGCNtuple)
  DECLARE_TOOL(L1TgcSkim)
}
