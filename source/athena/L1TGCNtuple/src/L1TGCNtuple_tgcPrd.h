#ifndef L1TGCNTUPLE_TGCPRD_h
#define L1TGCNTUPLE_TGCPRD_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class MuonIdHelper;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Muon { class MuonIdHelperTool; }
namespace Muon { class TgcPrepData; }

class L1TGCNtuple_tgcPrd {
  public:
    L1TGCNtuple_tgcPrd(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_tgcPrd();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    StatusCode fillTgcPrepData(const Muon::TgcPrepData *data, const int bunch);

    uint32_t           m_prd_n;
    std::vector<float> *m_prd_x;
    std::vector<float> *m_prd_y;
    std::vector<float> *m_prd_z;
    std::vector<float> *m_prd_shortWidth;
    std::vector<float> *m_prd_longWidth;
    std::vector<float> *m_prd_length;
    std::vector<int> *m_prd_isStrip;
    std::vector<int> *m_prd_gasGap;
    std::vector<int> *m_prd_channel;
    std::vector<int> *m_prd_eta;
    std::vector<int> *m_prd_phi;
    std::vector<int> *m_prd_station;
    std::vector<int> *m_prd_bunch;

    ToolHandle<Muon::MuonIdHelperTool> m_idHelper;
    std::string m_tgcPrepDataAllBCs;
    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_tgcPrd_h
/* eof */
