#ifndef L1TGCNTUPLE_RPCPRD_h
#define L1TGCNTUPLE_RPCPRD_h

#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "RPCcablingInterface/IRPCcablingServerSvc.h"
//#include "../../Trigger/TrigT1/TrigT1RPClogic/TrigT1RPClogic/RPCsimuData.h"
//#include "../../Trigger/TrigT1/TrigT1RPClogic/TrigT1RPClogic/CMAdata.h"
//#include "../../Trigger/TrigT1/TrigT1RPClogic/TrigT1RPClogic/PADdata.h"
#include "TrigT1RPClogic/RPCsimuData.h"
#include "TrigT1RPClogic/CMAdata.h"
#include "TrigT1RPClogic/PADdata.h"
#include "TrigT1RPClogic/SLdata.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class MuonIdHelper;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Muon { class MuonIdHelperTool; }
namespace Muon { class RpcPrepData; }

class L1TGCNtuple_rpcPrd {
  public:
    L1TGCNtuple_rpcPrd(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_rpcPrd();
   
    StatusCode initialize(); 
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
    std::string stationNameString(int statNum);
   
    void clear();

  private:
    StatusCode fillRpcPrepData(const Muon::RpcPrepData *data, RPCsimuData& rpcData);

    uint32_t           m_prd_n;
    uint32_t           m_sl_n;
    std::vector<float> *m_prd_x;
    std::vector<float> *m_prd_y;
    std::vector<float> *m_prd_z;
    std::vector<float> *m_prd_x2;
    std::vector<float> *m_prd_y2;
    std::vector<float> *m_prd_z2;
	std::vector<float> *m_prd_time;
    std::vector<int> *m_prd_triggerInfo;
    std::vector<int> *m_prd_ambiguityFlag;
    std::vector<int> *m_prd_measuresPhi;
    std::vector<int> *m_prd_inRibs;
    std::vector<int> *m_prd_station;
    std::vector<int> *m_prd_stationEta;
    std::vector<int> *m_prd_stationPhi;
    std::vector<int> *m_prd_doubletR;
    std::vector<int> *m_prd_doubletZ;
    std::vector<double> *m_prd_stripWidth;
    std::vector<double> *m_prd_stripLength;
    std::vector<int> *m_prd_gasGap;
    std::vector<int> *m_prd_channel;
    std::vector<int> *m_sl_isMoreCandInRoI;
    std::vector<int> *m_sl_thrNumber;
    std::vector<int> *m_sl_roiNumber;
    std::vector<int> *m_sl_dbc;
    std::vector<int> *m_sl_bunchXID;
    std::vector<int> *m_sl_sectorID;
    std::vector<int> *m_sl_side;

    ToolHandle<Muon::MuonIdHelperTool> m_idHelper;
    ServiceHandle <IRPCcablingServerSvc> m_cabling_getter;
    const IRPCcablingSvc* m_cabling;
    std::string m_rpcPrepDataAllBCs;
    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_rpcPrd_h
/* eof */

