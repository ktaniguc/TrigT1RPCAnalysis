#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "MuonPrepRawData/MdtPrepDataCollection.h"
#include "MuonPrepRawData/MdtPrepDataContainer.h"
#include "MuonPrepRawData/MdtPrepData.h"
#include "MuonReadoutGeometry/MdtReadoutElement.h"
#include "EventPrimitives/EventPrimitivesHelpers.h"

#include "TTree.h"

#include "src/L1TGCNtuple_mdtPrd.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_mdtPrd::L1TGCNtuple_mdtPrd(IMessageSvc* svc,
                                 const std::string& name)
  : m_prd_n(0),
    m_prd_x(new std::vector<float>),
    m_prd_y(new std::vector<float>),
    m_prd_z(new std::vector<float>),
    m_prd_adc(new std::vector<int>),
    m_prd_tdc(new std::vector<int>),
    m_prd_status(new std::vector<int>),
    m_prd_drift_radius(new std::vector<float>),
    m_prd_drift_radius_error(new std::vector<float>),
    msg(svc, name) {
}

L1TGCNtuple_mdtPrd::~L1TGCNtuple_mdtPrd() {

  delete m_prd_x;
  delete m_prd_y;
  delete m_prd_z;
  delete m_prd_adc;
  delete m_prd_tdc;
  delete m_prd_status;
  delete m_prd_drift_radius;
  delete m_prd_drift_radius_error;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_mdtPrd::book(TTree* ttree) {

  ttree->Branch("MDT_prd_n", &m_prd_n, "MDT_prd_n/I"); 
  ttree->Branch("MDT_prd_x", &m_prd_x);
  ttree->Branch("MDT_prd_y", &m_prd_y);
  ttree->Branch("MDT_prd_z", &m_prd_z);
  ttree->Branch("MDT_prd_adc", &m_prd_adc);
  ttree->Branch("MDT_prd_tdc", &m_prd_tdc);
  ttree->Branch("MDT_prd_status", &m_prd_status);
  ttree->Branch("MDT_prd_drift_radius", &m_prd_drift_radius);
  ttree->Branch("MDT_prd_drift_radius_error", &m_prd_drift_radius_error);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_mdtPrd::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {
 
  const Muon::MdtPrepDataContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, "MDT_DriftCircles");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  for (Muon::MdtPrepDataContainer::const_iterator cit_cont = container->begin();
    cit_cont != container->end(); cit_cont++) {
    for (Muon::MdtPrepDataCollection::const_iterator cit_coll = (*cit_cont)->begin();
      cit_coll != (*cit_cont)->end(); cit_coll++) {

      const Muon::MdtPrepData *data = *cit_coll;

      const MuonGM::MdtReadoutElement* element = data->detectorElement();
      if (element->isInBarrel()) continue;

      const Amg::Vector3D& tubePos = data->globalPosition();

      if (data->status() != 1) { // non-"valid measurement"
        // if (/* noalg trigger */){ 
        continue;
        //}
      }

      m_prd_x->push_back(tubePos[0]);
      m_prd_y->push_back(tubePos[1]);
      m_prd_z->push_back(tubePos[2]);
      m_prd_adc->push_back(data->adc());
      m_prd_tdc->push_back(data->tdc());
      m_prd_status->push_back(data->status());
      m_prd_drift_radius->push_back(data->localPosition()[Trk::driftRadius]);
      m_prd_drift_radius_error->push_back(Amg::error(data->localCovariance(),Trk::driftRadius));

      m_prd_n++;
    }
  }

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_mdtPrd::clear() {
  
  m_prd_n = 0;
  m_prd_x->clear();
  m_prd_y->clear();
  m_prd_z->clear();
  m_prd_adc->clear();
  m_prd_tdc->clear();
  m_prd_status->clear();
  m_prd_drift_radius->clear();
  m_prd_drift_radius_error->clear();

  return;
}

/* eof */
