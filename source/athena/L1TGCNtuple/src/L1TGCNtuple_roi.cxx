#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODTrigger/MuonRoIContainer.h"

#include "TTree.h"

#include "src/L1TGCNtuple_roi.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_roi::L1TGCNtuple_roi(IMessageSvc* svc,
                                 const std::string& name)
  : m_roi_n(0),
    m_roi_pt(new std::vector<float>),
    m_roi_eta(new std::vector<float>),
    m_roi_phi(new std::vector<float>),
    m_roi_thrName(new std::vector<std::string>),
    m_roi_thrNumber(new std::vector<short>),
    m_roi_RoINumber(new std::vector<short>),
    m_roi_sectorAddress(new std::vector<short>),
    m_roi_firstCandidate(new std::vector<int>),
    m_roi_moreCandInRoI(new std::vector<int>),
    m_roi_moreCandInSector(new std::vector<int>),
    m_roi_source(new std::vector<short>),
    m_roi_hemisphere(new std::vector<short>),
    m_roi_charge(new std::vector<short>),
    m_roi_vetoed(new std::vector<int>),

    m_muRoi("LVL1MuonRoIs"),
    
    msg(svc, name) {
}

L1TGCNtuple_roi::~L1TGCNtuple_roi() {

  delete m_roi_pt;
  delete m_roi_eta;
  delete m_roi_phi;
  delete m_roi_thrName;
  delete m_roi_thrNumber;
  delete m_roi_RoINumber;
  delete m_roi_sectorAddress;
  delete m_roi_firstCandidate;
  delete m_roi_moreCandInRoI;
  delete m_roi_moreCandInSector;
  delete m_roi_source;
  delete m_roi_hemisphere;
  delete m_roi_charge;
  delete m_roi_vetoed;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_roi::book(TTree* ttree) {

  ttree->Branch("trig_L1_mu_n", &m_roi_n, "trig_L1_mu_n/I"); 
  //ttree->Branch("trig_L1_mu_pt", &m_roi_pt);
  ttree->Branch("trig_L1_mu_eta", &m_roi_eta);
  ttree->Branch("trig_L1_mu_phi", &m_roi_phi);
  ttree->Branch("trig_L1_mu_thrName", &m_roi_thrName);
  ttree->Branch("trig_L1_mu_thrNumber", &m_roi_thrNumber);
  ttree->Branch("trig_L1_mu_RoINumber", &m_roi_RoINumber);
  ttree->Branch("trig_L1_mu_sectorAddress", &m_roi_sectorAddress);
  ttree->Branch("trig_L1_mu_firstCandidate", &m_roi_firstCandidate);
  ttree->Branch("trig_L1_mu_moreCandInRoI", &m_roi_moreCandInRoI);
  ttree->Branch("trig_L1_mu_moreCandInSector", &m_roi_moreCandInSector);
  ttree->Branch("trig_L1_mu_source", &m_roi_source);
  ttree->Branch("trig_L1_mu_hemisphere", &m_roi_hemisphere);
  ttree->Branch("trig_L1_mu_charge", &m_roi_charge);
  ttree->Branch("trig_L1_mu_vetoed", &m_roi_vetoed);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_roi::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::MuonRoIContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, m_muRoi));

  xAOD::MuonRoIContainer::const_iterator roi_it;

  for (roi_it = container->begin(); roi_it != container->end(); roi_it++) {
    const xAOD::MuonRoI* roi = *roi_it;

    m_roi_eta->push_back(roi->eta());
    m_roi_phi->push_back(roi->phi());
    m_roi_thrName->push_back(roi->thrName());
    m_roi_thrNumber->push_back(roi->getThrNumber());
    if(roi->getSource() == xAOD::MuonRoI::Barrel){
      std::cout << "roi isMoreCandInRoI/sectorAddress/thrNumber = " << roi->isMoreCandInRoI() << "/" << roi->getSectorAddress() << "/" << roi->getThrNumber() << std::endl;;
      std::cout << "roiWord/hemisphere = " << roi->roiWord() << "/" << roi->getHemisphere() << std::endl;
    }
    m_roi_RoINumber->push_back(roi->getRoI());
    m_roi_sectorAddress->push_back(roi->getSectorAddress());
    m_roi_firstCandidate->push_back(roi->isFirstCandidate());
    m_roi_moreCandInRoI->push_back(roi->isMoreCandInRoI());
    m_roi_moreCandInSector->push_back(roi->isMoreCandInSector());
    m_roi_vetoed->push_back(roi->isVetoed());

    if (roi->getSource() == xAOD::MuonRoI::Barrel) m_roi_source->push_back(0);
    else if (roi->getSource() == xAOD::MuonRoI::Endcap) m_roi_source->push_back(1);
    else if (roi->getSource() == xAOD::MuonRoI::Forward) m_roi_source->push_back(2);
    else m_roi_source->push_back(-1);

    if (roi->getHemisphere() == xAOD::MuonRoI::Negative) m_roi_hemisphere->push_back(0);
    else if (roi->getHemisphere() == xAOD::MuonRoI::Positive) m_roi_hemisphere->push_back(1);
    else m_roi_hemisphere->push_back(-1);

    if (roi->getCharge() == xAOD::MuonRoI::Neg) m_roi_charge->push_back(0);
    else if (roi->getCharge() == xAOD::MuonRoI::Pos) m_roi_charge->push_back(1);
    else if (roi->getCharge() == xAOD::MuonRoI::Undef) m_roi_charge->push_back(100);
    else m_roi_hemisphere->push_back(-1);

    m_roi_n++;
  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_roi::clear() {
  
  m_roi_n = 0;
  m_roi_pt->clear();
  m_roi_eta->clear();
  m_roi_phi->clear();
  m_roi_thrName->clear();
  m_roi_thrNumber->clear();
  m_roi_RoINumber->clear();
  m_roi_sectorAddress->clear();
  m_roi_firstCandidate->clear();
  m_roi_moreCandInRoI->clear();
  m_roi_moreCandInSector->clear();
  m_roi_source->clear();
  m_roi_hemisphere->clear();
  m_roi_charge->clear();
  m_roi_vetoed->clear();

  return;
}

/* eof */
