#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "MuonIdHelpers/MuonIdHelperTool.h"
#include "MuonDigitContainer/sTgcDigitContainer.h"
#include "MuonDigitContainer/sTgcDigit.h"
#include "MuonReadoutGeometry/sTgcReadoutElement.h"

#include "TTree.h"

#include "src/L1TGCNtuple_sTgcRdo.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_sTgcRdo::L1TGCNtuple_sTgcRdo(IMessageSvc* svc,
                                 const std::string& name)
  : m_rdo_stationName(new std::vector<std::string>),
    m_rdo_stationEta(new std::vector<int>),
    m_rdo_stationPhi(new std::vector<int>),
    m_rdo_multiplet(new std::vector<int>),
    m_rdo_gasGap(new std::vector<int>),
    m_rdo_type(new std::vector<int>),
    m_rdo_channel(new std::vector<int>),
    m_rdo_x(new std::vector<float>),
    m_rdo_y(new std::vector<float>),
    m_rdo_z(new std::vector<float>),
    m_idHelper("Muon::MuonIdHelperTool/MuonIdHelperTool"),
    m_detManager(0),
    msg(svc, name) {
}

L1TGCNtuple_sTgcRdo::~L1TGCNtuple_sTgcRdo() {

  delete m_rdo_stationName;
  delete m_rdo_stationEta;
  delete m_rdo_stationPhi;
  delete m_rdo_multiplet;
  delete m_rdo_gasGap;
  delete m_rdo_type;
  delete m_rdo_channel;
  delete m_rdo_x;
  delete m_rdo_y;
  delete m_rdo_z;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_sTgcRdo::book(TTree* ttree,
    ServiceHandle<StoreGateSvc>& detector) {

  CHECK(detector->retrieve(m_detManager));
 
  ttree->Branch("sTGC_rdo_stationName", &m_rdo_stationName);
  ttree->Branch("sTGC_rdo_stationEta", &m_rdo_stationEta);
  ttree->Branch("sTGC_rdo_stationPhi", &m_rdo_stationPhi);
  ttree->Branch("sTGC_rdo_multiplet", &m_rdo_multiplet);
  ttree->Branch("sTGC_rdo_gasGap", &m_rdo_gasGap);
  ttree->Branch("sTGC_rdo_type", &m_rdo_type);
  ttree->Branch("sTGC_rdo_channel", &m_rdo_channel);
  ttree->Branch("sTGC_rdo_x", &m_rdo_x);
  ttree->Branch("sTGC_rdo_y", &m_rdo_y);
  ttree->Branch("sTGC_rdo_z", &m_rdo_z);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_sTgcRdo::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const sTgcIdHelper& sTgcIdHelper = m_idHelper->stgcIdHelper();

  const sTgcDigitContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, "sTGC_DIGITS"));

  sTgcDigitContainer::const_iterator tpdc_it;
  sTgcDigitCollection::const_iterator cit;

  for (tpdc_it = container->begin(); tpdc_it != container->end(); tpdc_it++) {
    for (cit = (*tpdc_it)->begin(); cit != (*tpdc_it)->end(); cit++) {
      const sTgcDigit *data = *cit;
      const Identifier id = data->identify();

      std::string stationName = sTgcIdHelper.stationNameString(sTgcIdHelper.stationName(id)); 
      int stationEta = sTgcIdHelper.stationEta(id);
      int stationPhi = sTgcIdHelper.stationPhi(id);
      int multiplet  = sTgcIdHelper.multilayer(id); 
      int gasGap     = sTgcIdHelper.gasGap(id);
      int channel    = sTgcIdHelper.channel(id);
      int type       = sTgcIdHelper.channelType(id);

      m_rdo_stationName->push_back(stationName);
      m_rdo_stationEta->push_back(stationEta);
      m_rdo_stationPhi->push_back(stationPhi);
      m_rdo_multiplet->push_back(multiplet);
      m_rdo_gasGap->push_back(gasGap);
      m_rdo_channel->push_back(channel);
      m_rdo_type->push_back(type);

      /*int isSmall = stationName[2] == 'S';
      const MuonGM::sTgcReadoutElement* rdoEl = 
        m_detManager->getsTgcRElement_fromIdFields(isSmall, stationEta, stationPhi, multiplet);

      const Identifier phiId, etaId;
      Amg::Vector3D gpos(0.,0.,0.);
      
      if(!rdoEl->spacePointPosition(phiId,etaId,gpos)) {
      } else {
        m_rdo_x->push_back(gpos.x());
        m_rdo_y->push_back(gpos.y());
        m_rdo_z->push_back(gpos.z());
      }*/
    }
  }

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_sTgcRdo::clear() {

  m_rdo_stationName->clear();
  m_rdo_stationEta->clear();
  m_rdo_stationPhi->clear();
  m_rdo_multiplet->clear();
  m_rdo_gasGap->clear();
  m_rdo_channel->clear();
  m_rdo_type->clear();  
  m_rdo_x->clear();  
  m_rdo_y->clear();  
  m_rdo_z->clear();  

  return;
}

/* eof */
