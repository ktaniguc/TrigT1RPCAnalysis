#ifndef L1TGCNTUPLE_MUON_h
#define L1TGCNTUPLE_MUON_h

#include "GaudiKernel/ToolHandle.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODMuon/MuonContainer.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

template <class T> class ToolHandle;

namespace CP { class IMuonSelectionTool; }

class L1TGCNtuple_muon {
  public:
    L1TGCNtuple_muon(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_muon();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:

    uint32_t             m_mu_n;
    std::vector<float>   *m_mu_pt;
    std::vector<float>   *m_mu_eta;
    std::vector<float>   *m_mu_phi;
    std::vector<float>   *m_mu_m;
    std::vector<int>     *m_mu_charge;

    std::vector<int>      *m_mu_author;
    std::vector<uint16_t> *m_mu_allAuthors;
    std::vector<int>      *m_mu_muonType;
    
    std::vector<float>   *m_mu_etcone20;
    std::vector<float>   *m_mu_etcone30;
    std::vector<float>   *m_mu_etcone40;
    std::vector<float>   *m_mu_ptcone20;
    std::vector<float>   *m_mu_ptcone30;
    std::vector<float>   *m_mu_ptcone40;
    
    std::vector<int>     *m_mu_isPassedMCP;
    std::vector<int>     *m_mu_quality;
    
    std::vector<float>   *m_mu_trackfitchi2;
    std::vector<float>   *m_mu_trackfitndof;

    std::vector<float>   *m_mu_cb_d0;
    std::vector<float>   *m_mu_cb_z0;
    std::vector<float>   *m_mu_cb_phi0;
    std::vector<float>   *m_mu_cb_theta;
    std::vector<float>   *m_mu_cb_qOverP;
    std::vector<float>   *m_mu_cb_vx;
    std::vector<float>   *m_mu_cb_vy;
    std::vector<float>   *m_mu_cb_vz;
    
    std::string m_muons;
    ToolHandle<CP::IMuonSelectionTool> m_mtool;

    std::vector<std::pair<xAOD::Muon::ParamDef, std::vector<float>*> > m_paramVars1; // float
    std::vector<std::pair<xAOD::Muon::ParamDef, std::vector<int>*> >   m_paramVars2; // int
    std::vector<std::pair<xAOD::SummaryType, std::vector<int>*> > m_trackVars;
    std::vector<std::pair<xAOD::MuonSummaryType, std::vector<int>*> > m_muonVars;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_muon_h
/* eof */
