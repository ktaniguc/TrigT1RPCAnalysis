#ifndef L1TGCNTUPLE_MDTPRD_h
#define L1TGCNTUPLE_MDTPRD_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class L1TGCNtuple_mdtPrd {
  public:
    L1TGCNtuple_mdtPrd(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_mdtPrd();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:

    uint32_t           m_prd_n;  
    std::vector<float> *m_prd_x;
    std::vector<float> *m_prd_y;
    std::vector<float> *m_prd_z;
    std::vector<int>   *m_prd_adc;
    std::vector<int>   *m_prd_tdc;
    std::vector<int>   *m_prd_status;
    std::vector<float> *m_prd_drift_radius;
    std::vector<float> *m_prd_drift_radius_error;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_mdtPrd_h
/* eof */
