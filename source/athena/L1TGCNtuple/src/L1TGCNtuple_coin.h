#ifndef L1TGCNTUPLE_COIN_h
#define L1TGCNTUPLE_COIN_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class MuonIdHelper;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Muon { class TgcCoinData; }

class L1TGCNtuple_coin {
  public:
    L1TGCNtuple_coin(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_coin();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    StatusCode fillTgcCoinData(const Muon::TgcCoinData* data, const int bunch);

    uint32_t           m_coin_n; 
    std::vector<float> *m_coin_x_In;
    std::vector<float> *m_coin_y_In;
    std::vector<float> *m_coin_z_In;
    std::vector<float> *m_coin_x_Out;
    std::vector<float> *m_coin_y_Out;
    std::vector<float> *m_coin_z_Out;
    std::vector<float> *m_coin_width_In;
    std::vector<float> *m_coin_width_Out;
    std::vector<float> *m_coin_width_R;
    std::vector<float> *m_coin_width_Phi;

    std::vector<int> *m_coin_isAside;
    std::vector<int> *m_coin_isForward;
    std::vector<int> *m_coin_isStrip;
    std::vector<int> *m_coin_isInner;
    std::vector<int> *m_coin_isPositiveDeltaR;

    std::vector<int> *m_coin_type;
    std::vector<int> *m_coin_trackletId;
    std::vector<int> *m_coin_trackletIdStrip;
    std::vector<int> *m_coin_phi;
    std::vector<int> *m_coin_roi;
    std::vector<int> *m_coin_pt;
    std::vector<int> *m_coin_delta;
    std::vector<int> *m_coin_sub;
    std::vector<int> *m_coin_veto;
    std::vector<int> *m_coin_bunch;
    std::vector<int> *m_coin_inner;
    
    std::string m_tgcCoinData;
    std::string m_tgcCoinDataPriorBC;
    std::string m_tgcCoinDataNextBC;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_prd_h
/* eof */
