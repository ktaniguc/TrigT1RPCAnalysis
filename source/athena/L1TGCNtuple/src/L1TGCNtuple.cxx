/*====================================================================
 * declarations
 *====================================================================*/


/*====================================================================*
 * headers
 *====================================================================*/
#include "L1TGCNtuple/L1TGCNtuple.h"

#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "StoreGate/StoreGateSvc.h"
#include "AthenaKernel/errorcheck.h"

#include "TTree.h"
#include "TLorentzVector.h"

#include "src/L1TGCNtuple_event.h"
#include "src/L1TGCNtuple_vertex.h"
#include "src/L1TGCNtuple_tgcPrd.h"
#include "src/L1TGCNtuple_rpcPrd.h"
#include "src/L1TGCNtuple_mdtPrd.h"
#include "src/L1TGCNtuple_coin.h"
#include "src/L1TGCNtuple_murcv.h"
#include "src/L1TGCNtuple_tileCell.h"
#include "src/L1TGCNtuple_roi.h"
#include "src/L1TGCNtuple_muon.h"
#include "src/L1TGCNtuple_museg.h"
#include "src/L1TGCNtuple_trigInfo.h"
#include "src/L1TGCNtuple_truth.h"
#include "src/L1TGCNtuple_trkRecord.h"
#include "src/L1TGCNtuple_muctpi.h"
#include "src/L1TGCNtuple_hierarchy.h"
#include "src/L1TGCNtuple_ext.h"
#include "src/L1TGCNtuple_sTgcRdo.h"

#include "src/L1TgcSkim.h"

/*====================================================================*
 * implementation
 *====================================================================*/

// ---------------------------------------------------------------------
// Constructors and Destructor
// ---------------------------------------------------------------------
L1TGCNtuple::L1TGCNtuple(const std::string& name, 
                         ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator),
    m_thistSvc("THistSvc", name),
    m_ttree(0),

    m_ntupleEvent(new L1TGCNtuple_event(msgSvc(), name)),
    m_ntupleVertex(new L1TGCNtuple_vertex(msgSvc(), name)),
    m_ntupleTgcPrd(new L1TGCNtuple_tgcPrd(msgSvc(), name)),
    m_ntupleRpcPrd(new L1TGCNtuple_rpcPrd(msgSvc(), name)),
    m_ntupleMdtPrd(new L1TGCNtuple_mdtPrd(msgSvc(), name)),
    m_ntupleSTgcRdo(new L1TGCNtuple_sTgcRdo(msgSvc(), name)),
    m_ntupleCoin(new L1TGCNtuple_coin(msgSvc(), name)),
//    m_ntupleMurcv(new L1TGCNtuple_murcv(msgSvc(), name)),
    m_ntupleTileCell(new L1TGCNtuple_tileCell(msgSvc(), name)),
    m_ntupleRoi(new L1TGCNtuple_roi(msgSvc(), name)),
    m_ntupleMuon(new L1TGCNtuple_muon(msgSvc(), name)),
    m_ntupleMuseg(new L1TGCNtuple_museg(msgSvc(), name)),
    m_ntupleTrigInfo(new L1TGCNtuple_trigInfo(msgSvc(), name)),
    m_ntupleTruth(new L1TGCNtuple_truth(msgSvc(), name)),
    m_ntupleTrkRecord(new L1TGCNtuple_trkRecord(msgSvc(), name)),
    m_ntupleMuctpi(new L1TGCNtuple_muctpi(msgSvc(), name)),
    m_ntupleHierarchy(new L1TGCNtuple_hierarchy(msgSvc(), name)),
//    m_ntupleExtBias(new L1TGCNtuple_ext(msgSvc(), name, "BIASED")),
//    m_ntupleExtUnbias(new L1TGCNtuple_ext(msgSvc(), name, "UNBIASED")),
    
    m_l1TgcSkim("L1TgcSkim")
{
  declareProperty("message", m_message = "You cannot always get what you want.");
  declareProperty("THistSvc", m_thistSvc);
  declareProperty("isData", m_isData =true);
  declareProperty("isCosmic", m_isCosmic =false);
  declareProperty("isESD", m_isESD = true);
  declareProperty("isAOD", m_isAOD = false);
  declareProperty("doTileCell", m_doTileCell = false);
  declareProperty("doNSW", m_doNSW = false);
  declareProperty("TMDBConstants", m_TMDBConstants = "TMDB_ADC_to_MeV_20151015.txt");
}

L1TGCNtuple::~L1TGCNtuple() {
  delete m_ntupleEvent;
  delete m_ntupleVertex;
  delete m_ntupleTgcPrd;
  delete m_ntupleRpcPrd;
  delete m_ntupleMdtPrd;
  delete m_ntupleSTgcRdo;
  delete m_ntupleCoin;
//  delete m_ntupleMurcv;
  delete m_ntupleTileCell;
  delete m_ntupleHierarchy;
  delete m_ntupleRoi;
  delete m_ntupleMuon;
  delete m_ntupleMuseg;
  delete m_ntupleTrigInfo;
  delete m_ntupleTruth;
  delete m_ntupleTrkRecord;
  delete m_ntupleMuctpi;
//  delete m_ntupleExtBias;
//  delete m_ntupleExtUnbias;
}

// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------

StatusCode L1TGCNtuple::initialize() {
  ATH_MSG_DEBUG("initialize:-");

  m_ttree = new TTree("physics", "physics");
  
  // set parameters
//  CHECK(m_ntupleMurcv->setConstants(m_TMDBConstants));
  
  // ntuple booking
  CHECK(m_ntupleEvent->book(m_ttree));
  CHECK(m_ntupleRoi->book(m_ttree));
  CHECK(m_ntupleMuon->book(m_ttree));
  CHECK(m_ntupleMuseg->book(m_ttree));
//  CHECK(m_ntupleExtBias->book(m_ttree));
//  CHECK(m_ntupleExtUnbias->book(m_ttree));
  CHECK(m_ntupleTrigInfo->book(m_ttree));
 
  if (!m_isCosmic) {
    CHECK(m_ntupleVertex->book(m_ttree));
  }

  if (m_isESD) {
    CHECK(m_ntupleTgcPrd->book(m_ttree));
    CHECK(m_ntupleRpcPrd->initialize());
    CHECK(m_ntupleRpcPrd->book(m_ttree));
    CHECK(m_ntupleMdtPrd->book(m_ttree));
    CHECK(m_ntupleCoin->book(m_ttree));
//    CHECK(m_ntupleMurcv->book(m_ttree, detStore()));
    CHECK(m_ntupleHierarchy->book(m_ttree));
    CHECK(m_ntupleMuctpi->book(m_ttree));
  }

  if (!m_isData) {
    CHECK(m_ntupleTruth->book(m_ttree));
    CHECK(m_ntupleTrkRecord->book(m_ttree));
  }
  
  if (m_doTileCell) {
    CHECK(m_ntupleTileCell->book(m_ttree, detStore()));
  }
  
  if (m_doNSW) {
    CHECK(m_ntupleSTgcRdo->book(m_ttree, detStore()));
  }

  // register
  CHECK(m_thistSvc->regTree("/L1TGCNtuple/", m_ttree));

  ATH_MSG_DEBUG("initialize:+");
  return StatusCode::SUCCESS;
}

StatusCode L1TGCNtuple::finalize() {
  ATH_MSG_DEBUG("finalize:-");
  
  ATH_MSG_DEBUG("finalize:+");
  
  return StatusCode::SUCCESS;
}

void
L1TGCNtuple::clear() {
  ATH_MSG_DEBUG("clear:-");

  m_ntupleEvent->clear();
  m_ntupleVertex->clear();
  m_ntupleTgcPrd->clear();
  m_ntupleRpcPrd->clear();
  m_ntupleMdtPrd->clear();
  m_ntupleSTgcRdo->clear();
  m_ntupleCoin->clear();
//  m_ntupleMurcv->clear();
  m_ntupleTileCell->clear();
  m_ntupleHierarchy->clear();
  m_ntupleRoi->clear();
  m_ntupleMuon->clear();
  m_ntupleMuseg->clear();
//  m_ntupleExtBias->clear();
//  m_ntupleExtUnbias->clear();
  m_ntupleTrigInfo->clear();
  m_ntupleMuctpi->clear();
  m_ntupleTruth->clear();
  m_ntupleTrkRecord->clear();

  ATH_MSG_DEBUG("clear:+");
}
 
StatusCode L1TGCNtuple::execute() {
  ATH_MSG_DEBUG("execute:-");
  
  if (m_isData && (!m_l1TgcSkim->passL1TgcSkim())) 
    return StatusCode::SUCCESS;
  std::cout << "L1TGCskim end" << std::endl;

  clear();
  std::cout << "clear end" << std::endl;

  ServiceHandle<StoreGateSvc> storeGate = evtStore();
  std::cout << "got storeGate" << std::endl;
  
  // fill
  std::cout << "start fill the info" << std::endl;
  CHECK(m_ntupleEvent->fill(storeGate));
  std::cout << "fill ntupleEvent info to ntuple succeed" << std::endl; 
  CHECK(m_ntupleRoi->fill(storeGate));
  std::cout << "fill roi info to ntuple succeed" << std::endl; 
  CHECK(m_ntupleMuon->fill(storeGate));
  std::cout << "fill ntupleMuon info to ntuple succeed" << std::endl; 
  CHECK(m_ntupleMuseg->fill(storeGate));
  std::cout << "fill Museg info to ntuple succeed" << std::endl; 
//  CHECK(m_ntupleExtBias->fill(storeGate));
//  CHECK(m_ntupleExtUnbias->fill(storeGate));
  CHECK(m_ntupleTrigInfo->fill(storeGate));
  std::cout << "fill ntupleTrig info to ntuple succeed" << std::endl; 

  if (!m_isCosmic) {
    CHECK(m_ntupleVertex->fill(storeGate));
  }

  if (m_isESD) {
    CHECK(m_ntupleTgcPrd->fill(storeGate));
    std::cout << "fill TgcPrd info to ntuple succeed" << std::endl; 
    CHECK(m_ntupleRpcPrd->fill(storeGate));
    std::cout << "fill RpcPrd info to ntuple succeed" << std::endl; 
    CHECK(m_ntupleMdtPrd->fill(storeGate));
    std::cout << "fill MdtPrd info to ntuple succeed" << std::endl; 
    CHECK(m_ntupleCoin->fill(storeGate));
    std::cout << "fill ntupleCoin info to ntuple succeed" << std::endl; 
//    CHECK(m_ntupleMurcv->fill(storeGate, m_isData));
//    std::cout << "fill Murcv info to ntuple succeed" << std::endl; 
    CHECK(m_ntupleHierarchy->fill(storeGate));
    std::cout << "fill Hierarchy info to ntuple succeed" << std::endl; 
    CHECK(m_ntupleMuctpi->fill(storeGate));
    std::cout << "fill Muctpi info to ntuple succeed" << std::endl; 
  }

  if (!m_isData) {
    CHECK(m_ntupleTruth->fill(storeGate));
    CHECK(m_ntupleTrkRecord->fill(storeGate));
  }
  
  if (m_doTileCell) {
    CHECK(m_ntupleTileCell->fill(storeGate));
  }
  
  if (m_doNSW) {
    CHECK(m_ntupleSTgcRdo->fill(storeGate));
  }
  std::cout << "end filling the info" << std::endl;

  m_ttree->Fill();

  ATH_MSG_DEBUG("execute:+");
  
  return StatusCode::SUCCESS;
}
