#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigDecisionTool/ChainGroup.h"
#include "xAODMuon/MuonContainer.h"

#include "TTree.h"

#include "src/L1TGCNtuple_trigInfo.h"


/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_trigInfo::L1TGCNtuple_trigInfo(IMessageSvc* svc,
                                 const std::string& name)
  : m_trig_n(0),
    m_chain(new std::vector<std::string>),
    m_isPassed(new std::vector<int>),
    m_nTracks(new std::vector<int>),

    m_typeVec(new std::vector<std::vector<int> >),
    m_ptVec(new std::vector<std::vector<float> >),
    m_etaVec(new std::vector<std::vector<float> >),
    m_phiVec(new std::vector<std::vector<float> >),
    m_qVec(new std::vector<std::vector<int> >),
    m_l1RoiWordVec(new std::vector<std::vector<int> >),

    m_tdt("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_pattern("(HLT_[0-9]*mu.*)|(HLT_noalg_L1[0-9]*MU.*)|(HLT_noalg_L1LowLumi)|(L1_[0-9]*MU.*)"),
    msg(svc, name) {
}

L1TGCNtuple_trigInfo::~L1TGCNtuple_trigInfo() {

  delete m_chain;
  delete m_isPassed;
  delete m_nTracks;

  delete m_typeVec;
  delete m_ptVec;
  delete m_etaVec;
  delete m_phiVec;
  delete m_qVec;
  delete m_l1RoiWordVec;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_trigInfo::book(TTree* ttree) {

  CHECK(m_tdt.retrieve());

  ttree->Branch("trigger_info_n", &m_trig_n, "trigger_info_n/I");
  ttree->Branch("trigger_info_chain", &m_chain);
  ttree->Branch("trigger_info_isPassed", &m_isPassed);
  ttree->Branch("trigger_info_nTracks", &m_nTracks);
  ttree->Branch("trigger_info_typeVec", &m_typeVec);
  ttree->Branch("trigger_info_ptVec", &m_ptVec);
  ttree->Branch("trigger_info_etaVec", &m_etaVec);
  ttree->Branch("trigger_info_phiVec", &m_phiVec);
  //ttree->Branch("trigger_info_qVec", &m_qVec);
  //ttree->Branch("trigger_info_l1RoiWordVec", &m_l1RoiWordVec);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_trigInfo::fill(ServiceHandle<StoreGateSvc>& /*storeGateSvc*/) {

  const Trig::ChainGroup* cg = m_tdt->getChainGroup(m_pattern);
  std::vector<std::string> chains = cg->getListOfTriggers();

  for (size_t ii = 0; ii < chains.size(); ii++) {

    const std::string chain = chains.at(ii);

    std::vector<int> typeVec;
    std::vector<float> ptVec, etaVec, phiVec;

    const Trig::FeatureContainer fc = m_tdt->features(chain);

    const std::vector<Trig::Feature<xAOD::MuonContainer> > MuFeatureContainers 
      = fc.get<xAOD::MuonContainer>();

    for (auto mucont : MuFeatureContainers) {
      for (auto mu : *mucont.cptr()) {

        typeVec.push_back(mu->muonType());
        ptVec.push_back(mu->pt());
        etaVec.push_back(mu->eta());
        phiVec.push_back(mu->phi());
      }
    }
   
    m_nTracks->push_back(typeVec.size()); 
    m_chain->push_back(chain);
    m_isPassed->push_back(m_tdt->isPassed(chain));
    m_typeVec->push_back(typeVec);
    m_ptVec->push_back(ptVec);
    m_etaVec->push_back(etaVec);
    m_phiVec->push_back(phiVec);

    m_trig_n++;
  }

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_trigInfo::clear() {
  
  m_trig_n = 0;

  m_chain->clear();
  m_isPassed->clear();
  m_nTracks->clear();

  m_typeVec->clear();
  m_ptVec->clear();
  m_etaVec->clear();
  m_phiVec->clear();
  m_qVec->clear();
  m_l1RoiWordVec->clear();
  
  return;
}

/* eof */
