#ifndef L1TGCSKIM_h
#define L1TGCSKIM_h

#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"

namespace Trig { class TrigDecisionTool; }

class ISvcLocator;
class ITHistSvc;
class TH1I;

static const InterfaceID IID_L1TgcSkim("L1TgcSkim", 1, 0);

class L1TgcSkim : public AthAlgTool {
  public:
    L1TgcSkim(const std::string& type,
              const std::string& name,
              const IInterface* parent);
    ~L1TgcSkim();
      
    virtual StatusCode initialize();
    virtual StatusCode finalize();

    static const InterfaceID& interfaceID() { return IID_L1TgcSkim; };
    
    bool passL1TgcSkim();

  private:
    bool passTandPSelection();
    bool passMuonSelection();

    ServiceHandle<ITHistSvc> m_thistSvc;
    ToolHandle<Trig::TrigDecisionTool> m_tdt;

    bool m_dumpAll;
    std::string m_passThroughTrigPattern;
    std::string m_trigPattern;
    float m_tandp_tag_pt;
    float m_tandp_tag_type;
    float m_tandp_probe_pt;
    float m_tandp_probe_type;
    float m_tandp_mass;
    int m_muons_n;
    int m_muons_type;
    float m_muons_pt;
    int   m_muons_prescale;

    TH1I* m_hNEvents;
    int nEventsPrescaled;
    int nEventsProcessed;
    int nEventsDumped; 
};

#endif
/* eof */
