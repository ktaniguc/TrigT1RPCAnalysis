#ifndef L1TGCNTUPLE_EXT_h
#define L1TGCNTUPLE_EXT_h

#include "GaudiKernel/ToolHandle.h"
#include "GeoPrimitives/GeoPrimitives.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class ITrigEffJpsiTools;
class MuonIdHelper;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Muon { class MuonIdHelperTool; }

class L1TgcTrkExtrapolator;

class L1TGCNtuple_ext {
  public:
    L1TGCNtuple_ext(IMessageSvc* svc,
                    const std::string& name,
                    const std::string type="BIASED");
    virtual ~L1TGCNtuple_ext();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    typedef std::multimap<int, Amg::Vector3D>  Target;

    enum {
      UNDEF,
      COMBINED,
      INNER_EXTRAPOLATED,
      INDET,
      BIASED,
      UNBIASED
    };

    StatusCode fillTargetDistanceTgc(ServiceHandle<StoreGateSvc>& storeGate,
        Target& target);

    ToolHandle<ITrigEffJpsiTools> m_trigEffJpsiTools;
    L1TgcTrkExtrapolator* m_l1TgcTrkExtrapolator;

    uint32_t         m_ext_n;
    std::vector<int> *m_ext_type;
    std::vector<int> *m_ext_index;
    std::vector<int> *m_ext_size;

    std::vector<std::vector<int> > *m_ext_targetVec;
    std::vector<std::vector<float> > *m_ext_targetDistanceVec;
    std::vector<std::vector<float> > *m_ext_targetEtaVec;
    std::vector<std::vector<float> > *m_ext_targetPhiVec;
    std::vector<std::vector<float> > *m_ext_targetDeltaEtaVec;
    std::vector<std::vector<float> > *m_ext_targetDeltaPhiVec;
    std::vector<std::vector<float> > *m_ext_targetPxVec;
    std::vector<std::vector<float> > *m_ext_targetPyVec;
    std::vector<std::vector<float> > *m_ext_targetPzVec;
   
    std::string m_extType;  
    std::string m_muons;

    ToolHandle<Muon::MuonIdHelperTool> m_idHelper;

    // target plane
    double m_M1_Z;
    double m_M2_Z;
    double m_M3_Z;
    double m_EI_Z;
    double m_FI_Z;
    
    double m_MS_IN_Z;
    double m_MS_OUT_Z;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_ext_h
/* eof */
