#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackingPrimitives.h"

#include "TrigEffJpsiTools/ITrigEffJpsiTools.h"
#include "MuonPrepRawData/TgcPrepDataCollection.h"
#include "MuonPrepRawData/TgcPrepDataContainer.h"
#include "MuonPrepRawData/TgcPrepData.h"
#include "MuonIdHelpers/MuonIdHelperTool.h"

#include "TTree.h"

#include "src/L1TGCNtuple_ext.h"
#include "src/L1TgcTrkExtrapolator.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_ext::L1TGCNtuple_ext(IMessageSvc* svc,
                                 const std::string& name,
                                 const std::string  type)
  : m_trigEffJpsiTools("TrigEffJpsiTools"),  
    m_l1TgcTrkExtrapolator(new L1TgcTrkExtrapolator(svc, name)),
    
    m_ext_n(0),
    m_ext_type(new std::vector<int>),
    m_ext_index(new std::vector<int>),
    m_ext_size(new std::vector<int>),
    m_ext_targetVec(new std::vector<std::vector<int> >),
    m_ext_targetDistanceVec(new std::vector<std::vector<float> >),
    m_ext_targetEtaVec(new std::vector<std::vector<float> >),
    m_ext_targetPhiVec(new std::vector<std::vector<float> >),
    m_ext_targetDeltaEtaVec(new std::vector<std::vector<float> >),
    m_ext_targetDeltaPhiVec(new std::vector<std::vector<float> >),
    m_ext_targetPxVec(new std::vector<std::vector<float> >),
    m_ext_targetPyVec(new std::vector<std::vector<float> >),
    m_ext_targetPzVec(new std::vector<std::vector<float> >),

    m_extType(type),
    m_muons("Muons"),

    m_idHelper("Muon::MuonIdHelperTool/MuonIdHelperTool"),

    m_M1_Z(13605.0),
    m_M2_Z(14860.0),
    m_M3_Z(15280.0),
    m_EI_Z(7425.0),
    m_FI_Z(7030.0),
    
    m_MS_IN_Z(6735.0),
    m_MS_OUT_Z(22030.0),

    msg(svc, name) {
}

L1TGCNtuple_ext::~L1TGCNtuple_ext() {

  delete m_ext_type;
  delete m_ext_index;
  delete m_ext_size;
  delete m_ext_targetVec;
  delete m_ext_targetDistanceVec;
  delete m_ext_targetEtaVec;
  delete m_ext_targetPhiVec;
  delete m_ext_targetDeltaEtaVec;
  delete m_ext_targetDeltaPhiVec;
  delete m_ext_targetPxVec;
  delete m_ext_targetPyVec;
  delete m_ext_targetPzVec;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_ext::book(TTree* ttree) {
  
  std::string prefix = "unknown";
  if (m_extType == "BIASED") prefix = "bias";  
  if (m_extType == "UNBIASED") prefix = "ubias";  

  ttree->Branch(("ext_mu_" +prefix+ "_n").c_str(), &m_ext_n, ("ext_mu_" +prefix+ "_n/I").c_str());
  ttree->Branch(("ext_mu_" +prefix+ "_type").c_str(), &m_ext_type);
  ttree->Branch(("ext_mu_" +prefix+ "_index").c_str(), &m_ext_index);
  ttree->Branch(("ext_mu_" +prefix+ "_size").c_str(), &m_ext_size);
  ttree->Branch(("ext_mu_" +prefix+ "_targetVec").c_str(), &m_ext_targetVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetDistanceVec").c_str(), &m_ext_targetDistanceVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetEtaVec").c_str(), &m_ext_targetEtaVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetPhiVec").c_str(), &m_ext_targetPhiVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetDeltaEtaVec").c_str(), &m_ext_targetDeltaEtaVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetDeltaPhiVec").c_str(), &m_ext_targetDeltaPhiVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetPxVec").c_str(), &m_ext_targetPxVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetPyVec").c_str(), &m_ext_targetPyVec);
  ttree->Branch(("ext_mu_" +prefix+ "_targetPzVec").c_str(), &m_ext_targetPzVec);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_ext::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  // fill target plane
  Target target;
  StatusCode sc = fillTargetDistanceTgc(storeGateSvc, target);

  if (sc.isFailure()) return StatusCode::SUCCESS;

  const xAOD::MuonContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, m_muons));
   
  xAOD::MuonContainer::const_iterator muon_it;
    
  int index = 0; 
  for (muon_it = container->begin(); muon_it != container->end(); muon_it++) {
    const xAOD::Muon* muon = *muon_it;

    // select track
    const xAOD::TrackParticle* track = 0;
    int type = -100;
    if (muon->combinedTrackParticleLink()) {
      type = COMBINED;
      if (m_extType == "BIASED") {
        const ElementLink< xAOD::TrackParticleContainer >& el = muon->combinedTrackParticleLink();
        track = *el;
      } else {
        if (muon->inDetTrackParticleLink()) {
          const ElementLink< xAOD::TrackParticleContainer >& el = muon->inDetTrackParticleLink();
          track = *el;
        }
      }
    
    } else if (muon->inDetTrackParticleLink()) {
      type = INDET;
      const ElementLink< xAOD::TrackParticleContainer >& el = muon->inDetTrackParticleLink();
      track = *el;   
    
    } /*else if (muon->extrapolatedMuonSpectrometerTrackParticleLink()) {
      type = INNER_EXTRAPOLATED;
      const ElementLink< xAOD::TrackParticleContainer >& el = extrapolatedMuonSpectrometerTrackParticleLink();
      track = *el;   
    }*/

    // extrapolation
    std::vector<int> vDetector;
    std::vector<float> vDistance, vEta, vPhi, vDeltaEta, vDeltaPhi, vPx, vPy, vPz;
    size_t nn = 0;
    if (track) {
      typedef std::map<std::string, double> DistanceMap;
      DistanceMap distanceZ;
      DistanceMap distanceR;
      double distance = -1.e30;
  
      Target::const_iterator cit_target;
      for (cit_target = target.begin(); cit_target != target.end(); cit_target++) {
        std::stringstream ss;
        ss.setf(std::ios::scientific);
        std::pair<DistanceMap::iterator, bool> rc;
    
        bool extrapolate = false;
        int detector = -1;

        if (cit_target->first == ITrigEffJpsiTools::TGC) {
          extrapolate = ((cit_target->second.z() * track->eta()) > 0.) && (fabs(track->eta()) > 0.9);
          detector = ITrigEffJpsiTools::TGC;
          distance = cit_target->second.z();
          ss << distance;
          rc = distanceZ.insert(DistanceMap::value_type(ss.str(), distance));
        }
      
        if (rc.second == false) continue;
        if (extrapolate == false) continue;

        Amg::Vector2D etaDeta(-1.e30, -1.e30);
        Amg::Vector2D phiDphi(-1.e30, -1.e30);
        Amg::Vector3D mom(-1.e30, -1.e30, -1.e30);
          
        if (m_extType == "BIASED") {
          m_l1TgcTrkExtrapolator->extrapolateFromMDT(track, cit_target->second, detector,
                                                       etaDeta, phiDphi, mom);

        } else if (m_extType == "UNBIASED") {
          //m_trigEffJpsiTools->extrapolateToPoint(track, &cit_target->second, detector,
           //                                        &etaDeta, &phiDphi, &mom);
        }
        
        vDetector.push_back(detector);
        vDistance.push_back(distance);
        
        vEta.push_back(etaDeta.x());
        vPhi.push_back(phiDphi.x());
        vDeltaEta.push_back(etaDeta.y());
        vDeltaPhi.push_back(phiDphi.y());
        vPx.push_back(mom.x());
        vPy.push_back(mom.y());
        vPz.push_back(mom.z());

        nn++;
      }
    }
    
    m_ext_n++;

    m_ext_type->push_back(type);
    m_ext_index->push_back(index++);

    m_ext_size->push_back(nn);
    m_ext_targetVec->push_back(vDetector);
    m_ext_targetDistanceVec->push_back(vDistance);
    m_ext_targetEtaVec->push_back(vEta);
    m_ext_targetPhiVec->push_back(vPhi);
    m_ext_targetDeltaEtaVec->push_back(vDeltaEta);
    m_ext_targetDeltaPhiVec->push_back(vDeltaPhi);
    m_ext_targetPxVec->push_back(vPx);
    m_ext_targetPyVec->push_back(vPy);
    m_ext_targetPzVec->push_back(vPz);
  }

  return StatusCode::SUCCESS;
}


StatusCode
L1TGCNtuple_ext::fillTargetDistanceTgc(ServiceHandle<StoreGateSvc>& storeGateSvc,
    Target& target) {

  const TgcIdHelper& tgcIdHelper = m_idHelper->tgcIdHelper();

  const Muon::TgcPrepDataContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, "TGC_MeasurementsAllBCs");

  if (sc.isFailure()) return StatusCode::FAILURE;

  Muon::TgcPrepDataContainer::const_iterator tpdc_it;
  Muon::TgcPrepDataCollection::const_iterator cit;

  for (tpdc_it = container->begin(); tpdc_it != container->end(); tpdc_it++) {
    for (cit = (*tpdc_it)->begin(); cit != (*tpdc_it)->end(); cit++) {
      const Muon::TgcPrepData *data = *cit;
      const MuonGM::TgcReadoutElement* element = data->detectorElement();
      const Identifier id = data->identify();
      const int gasGap = tgcIdHelper.gasGap(id);
      const int channel = tgcIdHelper.channel(id);
      const bool isStrip = tgcIdHelper.isStrip(id);
      Amg::Vector3D pos = isStrip ? element->stripPos(gasGap, channel) : element->gangPos(gasGap, channel);
      target.insert(Target::value_type(ITrigEffJpsiTools::TGC, pos));
    }
  }

  // constant target
  Amg::Vector3D m1a(0, 0, m_M1_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m1a));
  Amg::Vector3D m2a(0, 0, m_M2_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m2a));
  Amg::Vector3D m3a(0, 0, m_M3_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m3a));
  Amg::Vector3D eia(0, 0, m_EI_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, eia));
  Amg::Vector3D fia(0, 0, m_FI_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, fia));
  Amg::Vector3D m1c(0, 0, -m_M1_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m1c));
  Amg::Vector3D m2c(0, 0, -m_M2_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m2c));
  Amg::Vector3D m3c(0, 0, -m_M3_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, m3c));
  Amg::Vector3D eic(0, 0, -m_EI_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, eic));
  Amg::Vector3D fic(0, 0, -m_FI_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, fic));

  Amg::Vector3D ina(0, 0, m_MS_IN_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, ina));
  Amg::Vector3D outa(0, 0, m_MS_OUT_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, outa));
  Amg::Vector3D inc(0, 0, -m_MS_IN_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, inc));
  Amg::Vector3D outc(0, 0, -m_MS_OUT_Z);
  target.insert(Target::value_type(ITrigEffJpsiTools::TGC, outc));
  
  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_ext::clear() {
  m_ext_n = 0;  

  m_ext_type->clear();
  m_ext_index->clear();
  m_ext_size->clear();
  m_ext_targetVec->clear();
  m_ext_targetDistanceVec->clear();
  m_ext_targetEtaVec->clear();
  m_ext_targetPhiVec->clear();
  m_ext_targetDeltaEtaVec->clear();
  m_ext_targetDeltaPhiVec->clear();
  m_ext_targetPxVec->clear();
  m_ext_targetPyVec->clear();
  m_ext_targetPzVec->clear();

  return;
}

/* eof */
