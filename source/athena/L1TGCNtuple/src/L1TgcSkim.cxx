#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/ITHistSvc.h"
#include "AthenaKernel/errorcheck.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "xAODMuon/MuonContainer.h"

#include "TH1I.h"

#include "L1TgcSkim.h"

L1TgcSkim::L1TgcSkim(const std::string& type,
                     const std::string& name,
                     const IInterface* parent)
  : AthAlgTool(type, name, parent),
    m_thistSvc("THistSvc", name),
    m_tdt("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_hNEvents(0),
    nEventsPrescaled(0),
    nEventsProcessed(0),
    nEventsDumped(0) 
{
  declareInterface<L1TgcSkim>(this);

  declareProperty("DumpAll", m_dumpAll=true, "set it to true for dump all the events");
  declareProperty("PassThroughTrigPattern", m_passThroughTrigPattern=".*", "set pass-through trigger pattern");
  declareProperty("TrigPattern", m_trigPattern=".*", "set trigger pattern");
  declareProperty("Tandp_tag_pt", m_tandp_tag_pt=0, "set pT threshold of tag muons");
  declareProperty("Tandp_tag_type", m_tandp_tag_type=-1, "set type of tag muons");
  declareProperty("Tandp_probe_pt", m_tandp_probe_pt=0, "set pT threshold of probe muons");
  declareProperty("Tandp_probe_type", m_tandp_probe_type=-1, "set pT threshold of probe muons");
  declareProperty("Tandp_mass", m_tandp_mass=0, "set mass threshold of tag and probe system");
  declareProperty("Muons_n", m_muons_n=0, "set number of muons");
  declareProperty("Muons_type", m_muons_type=-1, "set type of muons");
  declareProperty("Muons_pt", m_muons_pt=0, "set pT threshold of muons");
  declareProperty("Muons_prescale", m_muons_prescale=1, "set prescale");
}


L1TgcSkim::~L1TgcSkim()
{ 
}


StatusCode
L1TgcSkim::initialize()
{
  CHECK(m_thistSvc.retrieve());
  m_hNEvents = new TH1I("hNEvents", "hNEvents", 2, -0.5, 1.5);
  CHECK(m_thistSvc->regHist("/L1TGCNtuple/counter/", m_hNEvents));
  CHECK(m_tdt.retrieve());

  nEventsPrescaled = 0;
  nEventsProcessed = 0;
  nEventsDumped = 0;
  
  return StatusCode::SUCCESS;
}


StatusCode
L1TgcSkim::finalize()
{
  ATH_MSG_INFO("--- Summary of L1TGCNtuple event skimming ---");
  ATH_MSG_INFO("nEventsProcessed = " << nEventsProcessed << " events");
  ATH_MSG_INFO("nEventsDumped    = " << nEventsDumped << " events");
  
  TAxis *axis = (TAxis*)m_hNEvents->GetXaxis();
  axis->SetBinLabel(1, "nEventsProcessed");
  axis->SetBinLabel(2, "nEventsDumped");
  ATH_MSG_INFO("---------------------------------------------");
  
  m_hNEvents->SetBinContent(1, nEventsProcessed);
  m_hNEvents->SetBinContent(2, nEventsDumped);
  
  return StatusCode::SUCCESS;
}

bool
L1TgcSkim::passL1TgcSkim()
{
  nEventsProcessed++;

  if (m_dumpAll) {
    //setFilterPassed(true);
    nEventsDumped++;
    return true;
  }

  if (m_tdt->isPassed(m_passThroughTrigPattern)) {
    nEventsDumped++;
    return true;
  }

  if (! m_tdt->isPassed(m_trigPattern)) {
    return false;
  }

  if (passTandPSelection()) {
    nEventsDumped++;
    return true;
  }
  
  if (passMuonSelection()) {
    nEventsPrescaled++;
    if (nEventsPrescaled % m_muons_prescale != 0) return false;

    nEventsDumped++;
    return true;
  }

  return false;
}


bool 
L1TgcSkim::passTandPSelection()
{
  const xAOD::MuonContainer* container = 0;
  CHECK(evtStore()->retrieve(container, "Muons"));

  xAOD::MuonContainer::const_iterator muon_tag_it;
  xAOD::MuonContainer::const_iterator muon_probe_it;

  // tag
  for (muon_tag_it = container->begin(); muon_tag_it != container->end(); muon_tag_it++) {
    const xAOD::Muon* muon_tag = *muon_tag_it;
    
    if (m_tandp_tag_type != -1) {
      if (muon_tag->muonType() != m_tandp_tag_type) continue;
    }
    if (muon_tag->pt() < m_tandp_tag_pt) continue;
    
    // probe
    for (muon_probe_it = muon_tag_it; muon_probe_it != container->end(); muon_probe_it++) {
      const xAOD::Muon* muon_probe = *muon_probe_it;

      if (muon_tag_it == muon_probe_it) continue;
      
      if (m_tandp_probe_type != -1) {
        if (muon_probe->muonType() != m_tandp_probe_type) continue;
      }
      if (muon_probe->pt() < m_tandp_probe_pt) continue;

      // mass
      TLorentzVector p1, p2, p3;
      p1.SetPtEtaPhiM(muon_tag->pt(), muon_tag->eta(), muon_tag->phi(), 105.658367);
      p2.SetPtEtaPhiM(muon_probe->pt(), muon_probe->eta(), muon_probe->phi(), 105.658367);
      p3 = p1 + p2;

      if (p3.M() < m_tandp_mass) continue;

      return true;
    }
  }
  return false;
}


bool 
L1TgcSkim::passMuonSelection()
{
  const xAOD::MuonContainer* container = 0;
  CHECK(evtStore()->retrieve(container, "Muons"));

  xAOD::MuonContainer::const_iterator muon_it;

  int counter = 0;
  for (muon_it = container->begin(); muon_it != container->end(); muon_it++) {
    const xAOD::Muon* muon = *muon_it;

    if (m_muons_type != -1) {
      if (muon->muonType() != m_muons_type) continue;
    }
    if (muon->pt() < m_muons_pt) continue;

    counter++;
  }

  if (counter >= m_muons_n) return true;
  else return false;
}
