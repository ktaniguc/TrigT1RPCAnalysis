#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODMuon/MuonSegmentContainer.h"
#include "TrkSegment/Segment.h"

#include "TTree.h"

#include "src/L1TGCNtuple_museg.h"

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_museg::L1TGCNtuple_museg(IMessageSvc* svc,
                                 const std::string& name)
  : m_museg_n(0),
    m_museg_x(new std::vector<float>),
    m_museg_y(new std::vector<float>),
    m_museg_z(new std::vector<float>),
    m_museg_px(new std::vector<float>),
    m_museg_py(new std::vector<float>),
    m_museg_pz(new std::vector<float>),
    m_museg_t0(new std::vector<float>),
    m_museg_t0error(new std::vector<float>),
    m_museg_chi2(new std::vector<float>),
    m_museg_ndof(new std::vector<float>),
    m_museg_sector(new std::vector<int>),
    m_museg_stationName(new std::vector<int>),
    m_museg_stationEta(new std::vector<int>),
    m_museg_author(new std::vector<int>),

    msg(svc, name) {
}

L1TGCNtuple_museg::~L1TGCNtuple_museg() {

  delete m_museg_x;
  delete m_museg_y;
  delete m_museg_z;
  delete m_museg_px;
  delete m_museg_py;
  delete m_museg_pz;
  delete m_museg_t0;
  delete m_museg_t0error;
  delete m_museg_chi2;
  delete m_museg_ndof;
  delete m_museg_sector;
  delete m_museg_stationName;
  delete m_museg_stationEta;
  delete m_museg_author;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_museg::book(TTree* ttree) {

  ttree->Branch("museg_n", &m_museg_n, "TGC_museg_n/I"); 
  ttree->Branch("museg_x", &m_museg_x);
  ttree->Branch("museg_y", &m_museg_y);
  ttree->Branch("museg_z", &m_museg_z);
  ttree->Branch("museg_px", &m_museg_px);
  ttree->Branch("museg_py", &m_museg_py);
  ttree->Branch("museg_pz", &m_museg_pz);
  ttree->Branch("museg_t0", &m_museg_t0);
  ttree->Branch("museg_t0error", &m_museg_t0error);
  ttree->Branch("museg_chi2", &m_museg_chi2);
  ttree->Branch("museg_ndof", &m_museg_ndof);
  ttree->Branch("museg_sector", &m_museg_sector);
  ttree->Branch("museg_stationName", &m_museg_stationName);
  ttree->Branch("museg_stationEta", &m_museg_stationEta);
  ttree->Branch("museg_author", &m_museg_author);
  
  return StatusCode::SUCCESS;
}


/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_museg::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::MuonSegmentContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, "MuonSegments");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  xAOD::MuonSegmentContainer::const_iterator museg_it;

  for (museg_it = container->begin(); museg_it != container->end(); museg_it++) {

    const xAOD::MuonSegment* museg = *museg_it;

    m_museg_n++;
    m_museg_x->push_back(museg->x());
    m_museg_y->push_back(museg->y());
    m_museg_z->push_back(museg->z());
    m_museg_px->push_back(museg->px());
    m_museg_py->push_back(museg->py());
    m_museg_pz->push_back(museg->pz());
    m_museg_t0->push_back(museg->t0());
    m_museg_t0error->push_back(museg->t0error());
    m_museg_chi2->push_back(museg->chiSquared());
    m_museg_ndof->push_back(museg->numberDoF());
    m_museg_sector->push_back(museg->sector());
    m_museg_stationName->push_back(museg->chamberIndex());
    m_museg_stationEta->push_back(museg->etaIndex());

    if (museg->muonSegment()) {
      const ElementLink< Trk::SegmentCollection >& el = museg->muonSegment();
      const Trk::Segment* seg = *el;
      m_museg_author->push_back(seg->author());
    } else {
      m_museg_author->push_back(-1);
    }
  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_museg::clear() {
  
  m_museg_n = 0;

  m_museg_x->clear();
  m_museg_y->clear();
  m_museg_z->clear();
  m_museg_px->clear();
  m_museg_py->clear();
  m_museg_pz->clear();
  m_museg_t0->clear();
  m_museg_t0error->clear();
  m_museg_chi2->clear();
  m_museg_ndof->clear();
  m_museg_sector->clear();
  m_museg_stationName->clear();
  m_museg_stationEta->clear();
  m_museg_author->clear();
  
  return;
}

/* eof */
