#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "MuonPrepRawData/TgcPrepDataCollection.h"
#include "MuonPrepRawData/TgcPrepDataContainer.h"
#include "MuonPrepRawData/TgcPrepData.h"
#include "MuonDigitContainer/TgcDigit.h"
#include "MuonIdHelpers/MuonIdHelperTool.h"
#include "MuonReadoutGeometry/TgcReadoutElement.h"

#include "TTree.h"

#include "src/L1TGCNtuple_tgcPrd.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_tgcPrd::L1TGCNtuple_tgcPrd(IMessageSvc* svc,
                                 const std::string& name)
  : m_prd_n(0),
    m_prd_x(new std::vector<float>),
    m_prd_y(new std::vector<float>),
    m_prd_z(new std::vector<float>),
    m_prd_shortWidth(new std::vector<float>),
    m_prd_longWidth(new std::vector<float>),
    m_prd_length(new std::vector<float>),
    m_prd_isStrip(new std::vector<int>),
    m_prd_gasGap(new std::vector<int>),
    m_prd_channel(new std::vector<int>),
    m_prd_eta(new std::vector<int>),
    m_prd_phi(new std::vector<int>),
    m_prd_station(new std::vector<int>),
    m_prd_bunch(new std::vector<int>),
    m_idHelper("Muon::MuonIdHelperTool/MuonIdHelperTool"),
    m_tgcPrepDataAllBCs("TGC_MeasurementsAllBCs"),
    msg(svc, name) {
}

L1TGCNtuple_tgcPrd::~L1TGCNtuple_tgcPrd() {

  delete m_prd_x;
  delete m_prd_y;
  delete m_prd_z;
  delete m_prd_shortWidth;
  delete m_prd_longWidth;
  delete m_prd_length;
  delete m_prd_isStrip;
  delete m_prd_gasGap;
  delete m_prd_channel;
  delete m_prd_eta;
  delete m_prd_phi;
  delete m_prd_station;
  delete m_prd_bunch;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_tgcPrd::book(TTree* ttree) {

  ttree->Branch("TGC_prd_n", &m_prd_n, "TGC_prd_n/I"); 
  ttree->Branch("TGC_prd_x", &m_prd_x);
  ttree->Branch("TGC_prd_y", &m_prd_y);
  ttree->Branch("TGC_prd_z", &m_prd_z);
  ttree->Branch("TGC_prd_shortWidth", &m_prd_shortWidth);
  ttree->Branch("TGC_prd_longWidth", &m_prd_longWidth);
  ttree->Branch("TGC_prd_length", &m_prd_length);
  ttree->Branch("TGC_prd_isStrip", &m_prd_isStrip);
  ttree->Branch("TGC_prd_gasGap", &m_prd_gasGap);
  ttree->Branch("TGC_prd_channel", &m_prd_channel);
  ttree->Branch("TGC_prd_eta", &m_prd_eta);
  ttree->Branch("TGC_prd_phi", &m_prd_phi);
  ttree->Branch("TGC_prd_station", &m_prd_station);
  ttree->Branch("TGC_prd_bunch", &m_prd_bunch);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_tgcPrd::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const Muon::TgcPrepDataContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, m_tgcPrepDataAllBCs);

  if (sc.isFailure()) return StatusCode::SUCCESS;

  Muon::TgcPrepDataContainer::const_iterator tpdc_it;
  Muon::TgcPrepDataCollection::const_iterator cit;

  for (tpdc_it = container->begin(); tpdc_it != container->end(); tpdc_it++) {
    for (cit = (*tpdc_it)->begin(); cit != (*tpdc_it)->end(); cit++) {
      const Muon::TgcPrepData *data = *cit;

      if ((data->getBcBitMap()&Muon::TgcPrepData::BCBIT_PREVIOUS)==Muon::TgcPrepData::BCBIT_PREVIOUS) 
        CHECK(fillTgcPrepData(data, TgcDigit::BC_PREVIOUS));
      
      if ((data->getBcBitMap()&Muon::TgcPrepData::BCBIT_CURRENT)==Muon::TgcPrepData::BCBIT_CURRENT)
        CHECK(fillTgcPrepData(data, TgcDigit::BC_CURRENT));
      
      if ((data->getBcBitMap()&Muon::TgcPrepData::BCBIT_NEXT)==Muon::TgcPrepData::BCBIT_NEXT)
        CHECK(fillTgcPrepData(data, TgcDigit::BC_NEXT));
    } 
  }

  return StatusCode::SUCCESS;
}

StatusCode
L1TGCNtuple_tgcPrd::fillTgcPrepData(const Muon::TgcPrepData* data, const int bunch) {
  
  const TgcIdHelper& tgcIdHelper = m_idHelper->tgcIdHelper();
  const MuonGM::TgcReadoutElement* element = data->detectorElement();
  const Identifier id = data->identify();
  const int gasGap = tgcIdHelper.gasGap(id);
  const int channel = tgcIdHelper.channel(id);
  const bool isStrip = tgcIdHelper.isStrip(id);
  const Amg::Vector3D& pos = isStrip ? element->stripPos(gasGap, channel) : element->gangPos(gasGap, channel);
	
  m_prd_x->push_back(pos[0]);
  m_prd_y->push_back(pos[1]);
  m_prd_z->push_back(pos[2]);

  if (isStrip) {
    m_prd_shortWidth->push_back(element->stripShortWidth(gasGap, channel));
    m_prd_longWidth->push_back(element->stripLongWidth(gasGap, channel));
    m_prd_length->push_back(element->stripLength(gasGap, channel));
  } else {
    m_prd_shortWidth->push_back(element->gangShortWidth(gasGap, channel));
    m_prd_longWidth->push_back(element->gangLongWidth(gasGap, channel));
    m_prd_length->push_back(element->gangLength(gasGap, channel));
  }
  m_prd_isStrip->push_back(tgcIdHelper.isStrip(id));
  m_prd_gasGap->push_back(tgcIdHelper.gasGap(id));
  m_prd_channel->push_back(tgcIdHelper.channel(id));
  m_prd_eta->push_back(tgcIdHelper.stationEta(id));
  m_prd_phi->push_back(tgcIdHelper.stationPhi(id));
  m_prd_station->push_back(tgcIdHelper.stationName(id));
  m_prd_bunch->push_back(bunch);

  m_prd_n++;

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_tgcPrd::clear() {
  
  m_prd_n = 0;

  m_prd_x->clear();
  m_prd_y->clear();
  m_prd_z->clear();
  m_prd_shortWidth->clear();
  m_prd_longWidth->clear();
  m_prd_length->clear();
  m_prd_isStrip->clear();
  m_prd_gasGap->clear();
  m_prd_channel->clear();
  m_prd_eta->clear();
  m_prd_phi->clear();
  m_prd_station->clear();
  m_prd_bunch->clear();

  return;
}

/* eof */
