#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "MuonPrepRawData/RpcPrepDataCollection.h"
#include "MuonPrepRawData/RpcPrepDataContainer.h"
#include "MuonPrepRawData/RpcPrepData.h"
#include "MuonIdHelpers/MuonIdHelperTool.h"
#include "MuonReadoutGeometry/RpcReadoutElement.h"
#include "TrigT1RPClogic/decodeSL.h"

#include "TTree.h"

#include "src/L1TGCNtuple_rpcPrd.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_rpcPrd::L1TGCNtuple_rpcPrd(IMessageSvc* svc,
                                 const std::string& name)
  : m_prd_n(0),
    m_sl_n(0),
    m_prd_x(new std::vector<float>),
    m_prd_y(new std::vector<float>),
    m_prd_z(new std::vector<float>),
    m_prd_x2(new std::vector<float>),
    m_prd_y2(new std::vector<float>),
    m_prd_z2(new std::vector<float>),
    m_prd_time(new std::vector<float>),
    m_prd_triggerInfo(new std::vector<int>),
    m_prd_ambiguityFlag(new std::vector<int>),
    m_prd_measuresPhi(new std::vector<int>),
    m_prd_inRibs(new std::vector<int>),
    m_prd_station(new std::vector<int>),
    m_prd_stationEta(new std::vector<int>),
    m_prd_stationPhi(new std::vector<int>),
    m_prd_doubletR(new std::vector<int>),
    m_prd_doubletZ(new std::vector<int>),
    m_prd_stripWidth(new std::vector<double>),
    m_prd_stripLength(new std::vector<double>),
    m_prd_gasGap(new std::vector<int>),
    m_prd_channel(new std::vector<int>),
    m_sl_isMoreCandInRoI(new std::vector<int>),
    m_sl_thrNumber(new std::vector<int>),
    m_sl_roiNumber(new std::vector<int>),
    m_sl_dbc(new std::vector<int>),
    m_sl_bunchXID(new std::vector<int>),
    m_sl_sectorID(new std::vector<int>), //logic sector
    m_sl_side(new std::vector<int>),
    m_idHelper("Muon::MuonIdHelperTool/MuonIdHelperTool"),
    m_rpcPrepDataAllBCs("RPC_Measurements"),
    m_cabling_getter("RPCcablingServerSvc/RPCcablingServerSvc","TrigT1RPC"),
    m_cabling(0),
    msg(svc, name) {
}

StatusCode
L1TGCNtuple_rpcPrd::initialize()
{
  std::cout << "L1TGCNtuple_rpcPrd:: initialize ..." << std::endl;
  CHECK(m_cabling_getter.retrieve());
  CHECK(m_cabling_getter->giveCabling(m_cabling));
  return StatusCode::SUCCESS;
}

L1TGCNtuple_rpcPrd::~L1TGCNtuple_rpcPrd() {

  delete m_prd_x;
  delete m_prd_y;
  delete m_prd_z;
  delete m_prd_x2;
  delete m_prd_y2;
  delete m_prd_z2;
  delete m_prd_time;
  delete m_prd_triggerInfo;
  delete m_prd_ambiguityFlag;
  delete m_prd_measuresPhi;
  delete m_prd_inRibs;
  delete m_prd_station;
  delete m_prd_stationEta;
  delete m_prd_stationPhi;
  delete m_prd_doubletR;
  delete m_prd_doubletZ;
  delete m_prd_stripWidth;
  delete m_prd_stripLength;
  delete m_prd_gasGap;
  delete m_prd_channel;
  delete m_sl_isMoreCandInRoI;
  delete m_sl_thrNumber;
  delete m_sl_roiNumber;
  delete m_sl_dbc;
  delete m_sl_bunchXID;
  delete m_sl_sectorID;
  delete m_sl_side;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_rpcPrd::book(TTree* ttree) {

  ttree->Branch("RPC_prd_n", &m_prd_n, "RPC_prd_n/I"); 
  ttree->Branch("RPC_sl_n", &m_sl_n, "RPC_sl_n/I"); 
  ttree->Branch("RPC_prd_x", &m_prd_x);
  ttree->Branch("RPC_prd_y", &m_prd_y);
  ttree->Branch("RPC_prd_z", &m_prd_z);
  ttree->Branch("RPC_prd_x2", &m_prd_x);
  ttree->Branch("RPC_prd_y2", &m_prd_y);
  ttree->Branch("RPC_prd_z2", &m_prd_z);
  ttree->Branch("RPC_prd_time", &m_prd_time);
  ttree->Branch("RPC_prd_triggerInfo", &m_prd_triggerInfo);
  ttree->Branch("RPC_prd_ambiguityFlag", &m_prd_ambiguityFlag);
  ttree->Branch("RPC_prd_measuresPhi", &m_prd_measuresPhi);
  ttree->Branch("RPC_prd_inRibs", &m_prd_inRibs);
  ttree->Branch("RPC_prd_station", &m_prd_station);
  ttree->Branch("RPC_prd_stationEta", &m_prd_stationEta);
  ttree->Branch("RPC_prd_stationPhi", &m_prd_stationPhi);
  ttree->Branch("RPC_prd_doubletR", &m_prd_doubletR);
  ttree->Branch("RPC_prd_doubletZ", &m_prd_doubletZ);
  ttree->Branch("RPC_prd_stripWidth", &m_prd_stripWidth);
  ttree->Branch("RPC_prd_stripLength", &m_prd_stripLength);
  ttree->Branch("RPC_prd_gasGap", &m_prd_gasGap);
  ttree->Branch("RPC_prd_channel", &m_prd_channel);
  ttree->Branch("RPC_sl_isMoreCandInRoI", m_sl_isMoreCandInRoI);
  ttree->Branch("RPC_sl_thrNumber", m_sl_thrNumber);
  ttree->Branch("RPC_sl_roiNumber", m_sl_roiNumber);
  ttree->Branch("RPC_sl_dbc", m_sl_dbc);
  ttree->Branch("RPC_sl_bunchXID", m_sl_bunchXID);
  ttree->Branch("RPC_sl_sectorID", m_sl_sectorID);
  ttree->Branch("RPC_sl_side", m_sl_side);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_rpcPrd::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const Muon::RpcPrepDataContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, m_rpcPrepDataAllBCs);

  if (sc.isFailure()) return StatusCode::SUCCESS;

  RPCsimuData rpcData;

  Muon::RpcPrepDataContainer::const_iterator tpdc_it;
  Muon::RpcPrepDataCollection::const_iterator cit;

  for (tpdc_it = container->begin(); tpdc_it != container->end(); tpdc_it++) {
    for (cit = (*tpdc_it)->begin(); cit != (*tpdc_it)->end(); cit++) {
      
      const Muon::RpcPrepData *data = *cit;

      CHECK(fillRpcPrepData(data, rpcData));
    } 
  }
  CMAdata patterns(&rpcData, m_cabling, 0);
  PADdata pads(&patterns, m_cabling, 0);
  SLdata sectors(&pads, 0);
  SLdata::PatternsList sectors_patterns = sectors.give_patterns();      //
  SLdata::PatternsList::iterator SLit = sectors_patterns.begin();       //
  //
//  std::cout << "start sectorLogic algorithm" << std::endl;
  int firstBC_to_MUCTPI = -1;
  int lastBC_to_MUCTPI = 1;
  while(SLit != sectors_patterns.end())                                 //
  {                                                                     //
    SectorLogic* logic = (*SLit)->give_SectorL();                     //
    int sector     = (*SLit)->sector();                               //
    int subsystem  = (sector > 31)? 1 : 0;                            //
    int logic_sector  = sector%32;//
//    std::cout << "... SectorLogic sector/subsystem/logic_sector = " << sector << "/" << subsystem << "/" << logic_sector << std::endl;
    for (int dbc=firstBC_to_MUCTPI; dbc<=lastBC_to_MUCTPI; dbc++){
//      std::cout << ".... bunch = " << dbc << std::endl;
      unsigned int data_word = logic->outputToMuCTPI(dbc);
               std::cout << 
               "Input to MuCTPI: side=" << subsystem                    //
               << ", SL= " << logic_sector                                 //
               << ", BC= " << dbc
               << ", BunchXID=" << TriggerRPC::BunchXID(data_word)
               << ", RoI1/ROI2=" << TriggerRPC::ROI1(data_word) << "/" << TriggerRPC::ROI2(data_word)                 //
               << ", PT1/PT2=" << TriggerRPC::PT1(data_word) << "/" << TriggerRPC::PT2(data_word)   
               << ", moreThan1_1/moreThan1_2=" << TriggerRPC::moreThan1_1(data_word) << "/" << TriggerRPC::moreThan1_2(data_word) 
               << ", data_word=" << data_word
               << std::endl;   
               
      m_sl_isMoreCandInRoI->push_back(TriggerRPC::moreThan1_1(data_word));
      m_sl_thrNumber->push_back(TriggerRPC::PT1(data_word));
      m_sl_roiNumber->push_back(TriggerRPC::ROI1(data_word));
      m_sl_dbc->push_back(dbc);
      m_sl_bunchXID->push_back(TriggerRPC::BunchXID(data_word));
      m_sl_sectorID->push_back(logic_sector);
      m_sl_side->push_back(subsystem);
      m_sl_n++;
    }
    ++SLit;                                                           //
  }          

  return StatusCode::SUCCESS;
}


StatusCode
L1TGCNtuple_rpcPrd::fillRpcPrepData(const Muon::RpcPrepData* data, RPCsimuData& rpcData) {
  
  const RpcIdHelper& rpcIdHelper = m_idHelper->rpcIdHelper();
  
  const MuonGM::RpcReadoutElement* element = data->detectorElement();
  const Identifier id = data->identify();
  
  const Amg::Vector3D& pos = element->stripPos(id);
  m_prd_x->push_back(pos[0]);
  m_prd_y->push_back(pos[1]);
  m_prd_z->push_back(pos[2]);

  const Amg::Vector3D& pos2 = data->globalPosition();
  m_prd_x2->push_back(pos2[0]);
  m_prd_y2->push_back(pos2[1]);
  m_prd_z2->push_back(pos2[2]);
	
  m_prd_time->push_back(data->time());
  m_prd_triggerInfo->push_back(data->triggerInfo());
  m_prd_ambiguityFlag->push_back(data->ambiguityFlag());
 
  int measuresPhi = rpcIdHelper.measuresPhi(id);
  m_prd_measuresPhi->push_back(measuresPhi);

  m_prd_inRibs->push_back(element->inTheRibs());
  
  m_prd_station->push_back(rpcIdHelper.stationName(id));
  m_prd_stationEta->push_back(rpcIdHelper.stationEta(id));
  m_prd_stationPhi->push_back(rpcIdHelper.stationPhi(id));

  m_prd_doubletR->push_back(rpcIdHelper.doubletR(id));
  m_prd_doubletZ->push_back(rpcIdHelper.doubletZ(id));
  m_prd_stripWidth->push_back(element->StripWidth(measuresPhi));
  m_prd_stripLength->push_back(element->StripLength(measuresPhi));

  m_prd_gasGap->push_back(rpcIdHelper.gasGap(id));
  m_prd_channel->push_back(rpcIdHelper.channel(id));

  int stationEta = rpcIdHelper.stationEta(id);
  int stationPhi = rpcIdHelper.stationPhi(id);
  int doubletR = rpcIdHelper.doubletR(id);
  int doubletZ = rpcIdHelper.doubletZ(id);
  int gasGap = rpcIdHelper.gasGap(id);
  int doubletPhi = rpcIdHelper.doubletPhi(id);
  int strip = rpcIdHelper.strip(id);
  std::string stationName = rpcIdHelper.stationNameString(rpcIdHelper.stationName(id));
//  std::cout << "RPC prd :: doubletPhi/strip/stationName  = " << doubletPhi << "/" << strip << "/" << stationName << std::endl;
  unsigned long int strip_code_cab = m_cabling->strip_code_fromOffId(stationName, stationEta, stationPhi, 
                        doubletR, doubletZ, doubletPhi, gasGap, measuresPhi, strip);
  if(strip_code_cab){
//    std::cout << "Fill data for the Level-1 RPC digit" << std::endl;
    float xyz[4];
    xyz[1] = pos[0]/10.;
    xyz[2] = pos[1]/10.;
    xyz[3] = pos[2]/10.;
    xyz[0] = data->time();
//    std::cout << "x/y/z(cm) , time = " << xyz[1] << "/" << xyz[2] << "/" << xyz[3] << " , " << xyz[0] << std::endl;
    int param[3] = {0, 0, 0};
    RPCsimuDigit digit(0, strip_code_cab, param, xyz);
    rpcData << digit;
  }
  
  m_prd_n++;

  return StatusCode::SUCCESS;
}

std::string L1TGCNtuple_rpcPrd::stationNameString(int statNum)
{
  std::string stat = "";
  if(statNum == 1) stat = "BIL";
  else if(statNum == 2) stat = "BIS";
  else if(statNum == 3) stat = "BML";
  else if(statNum == 4) stat = "BMS";
  else if(statNum == 5) stat = "BOL";
  else if(statNum == 6) stat = "BOS";
  else if(statNum == 7) stat = "BEE";
  else if(statNum == 8) stat = "BIR";
  else if(statNum == 9) stat = "BMF";
  else if(statNum == 10) stat = "BOF";
  else if(statNum == 11) stat = "BOG";
  else if(statNum == 12) stat = "BME";
  else if(statNum == 13) stat = "BIM";
  else stat = "error";
  return stat;
}


void
L1TGCNtuple_rpcPrd::clear() {
  
  m_prd_n = 0;
  m_sl_n = 0;

  m_prd_x->clear();
  m_prd_y->clear();
  m_prd_z->clear();
  m_prd_x2->clear();
  m_prd_y2->clear();
  m_prd_z2->clear();
  m_prd_time->clear();
  m_prd_triggerInfo->clear();
  m_prd_ambiguityFlag->clear();
  m_prd_measuresPhi->clear();
  m_prd_inRibs->clear();
  m_prd_station->clear();
  m_prd_stationEta->clear();
  m_prd_stationPhi->clear();
  m_prd_doubletR->clear();
  m_prd_doubletZ->clear();
  m_prd_stripWidth->clear();
  m_prd_stripLength->clear();
  m_prd_gasGap->clear();
  m_prd_channel->clear();
  m_sl_isMoreCandInRoI->clear();
  m_sl_thrNumber->clear();
  m_sl_roiNumber->clear();
  m_sl_dbc->clear();
  m_sl_sectorID->clear();
  m_sl_side->clear();

  return;
}

/* eof */
