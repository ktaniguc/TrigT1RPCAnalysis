#ifndef L1TGCNTUPLE_TRUTH_h
#define L1TGCNTUPLE_TRUTH_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class L1TGCNtuple_truth {
  public:
    L1TGCNtuple_truth(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_truth();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    uint32_t             m_mc_n;
    std::vector<float>   *m_mc_pt;
    std::vector<float>   *m_mc_eta;
    std::vector<float>   *m_mc_phi;
    std::vector<float>   *m_mc_m;
    std::vector<int>     *m_mc_charge;

    std::vector<int>     *m_mc_pdgId;
    std::vector<int>     *m_mc_barcode;
    std::vector<int>     *m_mc_status;
    
    std::vector<float>     *m_mc_prodVtx_x;
    std::vector<float>     *m_mc_prodVtx_y;
    std::vector<float>     *m_mc_prodVtx_z;
    
    std::string m_truthParticles;
    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_truth_h
/* eof */
