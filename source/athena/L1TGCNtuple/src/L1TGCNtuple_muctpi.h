#ifndef L1TGCNTUPLE_MUCTPI_h
#define L1TGCNTUPLE_MUCTPI_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace LVL1 { class RecMuonRoiSvc; }

class L1TGCNtuple_muctpi {
  public:
    L1TGCNtuple_muctpi(IMessageSvc* svc,
                       const std::string& name);
    virtual ~L1TGCNtuple_muctpi();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    // Function creating an RoI word from the MuCTPI data words
    static uint32_t convertToRoIWord( uint32_t dataWord );
    // Function extracting the 3-bit BCID from the data words
    static short getBCID( uint32_t dataWord );  

    std::vector< uint32_t >* m_candidateMultiplicities; ///< Cand. multi. sent to the CTP
    int                      m_nDataWords; ///< Number of muon candidates in readout window
    std::vector< uint32_t >* m_dataWords; ///< The 32-bit dataWords of the muon candidates
  
    std::vector< float >* m_dw_eta; ///< Eta position of the muon candidate
    std::vector< float >* m_dw_phi; ///< Phi position of the muon candidate
    std::vector< short >* m_dw_source; ///< Detector source of the muon candidate
    std::vector< short >* m_dw_hemisphere; ///< Hemisphere where the candidate was seen
    std::vector< short >* m_dw_bcid; ///< BCID in which the candidate was received
    std::vector< short >* m_dw_sectorID; ///< SectorID of the muon candidate
    std::vector< short >* m_dw_thrNumber; ///< Threshold number of the muon candidate
    std::vector< short >* m_dw_roi; ///< RoI (subsector) number of the muon candidate
    std::vector< short >* m_dw_overlapFlags; ///< Overlap flags of the muon candidate
    std::vector< short >* m_dw_firstCandidate; ///< Candidate had highest pT in sector
    std::vector< short >* m_dw_moreCandInRoI; ///< More candidates were in the RoI
    std::vector< short >* m_dw_moreCandInSector; ///< More than 2 candidates in the sector
    std::vector< short >* m_dw_charge; ///< Candidate sign (0=negative,1=positive,100=undefined)
    std::vector< short >* m_dw_vetoed; ///< Candidate vetoed in multiplicity sum

    ServiceHandle< LVL1::RecMuonRoiSvc > m_rpcRoiSvc; ///< RPC rec. RoI service
    ServiceHandle< LVL1::RecMuonRoiSvc > m_tgcRoiSvc; ///< TGC rec. RoI service

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_trigInfo_h
/* eof */
