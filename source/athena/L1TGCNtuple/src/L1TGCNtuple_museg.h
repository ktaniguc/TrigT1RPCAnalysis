#ifndef L1TGCNTUPLE_MUSEG_h
#define L1TGCNTUPLE_MUSEG_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class L1TGCNtuple_museg {
  public:
    L1TGCNtuple_museg(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_museg();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    uint32_t             m_museg_n;
    std::vector<float>   *m_museg_x;
    std::vector<float>   *m_museg_y;
    std::vector<float>   *m_museg_z;
    std::vector<float>   *m_museg_px;
    std::vector<float>   *m_museg_py;
    std::vector<float>   *m_museg_pz;
    std::vector<float>   *m_museg_t0;
    std::vector<float>   *m_museg_t0error;
    std::vector<float>   *m_museg_chi2;
    std::vector<float>   *m_museg_ndof;
    std::vector<int>   *m_museg_sector;
    std::vector<int>   *m_museg_stationName;
    std::vector<int>   *m_museg_stationEta;
    std::vector<int>   *m_museg_author;


    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_museg_h
/* eof */
