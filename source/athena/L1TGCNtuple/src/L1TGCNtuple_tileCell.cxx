#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "TileEvent/TileCell.h"
#include "CaloIdentifier/CaloCell_ID.h"
#include "CaloIdentifier/TileID.h"
#include "TileIdentifier/TileHWID.h"

#include "TTree.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include "src/L1TGCNtuple_tileCell.h"

#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCellContainer.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_tileCell::L1TGCNtuple_tileCell(IMessageSvc* svc,
                                 const std::string& name)
  : m_cell_n(0),
    m_cell_E(new std::vector<float>),
    m_cell_ene1(new std::vector<float>),
    m_cell_eta(new std::vector<float>),
    m_cell_phi(new std::vector<float>),
    m_cell_sinTh(new std::vector<float>),
    m_cell_cosTh(new std::vector<float>),
    m_cell_cotTh(new std::vector<float>),
    m_cell_x(new std::vector<float>),
    m_cell_y(new std::vector<float>),
    m_cell_z(new std::vector<float>),
    m_cell_badcell(new std::vector<int>),
    m_cell_partition(new std::vector<int>),
    m_cell_section(new std::vector<int>),
    m_cell_side(new std::vector<int>),
    m_cell_module(new std::vector<int>),
    m_cell_tower(new std::vector<int>),
    m_cell_sample(new std::vector<int>),
    
    m_tileID(0),
    m_tileHWID(0),
    msg(svc, name) {

}

L1TGCNtuple_tileCell::~L1TGCNtuple_tileCell() {

  delete m_cell_E;
  delete m_cell_ene1;
  delete m_cell_eta;
  delete m_cell_phi;
  delete m_cell_sinTh;
  delete m_cell_cosTh;
  delete m_cell_cotTh;
  delete m_cell_x;
  delete m_cell_y;
  delete m_cell_z;
  delete m_cell_badcell;
  delete m_cell_partition;
  delete m_cell_section;
  delete m_cell_side;
  delete m_cell_module;
  delete m_cell_tower;
  delete m_cell_sample;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_tileCell::book(TTree* ttree,
    ServiceHandle<StoreGateSvc>& detector) {

  CHECK( detector->retrieve(m_tileID) );
  CHECK( detector->retrieve(m_tileHWID) );

  ttree->Branch("TILE_cell_n",  &m_cell_n, "TILE_cell_n/I"); 
  ttree->Branch("TILE_cell_E",  &m_cell_E); 
  ttree->Branch("TILE_cell_ene1",  &m_cell_ene1); 
  ttree->Branch("TILE_cell_eta",  &m_cell_eta); 
  ttree->Branch("TILE_cell_phi",  &m_cell_phi); 
  ttree->Branch("TILE_cell_sinTh",  &m_cell_sinTh); 
  ttree->Branch("TILE_cell_cosTh",  &m_cell_cosTh); 
  ttree->Branch("TILE_cell_cotTh",  &m_cell_cotTh); 
  ttree->Branch("TILE_cell_x",  &m_cell_x); 
  ttree->Branch("TILE_cell_y",  &m_cell_y); 
  ttree->Branch("TILE_cell_z",  &m_cell_z); 
  ttree->Branch("TILE_cell_badcell",  &m_cell_badcell); 
  ttree->Branch("TILE_cell_partition",  &m_cell_partition); 
  ttree->Branch("TILE_cell_section",  &m_cell_section); 
  ttree->Branch("TILE_cell_side",  &m_cell_side); 
  ttree->Branch("TILE_cell_module",  &m_cell_module); 
  ttree->Branch("TILE_cell_tower",  &m_cell_tower); 
  ttree->Branch("TILE_cell_sample",  &m_cell_sample); 

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/


StatusCode
L1TGCNtuple_tileCell::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  StatusCode sc;

  const CaloCellContainer* container = 0;
  sc = storeGateSvc->retrieve(container, "AllCalo");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  CaloCellContainer::const_iterator tcdc_it = container->begin();
  CaloCellContainer::const_iterator cit = container->end();

  for (; tcdc_it != cit; ++tcdc_it) {
    const CaloCell* cell = (*tcdc_it);

    // only TILE
    if (cell->caloDDE()->getSubCalo() != CaloCell_ID::TILE) continue; 
     
    m_cell_n++;

    m_cell_E->push_back(cell->energy()); 
    m_cell_eta->push_back(cell->eta()); 
    m_cell_phi->push_back(cell->phi()); 
    m_cell_sinTh->push_back(cell->sinTh()); 
    m_cell_cosTh->push_back(cell->cosTh()); 
    m_cell_cotTh->push_back(cell->cotTh()); 
    m_cell_x->push_back(cell->x()); 
    m_cell_y->push_back(cell->y()); 
    m_cell_z->push_back(cell->z()); 
    m_cell_badcell->push_back(cell->badcell()); 
 
    const TileCell* tilecell = dynamic_cast<const TileCell*> (cell);
    m_cell_ene1->push_back(tilecell->ene1());

    // online ID
    long gain1 = tilecell->gain1();
    const CaloDetDescrElement * caloDDE = tilecell->caloDDE();
    IdentifierHash hash1 = (gain1<0) ? TileHWID::NOT_VALID_HASH : caloDDE->onl1();

    if (hash1 != TileHWID::NOT_VALID_HASH) {
  
      HWIdentifier adc_id = m_tileHWID->adc_id(hash1,gain1);
      m_cell_partition->push_back(m_tileHWID->ros(adc_id));

    } else {
      m_cell_partition->push_back(0);
    }

    // offline ID
    m_cell_section->push_back(m_tileID->section(cell->ID()));
    m_cell_side->push_back(m_tileID->side(cell->ID()));
    m_cell_module->push_back(m_tileID->module(cell->ID()));
    m_cell_tower->push_back(m_tileID->tower(cell->ID()));
    m_cell_sample->push_back(m_tileID->sample(cell->ID()));

  }

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_tileCell::clear() {
 
  m_cell_n = 0;
  m_cell_E->clear();
  m_cell_ene1->clear();
  m_cell_eta->clear();
  m_cell_phi->clear();
  m_cell_sinTh->clear();
  m_cell_cosTh->clear();
  m_cell_cotTh->clear();
  m_cell_x->clear();
  m_cell_y->clear();
  m_cell_z->clear();
  m_cell_badcell->clear();
  m_cell_partition->clear();
  m_cell_section->clear();
  m_cell_side->clear();
  m_cell_module->clear();
  m_cell_tower->clear();
  m_cell_sample->clear();
  
  return;
}

/* eof */
