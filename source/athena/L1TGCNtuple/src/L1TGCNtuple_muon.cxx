#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODPrimitives/IsolationType.h"
#include "MuonSelectorTools/IMuonSelectionTool.h"
#include "xAODTracking/TrackingPrimitives.h"

#include "TTree.h"

#include "src/L1TGCNtuple_muon.h"

//using HepGeom::Point3D;

struct paramVar
{
  const char* name;
  xAOD::Muon::ParamDef type;
};

struct trackVar
{
  const char* name;
  xAOD::SummaryType type;
};

struct muonVar
{
  const char* name;
  xAOD::MuonSummaryType type;
};

const paramVar  paramVars1[] =
{
  {"mu_msInnerMatchChi2", xAOD::Muon::ParamDef::msInnerMatchChi2},
  {"mu_msOuterMatchChi2", xAOD::Muon::ParamDef::msOuterMatchChi2},
};

const paramVar  paramVars2[] =
{
  {"mu_msInnerMatchDOF",  xAOD::Muon::ParamDef::msInnerMatchDOF},
  {"mu_msOuterMatchDOF",  xAOD::Muon::ParamDef::msOuterMatchDOF}
};

const trackVar  trackVars[] =
{
  {"mu_nOutliersOnTrack", xAOD::SummaryType::numberOfOutliersOnTrack},

  {"mu_nBLHits",           xAOD::SummaryType::numberOfBLayerHits},
  {"mu_nPixHits",          xAOD::SummaryType::numberOfPixelHits},
  {"mu_nSCTHits",          xAOD::SummaryType::numberOfSCTHits},
  {"mu_nTRTHits",          xAOD::SummaryType::numberOfTRTHits},
  {"mu_nTRTHighTHits",     xAOD::SummaryType::numberOfTRTHighThresholdHits},
  {"mu_nBLSharedHits",     xAOD::SummaryType::numberOfBLayerSharedHits},
  {"mu_nPixSharedHits",    xAOD::SummaryType::numberOfPixelSharedHits},
  {"mu_nPixHoles",         xAOD::SummaryType::numberOfPixelHoles},
  {"mu_nSCTSharedHits",    xAOD::SummaryType::numberOfSCTSharedHits},
  {"mu_nSCTHoles",         xAOD::SummaryType::numberOfSCTHoles},
  {"mu_nTRTOutliers",      xAOD::SummaryType::numberOfTRTOutliers},
  {"mu_nTRTHighTOutliers", xAOD::SummaryType::numberOfTRTHighThresholdOutliers},
  {"mu_nGangedPixels",     xAOD::SummaryType::numberOfGangedPixels},
  {"mu_nPixelDeadSensors", xAOD::SummaryType::numberOfPixelDeadSensors},
  {"mu_nSCTDeadSensors",   xAOD::SummaryType::numberOfSCTDeadSensors},
  {"mu_nTRTDeadStraws",    xAOD::SummaryType::numberOfTRTDeadStraws},
  {"mu_expectBLayerHit",   xAOD::SummaryType::expectBLayerHit},

  {"mu_nPrecisionLayers",      xAOD::SummaryType::numberOfPrecisionLayers},
  {"mu_nPrecisionHoleLayers",  xAOD::SummaryType::numberOfPrecisionHoleLayers},
  {"mu_nPhiLayers",            xAOD::SummaryType::numberOfPhiLayers},
  {"mu_nPhiHoleLayers",        xAOD::SummaryType::numberOfPhiHoleLayers},
  {"mu_nTrigEtaLayers",     xAOD::SummaryType::numberOfTriggerEtaLayers},
  {"mu_nTrigEtaHoleLayers", xAOD::SummaryType::numberOfTriggerEtaHoleLayers}
};

const muonVar  muonVars[] =
{
  {"mu_primarySector", xAOD::MuonSummaryType::primarySector},
  {"mu_secondarySector", xAOD::MuonSummaryType::secondarySector},
  {"mu_nInnerSmallHits", xAOD::MuonSummaryType::innerSmallHits},
  {"mu_nInnerLargeHits", xAOD::MuonSummaryType::innerLargeHits},
  {"mu_nMiddleSmallHits", xAOD::MuonSummaryType::middleSmallHits},
  {"mu_nMiddleLargeHits", xAOD::MuonSummaryType::middleLargeHits},
  {"mu_nOuterSmallHits", xAOD::MuonSummaryType::outerSmallHits},
  {"mu_nOuterLargeHits", xAOD::MuonSummaryType::outerLargeHits},
  {"mu_nExtendedSmallHits", xAOD::MuonSummaryType::extendedSmallHits},
  {"mu_nExtendedLargeHits", xAOD::MuonSummaryType::extendedLargeHits},
  {"mu_nInnerSmallHoles", xAOD::MuonSummaryType::innerSmallHoles},
  {"mu_nInnerLargeHoles", xAOD::MuonSummaryType::innerLargeHoles},
  {"mu_nMiddleSmallHoles", xAOD::MuonSummaryType::middleSmallHoles},
  {"mu_nMiddleLargeHoles", xAOD::MuonSummaryType::middleLargeHoles},
  {"mu_nOuterSmallHoles", xAOD::MuonSummaryType::outerSmallHoles},
  {"mu_nOuterLargeHoles", xAOD::MuonSummaryType::outerLargeHoles},
  {"mu_nExtendedSmallHoles", xAOD::MuonSummaryType::extendedSmallHoles},
  {"mu_nExtendedLargeHoles", xAOD::MuonSummaryType::extendedLargeHoles},
  {"mu_nPhiLayer1Hits", xAOD::MuonSummaryType::phiLayer1Hits},
  {"mu_nPhiLayer2Hits", xAOD::MuonSummaryType::phiLayer2Hits},
  {"mu_nPhiLayer3Hits", xAOD::MuonSummaryType::phiLayer3Hits},
  {"mu_nPhiLayer4Hits", xAOD::MuonSummaryType::phiLayer4Hits},
  {"mu_nEtaLayer1Hits", xAOD::MuonSummaryType::etaLayer1Hits},
  {"mu_nEtaLayer2Hits", xAOD::MuonSummaryType::etaLayer2Hits},
  {"mu_nEtaLayer3Hits", xAOD::MuonSummaryType::etaLayer3Hits},
  {"mu_nEtaLayer4Hits", xAOD::MuonSummaryType::etaLayer4Hits},
  {"mu_nPhiLayer1Holes", xAOD::MuonSummaryType::phiLayer1Holes},
  {"mu_nPhiLayer2Holes", xAOD::MuonSummaryType::phiLayer2Holes},
  {"mu_nPhiLayer3Holes", xAOD::MuonSummaryType::phiLayer3Holes},
  {"mu_nPhiLayer4Holes", xAOD::MuonSummaryType::phiLayer4Holes},
  {"mu_nEtaLayer1Holes", xAOD::MuonSummaryType::etaLayer1Holes},
  {"mu_nEtaLayer2Holes", xAOD::MuonSummaryType::etaLayer2Holes},
  {"mu_nEtaLayer3Holes", xAOD::MuonSummaryType::etaLayer3Holes},
  {"mu_nEtaLayer4Holes", xAOD::MuonSummaryType::etaLayer4Holes}
};

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_muon::L1TGCNtuple_muon(IMessageSvc* svc,
                                 const std::string& name)
  : m_mu_n(0),
    m_mu_pt(new std::vector<float>),
    m_mu_eta(new std::vector<float>),
    m_mu_phi(new std::vector<float>),
    m_mu_m(new std::vector<float>),
    m_mu_charge(new std::vector<int>),

    m_mu_author(new std::vector<int>),
    m_mu_allAuthors(new std::vector<uint16_t>),
    m_mu_muonType(new std::vector<int>),
    
    m_mu_etcone20(new std::vector<float>),
    m_mu_etcone30(new std::vector<float>),
    m_mu_etcone40(new std::vector<float>),
    m_mu_ptcone20(new std::vector<float>),
    m_mu_ptcone30(new std::vector<float>),
    m_mu_ptcone40(new std::vector<float>),
    
    m_mu_isPassedMCP(new std::vector<int>),
    m_mu_quality(new std::vector<int>),
    
    m_mu_trackfitchi2(new std::vector<float>),
    m_mu_trackfitndof(new std::vector<float>),
    
    m_mu_cb_d0(new std::vector<float>),
    m_mu_cb_z0(new std::vector<float>),
    m_mu_cb_phi0(new std::vector<float>),
    m_mu_cb_theta(new std::vector<float>),
    m_mu_cb_qOverP(new std::vector<float>),
    m_mu_cb_vx(new std::vector<float>),
    m_mu_cb_vy(new std::vector<float>),
    m_mu_cb_vz(new std::vector<float>),

    m_muons("Muons"),
    m_mtool("CP::MuonSelectionTool"),
    
    msg(svc, name) {
}

L1TGCNtuple_muon::~L1TGCNtuple_muon() {

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;
  delete m_mu_charge;

  delete m_mu_author;
  delete m_mu_allAuthors;
  delete m_mu_muonType;

  delete m_mu_etcone20;
  delete m_mu_etcone30;
  delete m_mu_etcone40;
  delete m_mu_ptcone20;
  delete m_mu_ptcone30;
  delete m_mu_ptcone40;
  
  delete m_mu_trackfitchi2;
  delete m_mu_trackfitndof;
  
  delete m_mu_isPassedMCP;
  delete m_mu_quality;

  delete m_mu_cb_d0;
  delete m_mu_cb_z0;
  delete m_mu_cb_phi0;
  delete m_mu_cb_theta;
  delete m_mu_cb_qOverP;
  delete m_mu_cb_vx;
  delete m_mu_cb_vy;
  delete m_mu_cb_vz;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_muon::book(TTree* ttree) {
 
  CHECK(m_mtool.retrieve());

  ttree->Branch("mu_n", &m_mu_n, "mu_n/I");
  ttree->Branch("mu_pt", &m_mu_pt);
  ttree->Branch("mu_eta", &m_mu_eta);
  ttree->Branch("mu_phi", &m_mu_phi);
  ttree->Branch("mu_m", &m_mu_m);
  ttree->Branch("mu_charge", &m_mu_charge);
  ttree->Branch("mu_author", &m_mu_author);
  ttree->Branch("mu_allAuthors", &m_mu_allAuthors);
  ttree->Branch("mu_muonType", &m_mu_muonType);
  ttree->Branch("mu_etcone20", &m_mu_etcone20);
  ttree->Branch("mu_etcone30", &m_mu_etcone30);
  ttree->Branch("mu_etcone40", &m_mu_etcone40);
  ttree->Branch("mu_ptcone20", &m_mu_ptcone20);
  ttree->Branch("mu_ptcone30", &m_mu_ptcone30);
  ttree->Branch("mu_ptcone40", &m_mu_ptcone40);
  ttree->Branch("mu_trackfitchi2", &m_mu_trackfitchi2);
  ttree->Branch("mu_trackfitndof", &m_mu_trackfitndof);
  ttree->Branch("mu_isPassedMCP", &m_mu_isPassedMCP);
  ttree->Branch("mu_quality", &m_mu_quality);
  
  m_paramVars1.reserve (std::distance (std::begin(paramVars1), std::end(paramVars1)));
  for (const paramVar& var : paramVars1) {
    m_paramVars1.push_back (std::make_pair (var.type, nullptr));
    ttree->Branch(var.name, &(m_paramVars1.back().second));
  } 
  
  m_paramVars2.reserve (std::distance (std::begin(paramVars2), std::end(paramVars2)));
  for (const paramVar& var : paramVars2) {
    m_paramVars2.push_back (std::make_pair (var.type, nullptr));
    ttree->Branch(var.name, &(m_paramVars2.back().second));
  } 

  m_trackVars.reserve (std::distance (std::begin(trackVars), std::end(trackVars)));
  for (const trackVar& var : trackVars) {
    m_trackVars.push_back (std::make_pair (var.type, nullptr));
    ttree->Branch(var.name, &(m_trackVars.back().second));
  } 

  m_muonVars.reserve (std::distance (std::begin(muonVars), std::end(muonVars)));
  for (const muonVar& var : muonVars) {
    m_muonVars.push_back (std::make_pair (var.type, nullptr));
    ttree->Branch(var.name, &(m_muonVars.back().second));
  } 
  
  ttree->Branch("mu_cb_d0", &m_mu_cb_d0);
  ttree->Branch("mu_cb_z0", &m_mu_cb_z0);
  ttree->Branch("mu_cb_phi0", &m_mu_cb_phi0);
  ttree->Branch("mu_cb_theta", &m_mu_cb_theta);
  ttree->Branch("mu_cb_qOverP", &m_mu_cb_qOverP);
  ttree->Branch("mu_cb_vx", &m_mu_cb_vx);
  ttree->Branch("mu_cb_vy", &m_mu_cb_vy);
  ttree->Branch("mu_cb_vz", &m_mu_cb_vz);
  
  return StatusCode::SUCCESS;
}


/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_muon::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::MuonContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, m_muons));

  xAOD::MuonContainer::const_iterator muon_it;

  for (muon_it = container->begin(); muon_it != container->end(); muon_it++) {

    m_mu_isPassedMCP->push_back(m_mtool->accept(**muon_it));
    m_mu_quality->push_back(m_mtool->getQuality(**muon_it));

    const xAOD::Muon* muon = *muon_it;

    m_mu_n++;
    m_mu_pt->push_back(muon->pt());
    m_mu_eta->push_back(muon->eta());
    m_mu_phi->push_back(muon->phi());
    m_mu_m->push_back(muon->m());
    m_mu_charge->push_back(muon->charge());
    m_mu_author->push_back(muon->author());
    m_mu_allAuthors->push_back(muon->allAuthors());
    m_mu_muonType->push_back(muon->muonType());
    m_mu_etcone20->push_back(muon->isolation(xAOD::Iso::etcone20));
    m_mu_etcone30->push_back(muon->isolation(xAOD::Iso::etcone30));
    m_mu_etcone40->push_back(muon->isolation(xAOD::Iso::etcone40));
    m_mu_ptcone20->push_back(muon->isolation(xAOD::Iso::ptcone20));
    m_mu_ptcone30->push_back(muon->isolation(xAOD::Iso::ptcone30));
    m_mu_ptcone40->push_back(muon->isolation(xAOD::Iso::ptcone40));

    // fit chi2 of primary track
    const xAOD::TrackParticle* track = 0;
    track = muon->primaryTrackParticle();

    if (track) {
      m_mu_trackfitchi2->push_back(track->chiSquared());
      m_mu_trackfitndof->push_back(track->numberDoF());
    
    } else {
      m_mu_trackfitchi2->push_back(-9999.);
      m_mu_trackfitndof->push_back(-9999.);
    }

    // muon parameters, associated hits
    for (auto& var : m_paramVars1) {
      float p_tmp = 0;
      muon->parameter (p_tmp, var.first);
      var.second->push_back(p_tmp);
    }
    
    for (auto& var : m_paramVars2) {
      int p_tmp = 0;
      muon->parameter (p_tmp, var.first);
      var.second->push_back(p_tmp);
    }
    
    for (auto& var : m_trackVars) {
 
      if (muon->primaryTrackParticle()) {
        uint8_t n_tmp = 0;
        muon->summaryValue (n_tmp, var.first);
        var.second->push_back(n_tmp);
      } else {
        var.second->push_back(-9999.);
      }
    }

    for (auto& var : m_muonVars) {
      uint8_t n_tmp = 0;
      muon->summaryValue (n_tmp, var.first);
      var.second->push_back(n_tmp);
    }

    // associated tracks
    if (muon->combinedTrackParticleLink()) {
      const xAOD::TrackParticle* cb = 0;
      const ElementLink< xAOD::TrackParticleContainer >& el = muon->combinedTrackParticleLink();
      cb = *el;

      m_mu_cb_d0->push_back(cb->d0());
      m_mu_cb_z0->push_back(cb->z0());
      m_mu_cb_phi0->push_back(cb->phi0());
      m_mu_cb_theta->push_back(cb->theta());
      m_mu_cb_qOverP->push_back(cb->qOverP());
      m_mu_cb_vx->push_back(cb->vx());
      m_mu_cb_vy->push_back(cb->vy());
      m_mu_cb_vz->push_back(cb->vz());
    } else {
      m_mu_cb_d0->push_back(-9999.);
      m_mu_cb_z0->push_back(-9999.);
      m_mu_cb_phi0->push_back(-9999.);
      m_mu_cb_theta->push_back(-9999.);
      m_mu_cb_qOverP->push_back(-9999.);
      m_mu_cb_vx->push_back(-9999.);
      m_mu_cb_vy->push_back(-9999.);
      m_mu_cb_vz->push_back(-9999.);
    }
  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_muon::clear() {
  
  m_mu_n = 0;
  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();
  m_mu_charge->clear();

  m_mu_author->clear();
  m_mu_allAuthors->clear();
  m_mu_muonType->clear();

  m_mu_etcone20->clear();
  m_mu_etcone30->clear();
  m_mu_etcone40->clear();
  m_mu_ptcone20->clear();
  m_mu_ptcone30->clear();
  m_mu_ptcone40->clear();
  
  m_mu_trackfitchi2->clear();
  m_mu_trackfitndof->clear();
  
  m_mu_isPassedMCP->clear();
  m_mu_quality->clear();

  m_mu_cb_d0->clear();
  m_mu_cb_z0->clear();
  m_mu_cb_phi0->clear();
  m_mu_cb_theta->clear();
  m_mu_cb_qOverP->clear();
  m_mu_cb_vx->clear();
  m_mu_cb_vy->clear();
  m_mu_cb_vz->clear();

  for (auto& var : m_paramVars1) {
    var.second->clear();
  }
  
  for (auto& var : m_paramVars2) {
    var.second->clear();
  }
  
  for (auto& var : m_trackVars) {
    var.second->clear();
  }
  
  for (auto& var : m_muonVars) {
    var.second->clear();
  }
  
  return;
}

/* eof */
