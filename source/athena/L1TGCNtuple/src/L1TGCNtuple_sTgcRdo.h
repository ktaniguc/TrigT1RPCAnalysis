#ifndef L1TGCNTUPLE_STGCRDO_h
#define L1TGCNTUPLE_STGCRDO_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;
class MuonIdHelper;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Muon { class MuonIdHelperTool; }
namespace MuonGM { class MuonDetectorManager; }

class L1TGCNtuple_sTgcRdo {
  public:
    L1TGCNtuple_sTgcRdo(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_sTgcRdo();
    
    StatusCode book(TTree* ttree,
        ServiceHandle<StoreGateSvc>& detector);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:

    std::vector<std::string> *m_rdo_stationName;
    std::vector<int> *m_rdo_stationEta;
    std::vector<int> *m_rdo_stationPhi;
    std::vector<int> *m_rdo_multiplet;
    std::vector<int> *m_rdo_gasGap;
    std::vector<int> *m_rdo_type;
    std::vector<int> *m_rdo_channel;
    
    std::vector<float> *m_rdo_x;
    std::vector<float> *m_rdo_y;
    std::vector<float> *m_rdo_z;

    ToolHandle<Muon::MuonIdHelperTool> m_idHelper;
    MuonGM::MuonDetectorManager* m_detManager;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_sTgcRdo_h
/* eof */
