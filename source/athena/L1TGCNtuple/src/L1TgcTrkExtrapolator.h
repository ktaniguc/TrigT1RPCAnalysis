#ifndef L1TGCTRKEXTRAPOLATOR_h
#define L1TGCTRKEXTRAPOLATOR_h

#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/IAlgTool.h"
#include <string>

#include "xAODMuon/MuonContainer.h"

namespace Rec { class TrackParticle; }
namespace Trk { class IExtrapolator; }
namespace Trk { class TrackStateOnSurface; }

namespace CLHEP { class Hep2Vector; }
namespace CLHEP { class Hep3Vector; }

class L1TgcTrkExtrapolator {
  public:
    
    L1TgcTrkExtrapolator(IMessageSvc* svc,
                         const std::string& name);
    ~L1TgcTrkExtrapolator();
    
    virtual StatusCode initialize();
    virtual StatusCode finalize();
    
    enum TargetDetector { UNDEF, TGC, RPC };
    
    bool extrapolateFromMDT(const xAOD::TrackParticle* trackParticle,
			    const Amg::Vector3D& pos,
			    const int detector,
			    Amg::Vector2D& eta,
			    Amg::Vector2D& phi,
			    Amg::Vector3D& mom) const;
    
    
  private:
    const Trk::TrackParameters*
      extrapolateToTGC(const Trk::TrackStateOnSurface* tsos,
                       const Amg::Vector3D& pos,
		                   Amg::Vector2D& distance) const;
    const Trk::TrackParameters*
      extrapolateToRPC(const Trk::TrackStateOnSurface* tsos,
                       const Amg::Vector3D& pos,
		                   Amg::Vector2D& distance) const;
    double getError(const std::vector<double>& inputVec) const;
    
    // tool handles
    ToolHandle<Trk::IExtrapolator> m_extrapolator;
    
    double m_endcapPivotPlaneMinimumRadius;
    double m_endcapPivotPlaneMaximumRadius;
    double m_barrelPivotPlaneHalfLength;

    mutable MsgStream msg; 
  };


#endif
/* eof */
