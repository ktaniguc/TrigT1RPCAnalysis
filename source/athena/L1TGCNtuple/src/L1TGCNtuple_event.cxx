#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODEventInfo/EventInfo.h"

#include "TTree.h"

#include "src/L1TGCNtuple_event.h"

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_event::L1TGCNtuple_event(IMessageSvc* svc,
                                     const std::string& name)
  : m_runNumber(0),
    m_eventNumber(0),
    m_timeStamp(0),
    m_timeStampNSOffset(0),
    m_lbn(0),              
    m_bcid(0),
    m_detmask0(0),
    m_detmask1(0),
    m_actualIntPerXing(0),
    m_averageIntPerXing(0),
    m_pixelFlags(0),
    m_sctFlags(0),
    m_trtFlags(0),
    m_larFlags(0),
    m_tileFlags(0),
    m_muonFlags(0),
    m_fwdFlags(0),
    m_coreFlags(0),
    m_pixelError(0),
    m_sctError(0),
    m_trtError(0),
    m_larError(0),
    m_tileError(0),
    m_muonError(0),
    m_fwdError(0),
    m_coreError(0),

    msg(svc, name)
{
}

L1TGCNtuple_event::~L1TGCNtuple_event() {
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_event::book(TTree* ttree) {
  
  ttree->Branch("RunNumber", &m_runNumber, "runNumber/I");
  ttree->Branch("EventNumber", &m_eventNumber, "EventNumber/I");
  ttree->Branch("timeStamp", &m_timeStamp, "timeStamp/I");
  ttree->Branch("timeStampNSOffset", &m_timeStampNSOffset, "timeStampNSOffset/I");
  ttree->Branch("lbn", &m_lbn, "lbn/I");
  ttree->Branch("bcid", &m_bcid, "bcid/I");
  ttree->Branch("detmask0", &m_detmask0, "detmask0/I");
  ttree->Branch("detmask1", &m_detmask1, "detmask1/I");
  ttree->Branch("actualIntPerXing", &m_actualIntPerXing, "actualIntPerXing/F");
  ttree->Branch("averageIntPerXing", &m_averageIntPerXing, "averageIntPerXing/F");
  ttree->Branch("pixelFlags", &m_pixelFlags, "pixelFlags/I");
  ttree->Branch("sctFlags", &m_sctFlags, "sctFlags/I");
  ttree->Branch("trtFlags", &m_trtFlags, "trtFlags/I");
  ttree->Branch("larFlags", &m_larFlags, "larFlags/I");
  ttree->Branch("tileFlags", &m_tileFlags, "tileFlags/I");
  ttree->Branch("muonFlags", &m_muonFlags, "muonFlags/I");
  ttree->Branch("fwdFlags", &m_fwdFlags, "fwdFlags/I");
  ttree->Branch("coreFlags", &m_coreFlags, "coreFlags/I");
  ttree->Branch("pixelError", &m_pixelError, "pixelError/I");
  ttree->Branch("sctError", &m_sctError, "sctError/I");
  ttree->Branch("trtError", &m_trtError, "trtError/I");
  ttree->Branch("larError", &m_larError, "larError/I");
  ttree->Branch("tileError", &m_tileError, "tileError/I");
  ttree->Branch("muonError", &m_muonError, "muonError/I");
  ttree->Branch("fwdError", &m_fwdError, "fwdError/I");
  ttree->Branch("coreError", &m_coreError, "coreError/I");
  
  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_event::fill(ServiceHandle<StoreGateSvc>& /* storeGate */) {

  const xAOD::EventInfo* eventInfo = 0;

  CHECK(StoreGate::pointer()->retrieve(eventInfo));
  
  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_timeStamp = eventInfo->timeStamp();
  m_timeStampNSOffset = eventInfo->timeStampNSOffset();
  m_lbn = eventInfo->lumiBlock();
  m_bcid = eventInfo->bcid();
  m_detmask0 = eventInfo->detectorMask0();
  m_detmask1 = eventInfo->detectorMask1();
 
  m_actualIntPerXing  = eventInfo->actualInteractionsPerCrossing();
  m_averageIntPerXing = eventInfo->averageInteractionsPerCrossing();

  m_pixelFlags = eventInfo->eventFlags(xAOD::EventInfo::Pixel);
  m_sctFlags = eventInfo->eventFlags(xAOD::EventInfo::SCT);
  m_trtFlags = eventInfo->eventFlags(xAOD::EventInfo::TRT);
  m_larFlags = eventInfo->eventFlags(xAOD::EventInfo::LAr);
  m_tileFlags = eventInfo->eventFlags(xAOD::EventInfo::Tile);
  m_muonFlags = eventInfo->eventFlags(xAOD::EventInfo::Muon);
  m_fwdFlags = eventInfo->eventFlags(xAOD::EventInfo::ForwardDet);
  m_coreFlags = eventInfo->eventFlags(xAOD::EventInfo::Core);

  m_pixelError = eventInfo->errorState(xAOD::EventInfo::Pixel);
  m_sctError = eventInfo->errorState(xAOD::EventInfo::SCT);
  m_trtError = eventInfo->errorState(xAOD::EventInfo::TRT);
  m_larError = eventInfo->errorState(xAOD::EventInfo::LAr);
  m_tileError = eventInfo->errorState(xAOD::EventInfo::Tile);
  m_muonError = eventInfo->errorState(xAOD::EventInfo::Muon);
  m_fwdError = eventInfo->errorState(xAOD::EventInfo::ForwardDet);
  m_coreError = eventInfo->errorState(xAOD::EventInfo::Core);
  
  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_event::clear() {
  
  m_runNumber = 0;
  m_eventNumber = 0;

  m_timeStamp = 0;
  m_timeStampNSOffset = 0;
  m_lbn = 0;
  m_bcid = 0;
  m_detmask0 = 0;
  m_detmask1 = 0;
  m_actualIntPerXing = 0;
  m_averageIntPerXing = 0;
  m_pixelFlags = 0;
  m_sctFlags = 0;
  m_trtFlags = 0;
  m_larFlags = 0;
  m_tileFlags = 0;
  m_muonFlags = 0;
  m_fwdFlags = 0;
  m_coreFlags = 0;
  m_pixelError = 0;
  m_sctError = 0;
  m_trtError = 0;
  m_larError = 0;
  m_tileError = 0;
  m_muonError = 0;
  m_fwdError = 0;
  m_coreError = 0;

  return;
}

/* eof */
