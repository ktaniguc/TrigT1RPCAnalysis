#ifndef L1TGCNTUPLE_EVENT_h
#define L1TGCNTUPLE_EVENT_h

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

class L1TGCNtuple_event {
  public:
    L1TGCNtuple_event(IMessageSvc* svc,
                      const std::string& name);
    virtual ~L1TGCNtuple_event();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:

    uint32_t m_runNumber;    // run number
    uint32_t m_eventNumber;  // event number
    uint32_t m_timeStamp;  
    uint32_t m_timeStampNSOffset;  
    uint32_t m_lbn;  
    uint32_t m_bcid;  
    uint32_t m_detmask0;  
    uint32_t m_detmask1; 
    float    m_actualIntPerXing;
    float    m_averageIntPerXing; 
    uint32_t m_pixelFlags;
    uint32_t m_sctFlags;
    uint32_t m_trtFlags;
    uint32_t m_larFlags;
    uint32_t m_tileFlags;
    uint32_t m_muonFlags;
    uint32_t m_fwdFlags;
    uint32_t m_coreFlags;
    uint32_t m_pixelError;
    uint32_t m_sctError;
    uint32_t m_trtError;
    uint32_t m_larError;
    uint32_t m_tileError;
    uint32_t m_muonError;
    uint32_t m_fwdError;
    uint32_t m_coreError;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_h
/* eof */
