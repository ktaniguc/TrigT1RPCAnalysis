#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "TrigMuonCoinHierarchy/IMuonRoiChainSvc.h"
#include "TrigMuonCoinHierarchy/ITgcCoinHierarchySvc.h"
#include "xAODTrigger/MuonRoIContainer.h"

#include "TTree.h"

#include "src/L1TGCNtuple_hierarchy.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_hierarchy::L1TGCNtuple_hierarchy(IMessageSvc* svc,
                                 const std::string& name)
  : m_hierarchy_n(0),
    m_hierarchy_index(new std::vector<int>),
    m_hierarchy_dR_hiPt(new std::vector<int>),
    m_hierarchy_dPhi_hiPt(new std::vector<int>),
    m_hierarchy_dR_tracklet(new std::vector<int>),
    m_hierarchy_dPhi_tracklet(new std::vector<int>),
    m_hierarchy_isChamberBoundary(new std::vector<int>),

    m_muRoi("LVL1MuonRoIs"),
    m_muonRoiChainSvc("Trigger::MuonRoiChainSvc", name),
    m_tgcCoinHierarchySvc("Trigger::TgcCoinHierarchySvc", name),
    
    msg(svc, name) {
}

L1TGCNtuple_hierarchy::~L1TGCNtuple_hierarchy() {

  delete m_hierarchy_index;
  delete m_hierarchy_dR_hiPt;
  delete m_hierarchy_dPhi_hiPt;
  delete m_hierarchy_dR_tracklet;
  delete m_hierarchy_dPhi_tracklet;
  delete m_hierarchy_isChamberBoundary;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_hierarchy::book(TTree* ttree) {

  ttree->Branch("TGC_hierarchy_n", &m_hierarchy_n, "TGC_hierarchy_n/I"); 
  ttree->Branch("TGC_hierarchy_index", &m_hierarchy_index);
  ttree->Branch("TGC_hierarchy_dR_hiPt", &m_hierarchy_dR_hiPt);
  ttree->Branch("TGC_hierarchy_dPhi_hiPt", &m_hierarchy_dPhi_hiPt);
  ttree->Branch("TGC_hierarchy_dR_tracklet", &m_hierarchy_dR_tracklet);
  ttree->Branch("TGC_hierarchy_dPhi_tracklet", &m_hierarchy_dPhi_tracklet);
  ttree->Branch("TGC_hierarchy_isChamberBoundary", &m_hierarchy_isChamberBoundary);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_hierarchy::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::MuonRoIContainer* container = 0;
  CHECK(storeGateSvc->retrieve(container, m_muRoi));

//  xAOD::MuonRoIContainer::const_iterator roi_it;
//
//  int index = 0;
//  for (roi_it = container->begin(); roi_it != container->end(); roi_it++) {
//    const xAOD::MuonRoI* roi = *roi_it;
//    const Muon::TgcCoinData* tgcSL = 0;
//    const Muon::TgcCoinData* tgcHiptWire = 0;
//    const Muon::TgcCoinData* tgcHiptStrip = 0;
//    const Muon::TgcCoinData* tgcTrackletWire = 0;
//    const Muon::TgcCoinData* tgcTrackletStrip = 0;
//    int tgcIsInEndcapChamberBoundary = -1;
//
//    const Trigger::MuonRoiChain* chain = 0;
//    CHECK(m_muonRoiChainSvc->getChain(&chain, roi->roiWord()));
//
//    if (roi->getSource() != xAOD::MuonRoI::Barrel) { // only TGC
//      if (chain) {
//        tgcIsInEndcapChamberBoundary = static_cast<int>(chain->isInEndcapChamberBoundary());
//        tgcSL = chain->getTgcCoinData();
//
//        if (tgcSL) {
//          std::vector<const Trigger::TgcCoinHierarchy*> hierarchies;
//          CHECK(m_tgcCoinHierarchySvc->getHierarchy(&hierarchies, tgcSL));
//
//          if (hierarchies.size() == 1) {
//          
//            const Trigger::TgcCoinHierarchy* hierarchy = hierarchies.at(0);
//            tgcHiptWire = hierarchy->getCoincidence(Muon::TgcCoinData::TYPE_HIPT, false);
//            tgcHiptStrip = hierarchy->getCoincidence(Muon::TgcCoinData::TYPE_HIPT, true);
//            tgcTrackletWire = hierarchy->getCoincidence(Muon::TgcCoinData::TYPE_TRACKLET, false);
//            tgcTrackletStrip = hierarchy->getCoincidence(Muon::TgcCoinData::TYPE_TRACKLET, true);
//          } 
//        } 
//      } 
//    }
//
//    m_hierarchy_index->push_back(index++);
//    m_hierarchy_dR_hiPt->push_back(tgcHiptWire ? tgcHiptWire->delta() : -31);
//    m_hierarchy_dPhi_hiPt->push_back(tgcHiptStrip ? tgcHiptStrip->delta() : -31);
//    m_hierarchy_dR_tracklet->push_back(tgcTrackletWire ? tgcTrackletWire->delta() : -31);
//    m_hierarchy_dPhi_tracklet->push_back(tgcTrackletStrip ? tgcTrackletStrip->delta() : -31);
//    m_hierarchy_isChamberBoundary->push_back(tgcIsInEndcapChamberBoundary);
//
//    m_hierarchy_n++;
//  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_hierarchy::clear() {
  
  m_hierarchy_n = 0;
  m_hierarchy_index->clear();
  m_hierarchy_dR_hiPt->clear();
  m_hierarchy_dPhi_hiPt->clear();
  m_hierarchy_dR_tracklet->clear();
  m_hierarchy_dPhi_tracklet->clear();
  m_hierarchy_isChamberBoundary->clear();

  return;
}

/* eof */
