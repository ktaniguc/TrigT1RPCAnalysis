#ifndef L1TGCNTUPLE_VERTEX_h
#define L1TGCNTUPLE_VERTEX_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class L1TGCNtuple_vertex {
  public:
    L1TGCNtuple_vertex(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_vertex();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    uint32_t             m_vxp_n;
    std::vector<float>   *m_vxp_x;
    std::vector<float>   *m_vxp_y;
    std::vector<float>   *m_vxp_z;
    std::vector<float>   *m_vxp_cov_x;
    std::vector<float>   *m_vxp_cov_y;
    std::vector<float>   *m_vxp_cov_z;
    std::vector<float>   *m_vxp_cov_xy;
    std::vector<float>   *m_vxp_cov_xz;
    std::vector<float>   *m_vxp_cov_yz;
    std::vector<float>   *m_vxp_chi2;
    std::vector<int>     *m_vxp_ndof;
    std::vector<int>     *m_vxp_nTracks;
    std::vector<int>     *m_vxp_type;

    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_vertex_h
/* eof */
