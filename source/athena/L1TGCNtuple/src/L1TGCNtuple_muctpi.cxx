#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "TTree.h"

#include "TrigT1Result/MuCTPI_RDO.h"
#include "TrigT1Interfaces/RecMuonRoI.h"
#include "TrigT1Interfaces/RecMuonRoiSvc.h"
#include "TrigConfL1Data/TriggerThreshold.h"

#include "src/L1TGCNtuple_muctpi.h"


/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_muctpi::L1TGCNtuple_muctpi(IMessageSvc* svc,
                                       const std::string& name)
  : m_candidateMultiplicities(new std::vector< uint32_t >),
    m_nDataWords(0),
    m_dataWords(new std::vector< uint32_t >),

    m_dw_eta(new std::vector< float >),
    m_dw_phi(new std::vector< float >),
    m_dw_source(new std::vector< short >),
    m_dw_hemisphere(new std::vector< short >),
    m_dw_bcid(new std::vector< short >),
    m_dw_sectorID(new std::vector< short >),
    m_dw_thrNumber(new std::vector< short >),
    m_dw_roi(new std::vector< short >),
    m_dw_overlapFlags(new std::vector< short >),
    m_dw_firstCandidate(new std::vector< short >),
    m_dw_moreCandInRoI(new std::vector< short >),
    m_dw_moreCandInSector(new std::vector< short >),
    m_dw_charge(new std::vector< short >),
    m_dw_vetoed(new std::vector< short >),

    m_rpcRoiSvc("LVL1RPC::RPCRecRoiSvc", name),
    m_tgcRoiSvc("LVL1TGC::TGCRecRoiSvc", name),

    msg(svc, name) {
}

L1TGCNtuple_muctpi::~L1TGCNtuple_muctpi() {

  delete m_candidateMultiplicities;
  delete m_dataWords;

  delete m_dw_eta;
  delete m_dw_phi;
  delete m_dw_source;
  delete m_dw_hemisphere;
  delete m_dw_bcid;
  delete m_dw_sectorID;
  delete m_dw_thrNumber;
  delete m_dw_roi;
  delete m_dw_overlapFlags;
  delete m_dw_firstCandidate;
  delete m_dw_moreCandInRoI;
  delete m_dw_moreCandInSector;
  delete m_dw_charge;
  delete m_dw_vetoed;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_muctpi::book(TTree* ttree) {

  CHECK( m_rpcRoiSvc.retrieve() );
  CHECK( m_tgcRoiSvc.retrieve() );

  ttree->Branch("muctpi_candidateMultiplicities", &m_candidateMultiplicities);
  ttree->Branch("muctpi_nDataWords", &m_nDataWords, "muctpi_nDataWords/I");
  ttree->Branch("muctpi_dataWords", &m_dataWords);
  ttree->Branch("muctpi_dw_eta", &m_dw_eta);
  ttree->Branch("muctpi_dw_phi", &m_dw_phi);
  ttree->Branch("muctpi_dw_source", &m_dw_source);
  ttree->Branch("muctpi_dw_hemisphere", &m_dw_hemisphere);
  ttree->Branch("muctpi_dw_bcid", &m_dw_bcid);
  ttree->Branch("muctpi_dw_sectorID", &m_dw_sectorID);
  ttree->Branch("muctpi_dw_thrNumber", &m_dw_thrNumber);
  ttree->Branch("muctpi_dw_roi", &m_dw_roi);
  ttree->Branch("muctpi_dw_veto", &m_dw_overlapFlags);
  ttree->Branch("muctpi_dw_firstCandidate", &m_dw_firstCandidate);
  ttree->Branch("muctpi_dw_moreCandInRoI", &m_dw_moreCandInRoI);
  ttree->Branch("muctpi_dw_moreCandInSector", &m_dw_moreCandInSector);
  ttree->Branch("muctpi_dw_charge", &m_dw_charge);
  ttree->Branch("muctpi_dw_candidateVetoed", &m_dw_vetoed);

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_muctpi::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  std::string containerName = "MUCTPI_RDO";
  const MuCTPI_RDO* rdo = 0;
  CHECK(storeGateSvc->retrieve(rdo, containerName)); 

  *m_candidateMultiplicities = rdo->getAllCandidateMultiplicities();
  m_nDataWords = rdo->dataWord().size();
  *m_dataWords = rdo->dataWord();

  // Create some dummy LVL1 muon thresholds:
  std::vector< TrigConf::TriggerThreshold* > dummy_thresholds;
 
  std::vector< uint32_t >::const_iterator dw_itr = rdo->dataWord().begin();
  std::vector< uint32_t >::const_iterator dw_end = rdo->dataWord().end();
  for( ; dw_itr != dw_end; ++dw_itr ) {

    LVL1::RecMuonRoI roi(convertToRoIWord( *dw_itr ),
                         &( *m_rpcRoiSvc ), &( *m_tgcRoiSvc ),
                         &dummy_thresholds );
    
    m_dw_eta->push_back( roi.eta() );
    m_dw_phi->push_back( roi.phi() );
    m_dw_source->push_back( roi.sysID() );
    m_dw_hemisphere->push_back( roi.subsysID() );
    m_dw_bcid->push_back( getBCID( *dw_itr ) );
    m_dw_sectorID->push_back( roi.sectorID() );
    m_dw_thrNumber->push_back( roi.getThresholdNumber() );
    m_dw_roi->push_back( roi.getRoINumber() );
    m_dw_overlapFlags->push_back( roi.getOverlap() );
    m_dw_firstCandidate->push_back( roi.firstCandidate() );
    m_dw_moreCandInRoI->push_back( roi.padOverflow() );
    m_dw_moreCandInSector->push_back( roi.sectorOverflow() );
    m_dw_charge->push_back( static_cast< short >( roi.candidateCharge() ) );
    m_dw_vetoed->push_back( roi.candidateVetoed() );
    if(roi.sysID() == 0 /*barrel*/){
      std::cout << "# barrel muctpi" << std::endl; 
      std::cout << "## sectorID/hemisphere/bcid = " << roi.sectorID() << "/" << roi.subsysID() << "/" << getBCID(*dw_itr) << std::endl;
      std::cout << "## roiNumber/thrNumber = " << roi.getRoINumber() << "/" << roi.getThresholdNumber() << std::endl;
      std::cout << "## isMoreCandInRoI/roiWord = " << roi.padOverflow() << "/" << roi.roiWord() << std::endl;
    }
  } 

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_muctpi::clear() {
  
  m_candidateMultiplicities->clear();
  m_nDataWords = 0;
  m_dataWords->clear();

  m_dw_eta->clear();
  m_dw_phi->clear();
  m_dw_source->clear();
  m_dw_hemisphere->clear();
  m_dw_bcid->clear();
  m_dw_sectorID->clear();
  m_dw_thrNumber->clear();
  m_dw_roi->clear();
  m_dw_overlapFlags->clear();
  m_dw_firstCandidate->clear();
  m_dw_moreCandInRoI->clear();
  m_dw_moreCandInSector->clear();
  m_dw_charge->clear();
  m_dw_vetoed->clear();
  
  return;
}

/*
 * For more information about the format of the MuCTPI data words and the
 * RoI words, see the document: https://edms.cern.ch/file/248757/1/mirod.pdf
 *
 * See also: https://edms.cern.ch/file/1142589/1/MIROD-Event-Format.pdf
 */
uint32_t L1TGCNtuple_muctpi::convertToRoIWord( uint32_t dataWord ) {

  return ( ( dataWord & 0x18000000 ) | ( ( dataWord & 0x3fe0000 ) >> 3 ) |
    ( dataWord & 0x3fff ) );
}

/*
 * For more information about the format of the MuCTPI data words and the
 * RoI words, see the document: https://edms.cern.ch/file/248757/1/mirod.pdf
 */
short L1TGCNtuple_muctpi::getBCID( uint32_t dataWord ) {

  return ( ( dataWord >> 14 ) & 0x7 );
}


/* eof */
