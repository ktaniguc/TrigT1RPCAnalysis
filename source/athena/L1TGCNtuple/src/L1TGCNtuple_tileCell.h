#ifndef L1TGCNTUPLE_TILECELL_h
#define L1TGCNTUPLE_TILECELL_h

#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

class TileID;
class TileHWID;

class L1TGCNtuple_tileCell {
  public:
    L1TGCNtuple_tileCell(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_tileCell();
    
    StatusCode book(TTree* ttree,
        ServiceHandle<StoreGateSvc>& detector);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:
    
    int               m_cell_n; 
    std::vector<float> *m_cell_E;
    std::vector<float> *m_cell_ene1;
    std::vector<float> *m_cell_eta;
    std::vector<float> *m_cell_phi;
    std::vector<float> *m_cell_sinTh;
    std::vector<float> *m_cell_cosTh;
    std::vector<float> *m_cell_cotTh;
    std::vector<float> *m_cell_x;
    std::vector<float> *m_cell_y;
    std::vector<float> *m_cell_z;
    std::vector<int>   *m_cell_badcell;
    std::vector<int>   *m_cell_partition;
    std::vector<int>   *m_cell_section;
    std::vector<int>   *m_cell_side;
    std::vector<int>   *m_cell_module;
    std::vector<int>   *m_cell_tower;
    std::vector<int>   *m_cell_sample;
   
    const TileID* m_tileID;
    const TileHWID* m_tileHWID;

    mutable MsgStream msg;

};

#endif  // L1TGCNtuple_tileCell_h
/* eof */
