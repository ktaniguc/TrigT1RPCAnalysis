#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"
#include "EventContainers/SelectAllObject.h"

#include "TileEvent/TileContainer.h"
#include "TileEvent/TileMuonReceiverObj.h"
#include "TileEvent/TileRawChannelContainer.h"
#include "TileEvent/TileDigitsContainer.h"

#include "TileIdentifier/TileHWID.h"

#include "PathResolver/PathResolver.h"

#include "TTree.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include "src/L1TGCNtuple_murcv.h"

//using HepGeom::Point3D;

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_murcv::L1TGCNtuple_murcv(IMessageSvc* svc,
                                 const std::string& name)
  : m_murcv_trig_n(0),
    m_murcv_trig_mod(new std::vector<int>),
    m_murcv_trig_part(new std::vector<int>),
    m_murcv_trig_bit0(new std::vector<bool>),
    m_murcv_trig_bit1(new std::vector<bool>),
    m_murcv_trig_bit2(new std::vector<bool>),
    m_murcv_trig_bit3(new std::vector<bool>),

    m_murcv_raw_n(0),
    m_murcv_raw_count(new std::vector<float>),
    m_murcv_raw_energy(new std::vector<float>),
    m_murcv_raw_ros(new std::vector<int>),
    m_murcv_raw_drawer(new std::vector<int>),
    m_murcv_raw_channel(new std::vector<int>),

    m_murcv_digit_n(0),
    m_murcv_digit_nSamples(new std::vector<int>),
    m_murcv_digit_ros(new std::vector<int>),
    m_murcv_digit_drawer(new std::vector<int>),
    m_murcv_digit_channel(new std::vector<int>),
    m_murcv_digit_sampleVec(new std::vector<std::vector<float> >),
    
    m_constants("TMDB_ADC_to_MeV_20151015.txt"),
    
    m_tileHWID(0),
    msg(svc, name) {

  for (int ii = 0; ii < SIDE_SIZE; ii++) {
    for (int jj = 0; jj < MODULE_SIZE; jj++) {
      for (int kk = 0; kk < CHANNEL_SIZE; kk++) {
        m_raw_const_a[ii][jj][kk] = 1.;
        m_raw_const_b[ii][jj][kk] = 0.;
      }
    }
  }  
}

L1TGCNtuple_murcv::~L1TGCNtuple_murcv() {

  delete m_murcv_trig_mod;
  delete m_murcv_trig_part;
  delete m_murcv_trig_bit0;
  delete m_murcv_trig_bit1;
  delete m_murcv_trig_bit2;
  delete m_murcv_trig_bit3;

  delete m_murcv_raw_count;
  delete m_murcv_raw_energy;
  delete m_murcv_raw_ros;
  delete m_murcv_raw_drawer;
  delete m_murcv_raw_channel;
  
  delete m_murcv_digit_nSamples;
  delete m_murcv_digit_ros;
  delete m_murcv_digit_drawer;
  delete m_murcv_digit_channel;
  delete m_murcv_digit_sampleVec;
}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_murcv::book(TTree* ttree,
    ServiceHandle<StoreGateSvc>& detector) {

  CHECK( detector->retrieve(m_tileHWID) );

  ttree->Branch("TILE_murcv_trig_n",    &m_murcv_trig_n, "TILE_murcv_trig_n/I"); 
  ttree->Branch("TILE_murcv_trig_mod",  &m_murcv_trig_mod); 
  ttree->Branch("TILE_murcv_trig_part", &m_murcv_trig_part); 
  ttree->Branch("TILE_murcv_trig_bit0", &m_murcv_trig_bit0); 
  ttree->Branch("TILE_murcv_trig_bit1", &m_murcv_trig_bit1); 
  ttree->Branch("TILE_murcv_trig_bit2", &m_murcv_trig_bit2); 
  ttree->Branch("TILE_murcv_trig_bit3", &m_murcv_trig_bit3); 
  
  ttree->Branch("TILE_murcv_raw_n",       &m_murcv_raw_n, "TILE_murcv_raw_n/I"); 
  ttree->Branch("TILE_murcv_raw_count",  &m_murcv_raw_count); 
  ttree->Branch("TILE_murcv_raw_energy",  &m_murcv_raw_energy); 
  ttree->Branch("TILE_murcv_raw_ros",     &m_murcv_raw_ros); 
  ttree->Branch("TILE_murcv_raw_drawer",  &m_murcv_raw_drawer); 
  ttree->Branch("TILE_murcv_raw_channel", &m_murcv_raw_channel); 
  
  ttree->Branch("TILE_murcv_digit_n",        &m_murcv_digit_n, "TILE_murcv_digit_n/I"); 
  ttree->Branch("TILE_murcv_digit_nSamples", &m_murcv_digit_nSamples); 
  ttree->Branch("TILE_murcv_digit_ros",      &m_murcv_digit_ros); 
  ttree->Branch("TILE_murcv_digit_drawer",   &m_murcv_digit_drawer); 
  ttree->Branch("TILE_murcv_digit_channel",  &m_murcv_digit_channel); 
  ttree->Branch("TILE_murcv_digit_sampleVec",  &m_murcv_digit_sampleVec); 

  // initialization
  std::vector<std::string> fileNames;
  fileNames.push_back(m_constants);
  
  for (int ii = 0; ii < (int)fileNames.size(); ii++) {
    std::string fullName = PathResolver::find_file((fileNames.at(ii)).c_str(), "DATAPATH");
  
    std::ifstream file(fullName.c_str());
    enum {BufferSize=1024};
    char buf[BufferSize];

    std::string tag, channel;
    int module;
    float  const_a, const_b;
   
    while(file.getline(buf,BufferSize)) {
      std::istringstream header(buf);

      header >> tag;
      if (tag != "EBA" && tag != "EBC") continue;

      header >> module >> channel >> const_a >> const_b;

      int index0 = 0;
      int index1 = module - 1;
      int index2 = 0;

      if (tag == "EBA")     index0 = 0;
      if (tag == "EBC")     index0 = 1;
      if (channel == "D5L") index2 = 0;
      if (channel == "D5R") index2 = 1;
      if (channel == "D6L") index2 = 2;
      if (channel == "D6R") index2 = 3;

      m_raw_const_a[index0][index1][index2] = const_a;
      m_raw_const_b[index0][index1][index2] = const_b;
    }
  }

  return StatusCode::SUCCESS;
}

/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/


StatusCode
L1TGCNtuple_murcv::fill(ServiceHandle<StoreGateSvc>& storeGateSvc,
    const bool isData) {

  // TileMuonReceiverContainer
  
  StatusCode sc;

  const TileMuonReceiverContainer* container = 0;
  sc = storeGateSvc->retrieve(container, "TileMuRcvCnt");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  TileMuonReceiverContainer::const_iterator tcdc_it = container->begin();
  TileMuonReceiverContainer::const_iterator cit = container->end();

  for (; tcdc_it != cit; ++tcdc_it) { 

    const TileMuonReceiverObj *obj = (*tcdc_it);
    const std::vector<bool> &decision = obj->GetDecision(); 

    if (decision.size() != 4) continue;

    int id = obj->GetID();

    if (isData) {
      m_murcv_trig_mod->push_back(id&0x0ff);
      m_murcv_trig_part->push_back((id&0xf00)>>8);
    } else {
      m_murcv_trig_mod->push_back(id%100);
      m_murcv_trig_part->push_back(id/100);
    }
    
    m_murcv_trig_bit0->push_back(decision[0]);
    m_murcv_trig_bit1->push_back(decision[1]);
    m_murcv_trig_bit2->push_back(decision[2]);
    m_murcv_trig_bit3->push_back(decision[3]);

    m_murcv_trig_n++;
  }

  // TileRawChannelContainer
  //
  const TileRawChannelContainer* RawChannelCnt;
  sc = storeGateSvc->retrieve(RawChannelCnt, "MuRcvRawChCnt");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  SelectAllObject<TileRawChannelContainer> selRCs(RawChannelCnt);
  SelectAllObject<TileRawChannelContainer>::const_iterator it_raw = selRCs.begin(); 
  SelectAllObject<TileRawChannelContainer>::const_iterator end_raw = selRCs.end(); 
 
  for(; it_raw != end_raw; ++it_raw) {

    float amplitude = (*it_raw)->amplitude();
    m_murcv_raw_count->push_back(amplitude);

    HWIdentifier hwid=(*it_raw)->adc_HWID();

    int ros = m_tileHWID->ros(hwid);
    int drawer = m_tileHWID->drawer(hwid);
    
    m_murcv_raw_ros->push_back(ros);
    m_murcv_raw_drawer->push_back(drawer);

    int channel = m_tileHWID->channel(hwid);

    if (isData) {
      m_murcv_raw_channel->push_back(channel);
      
      int side = -1;
      if (ros == 3) side = 0; 
      if (ros == 4) side = 1; 

      float const_a = m_raw_const_a[side][drawer][channel];
      float const_b = m_raw_const_b[side][drawer][channel];

      float energy = const_a*amplitude + const_b;
      m_murcv_raw_energy->push_back(energy);
 
    } else {
      
      if (channel == 17) m_murcv_raw_channel->push_back(0); 
      else if (channel == 16) m_murcv_raw_channel->push_back(1);
      else if (channel == 37) m_murcv_raw_channel->push_back(2);
      else if (channel == 38) m_murcv_raw_channel->push_back(3);
      else m_murcv_raw_channel->push_back(-1);

      m_murcv_raw_energy->push_back(amplitude);
    }

    m_murcv_raw_n++;
  } 

  // TileDigitsContainer

  const TileDigitsContainer* DigitCnt;
  sc = storeGateSvc->retrieve(DigitCnt, "MuRcvDigitsCnt");

  if (sc.isFailure()) return StatusCode::SUCCESS;

  SelectAllObject<TileDigitsContainer> selDCs(DigitCnt);
  SelectAllObject<TileDigitsContainer>::const_iterator it_digit = selDCs.begin(); 
  SelectAllObject<TileDigitsContainer>::const_iterator end_digit = selDCs.end(); 
  
  for(; it_digit != end_digit; ++it_digit) {

    int nSamples = (*it_digit)->NtimeSamples();
    m_murcv_digit_nSamples->push_back(nSamples);

    HWIdentifier hwid=(*it_digit)->adc_HWID();
        
    m_murcv_digit_ros->push_back(m_tileHWID->ros(hwid));
    m_murcv_digit_drawer->push_back(m_tileHWID->drawer(hwid));
                
    if (isData) {
      m_murcv_digit_channel->push_back(m_tileHWID->channel(hwid));
                            
    } else {
      int channel = m_tileHWID->channel(hwid);
                                          
      if (channel == 17) m_murcv_digit_channel->push_back(0); 
      else if (channel == 16) m_murcv_digit_channel->push_back(1);
      else if (channel == 37) m_murcv_digit_channel->push_back(2);
      else if (channel == 38) m_murcv_digit_channel->push_back(3);
      else m_murcv_digit_channel->push_back(-1);
    }

    std::vector<float> sampleVec;
    const std::vector<float> &samples = (*it_digit)->samples();

    for (int ii = 0; ii < nSamples; ii++) {
      sampleVec.push_back(samples[ii]);
    }
    
    m_murcv_digit_sampleVec->push_back(sampleVec);
    m_murcv_digit_n++;
  }

  return StatusCode::SUCCESS;
}


StatusCode L1TGCNtuple_murcv::setConstants(const std::string name) {
  m_constants = name;

  return StatusCode::SUCCESS;
}


void
L1TGCNtuple_murcv::clear() {
 
  m_murcv_trig_n = 0;
  m_murcv_trig_mod->clear();
  m_murcv_trig_part->clear();
  m_murcv_trig_bit0->clear();
  m_murcv_trig_bit1->clear();
  m_murcv_trig_bit2->clear();
  m_murcv_trig_bit3->clear();

  m_murcv_raw_n = 0;
  m_murcv_raw_count->clear();
  m_murcv_raw_energy->clear();
  m_murcv_raw_ros->clear();
  m_murcv_raw_drawer->clear();
  m_murcv_raw_channel->clear();

  m_murcv_digit_n = 0;
  m_murcv_digit_nSamples->clear();
  m_murcv_digit_ros->clear();
  m_murcv_digit_drawer->clear();
  m_murcv_digit_channel->clear();
  m_murcv_digit_sampleVec->clear();
  
  return;
}

/* eof */
