#ifndef L1TGCNTUPLE_TRIGINFO_h
#define L1TGCNTUPLE_TRIGINFO_h

#include "GaudiKernel/ToolHandle.h"

class IMessageSvc;
class MsgStream;
class TTree;
class StatusCode;

template <class T> class ToolHandle;
template <class T> class ServiceHandle;

namespace Trig { class TrigDecisionTool; }

class L1TGCNtuple_trigInfo {
  public:
    L1TGCNtuple_trigInfo(IMessageSvc* svc,
                    const std::string& name);
    virtual ~L1TGCNtuple_trigInfo();
    
    StatusCode book(TTree* ttree);
    StatusCode fill(ServiceHandle<StoreGateSvc>& storeGate);
   
    void clear();

  private:

    uint32_t         m_trig_n;
    std::vector<std::string> *m_chain;
    std::vector<int> *m_isPassed;
    std::vector<int> *m_nTracks;
  
    std::vector<std::vector<int> > *m_typeVec;
    std::vector<std::vector<float> > *m_ptVec;
    std::vector<std::vector<float> > *m_etaVec;
    std::vector<std::vector<float> > *m_phiVec;
    std::vector<std::vector<int> > *m_qVec;
    std::vector<std::vector<int> > *m_l1RoiWordVec;
    
    ToolHandle<Trig::TrigDecisionTool> m_tdt;

    std::string m_pattern;    
    mutable MsgStream msg;
};

#endif  // L1TGCNtuple_trigInfo_h
/* eof */
