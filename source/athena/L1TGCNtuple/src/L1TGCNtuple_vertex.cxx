#include "GaudiKernel/MsgStream.h"
#include "StoreGate/StoreGate.h"
#include "AthenaKernel/errorcheck.h"

#include "xAODTracking/VertexContainer.h"
#include "TrkEventPrimitives/ParamDefs.h"


#include "TTree.h"

#include "src/L1TGCNtuple_vertex.h"

/*--------------------------------------------------------------------*
 * Constructors and Destructor
 *--------------------------------------------------------------------*/

L1TGCNtuple_vertex::L1TGCNtuple_vertex(IMessageSvc* svc,
                                 const std::string& name)
  : m_vxp_n(0),
    m_vxp_x(new std::vector<float>),
    m_vxp_y(new std::vector<float>),
    m_vxp_z(new std::vector<float>),
    m_vxp_cov_x(new std::vector<float>),
    m_vxp_cov_y(new std::vector<float>),
    m_vxp_cov_z(new std::vector<float>),
    m_vxp_cov_xy(new std::vector<float>),
    m_vxp_cov_xz(new std::vector<float>),
    m_vxp_cov_yz(new std::vector<float>),
    m_vxp_chi2(new std::vector<float>),
    m_vxp_ndof(new std::vector<int>),
    m_vxp_nTracks(new std::vector<int>),
    m_vxp_type(new std::vector<int>),

    msg(svc, name) {
}

L1TGCNtuple_vertex::~L1TGCNtuple_vertex() {

  delete m_vxp_x;
  delete m_vxp_y;
  delete m_vxp_z;
  delete m_vxp_cov_x;
  delete m_vxp_cov_y;
  delete m_vxp_cov_z;
  delete m_vxp_cov_xy;
  delete m_vxp_cov_xz;
  delete m_vxp_cov_yz;
  delete m_vxp_chi2;
  delete m_vxp_ndof;
  delete m_vxp_nTracks;
  delete m_vxp_type;

}

/*--------------------------------------------------------------------*
 * book ntuples
 *--------------------------------------------------------------------*/
StatusCode
L1TGCNtuple_vertex::book(TTree* ttree) {
 
  ttree->Branch("vxp_n", &m_vxp_n, "vxp_n/I");
  ttree->Branch("vxp_x", &m_vxp_x);
  ttree->Branch("vxp_y", &m_vxp_y);
  ttree->Branch("vxp_z", &m_vxp_z);
  ttree->Branch("vxp_cov_x", &m_vxp_cov_x);
  ttree->Branch("vxp_cov_y", &m_vxp_cov_y);
  ttree->Branch("vxp_cov_z", &m_vxp_cov_z);
  ttree->Branch("vxp_cov_xy", &m_vxp_cov_xy);
  ttree->Branch("vxp_cov_xz", &m_vxp_cov_xz);
  ttree->Branch("vxp_cov_yz", &m_vxp_cov_yz);
  ttree->Branch("vxp_chi2", &m_vxp_chi2);
  ttree->Branch("vxp_ndof", &m_vxp_ndof);
  ttree->Branch("vxp_nTracks", &m_vxp_nTracks);
  ttree->Branch("vxp_type", &m_vxp_type);
  
  return StatusCode::SUCCESS;
}


/*--------------------------------------------------------------------*
 * fill ntuples
 *--------------------------------------------------------------------*/

StatusCode
L1TGCNtuple_vertex::fill(ServiceHandle<StoreGateSvc>& storeGateSvc) {

  const xAOD::VertexContainer* container = 0;
  StatusCode sc = storeGateSvc->retrieve(container, "PrimaryVertices");

  if (sc.isFailure()) return StatusCode::SUCCESS;
  
  m_vxp_n = container->size();

  xAOD::VertexContainer::const_iterator vxp_it;

  for (vxp_it = container->begin(); vxp_it != container->end(); vxp_it++) {

    const xAOD::Vertex* vxp = *vxp_it;

    m_vxp_x->push_back(vxp->x());
    m_vxp_y->push_back(vxp->y());
    m_vxp_z->push_back(vxp->z());
    m_vxp_cov_x->push_back(vxp->covariancePosition()(Trk::x, Trk::x));
    m_vxp_cov_y->push_back(vxp->covariancePosition()(Trk::y, Trk::y));
    m_vxp_cov_z->push_back(vxp->covariancePosition()(Trk::z, Trk::z));
    m_vxp_cov_xy->push_back(vxp->covariancePosition()(Trk::x, Trk::y));
    m_vxp_cov_xz->push_back(vxp->covariancePosition()(Trk::z, Trk::z));
    m_vxp_cov_yz->push_back(vxp->covariancePosition()(Trk::y, Trk::z));
    m_vxp_chi2->push_back(vxp->chiSquared());
    m_vxp_ndof->push_back(vxp->numberDoF());
    m_vxp_nTracks->push_back(vxp->nTrackParticles());
    m_vxp_type->push_back(vxp->vertexType());
  }

  return StatusCode::SUCCESS;
}

void
L1TGCNtuple_vertex::clear() {
  
  m_vxp_n = 0;

  m_vxp_x->clear();
  m_vxp_y->clear();
  m_vxp_z->clear();
  m_vxp_cov_x->clear();
  m_vxp_cov_y->clear();
  m_vxp_cov_z->clear();
  m_vxp_cov_xy->clear();
  m_vxp_cov_xz->clear();
  m_vxp_cov_yz->clear();
  m_vxp_chi2->clear();
  m_vxp_ndof->clear();
  m_vxp_nTracks->clear();
  m_vxp_type->clear();  

  return;
}

/* eof */
