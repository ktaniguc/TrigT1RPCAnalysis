## Twiki
- Detailed information available: https://twiki.cern.ch/twiki/bin/view/Main/L1TGCNtuple

## Tags
- L1TGCNtuple-00-01-03: finale version of Run2 

## How to run L1TGCNtupe
### Checkout and compile
- Setup athena
```
$ setupATLAS
$ mkdir build source run
$ asetup Athena,XX.XX.XX --testarea=$PWD/source
```  
- checkout and compile   
```
$ lsetup git
$ git clone ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/L1TGCNtuple.git source/L1TGCNtuple
$ source  source/L1TGCNtuple/run/compile.sh
```

### Run local job
- Edit jobOption file and run
```
$ cp source/L1TGCNtuple/share/runL1TGCNtuple.py run/
$ cd run/
edit runL1TGCNtuple.py (events, inputs and output)
- $ athena.py runL1TGCNtuple.py
```

### Run grid job
- Edit submit script and run
```
$ cp source/L1TGCNtuple/share/runL1TGCNtuple.py run/
$ cp source/L1TGCNtuple/share/submit.py run/
$ cd run/
edit submit.py
$ localSetupPandaClient
$ python submit.py
```

## Directory structure
```
   <cwd>
     |
     +- run: run script at local
     |
     +- grid: run script on grid 
     |
     +- L1TGCNtuple:  header files
     |
     +- src: source files
     |
     +- root: sample macros for TGC analysis
```
## Advanced
- to make new class to fill variables 
    - edit L1TGCNtuple/L1TGCNtuple.h
    - define L1TGCNtuple_xxx class
    - edit src/L1TGCNtuple.cxx
        - add #include "src/L1TGCNtuple_xxx.h"
        - modify constructor and destructor
        - add book/clear/fill methods as in L1TGCNtuple_event.h
  
- to run sample macros in "root" directory
    - edit root/makeChain.h
```
      $ cd root
      $ root -b -l process.cpp++ 
```  

