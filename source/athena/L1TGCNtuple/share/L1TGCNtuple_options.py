#-----------------------------------------------------------------------------
# Athena imports
#-----------------------------------------------------------------------------
from AthenaCommon.GlobalFlags import globalflags as gf
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
from AthenaCommon.AppMgr import ToolSvc
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

#-----------------------------------------------------------------------------
# Algorithms
#-----------------------------------------------------------------------------
# TriggerDecisionTool
from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
cfg = TriggerConfigGetter("ReadPool")

# TGCcablingServerSvc
from MuonCablingServers.MuonCablingServersConf import TGCcablingServerSvc
ServiceMgr += TGCcablingServerSvc()
theApp.CreateSvc += ["TGCcablingServerSvc"]
ServiceMgr.TGCcablingServerSvc.Atlas = True
ServiceMgr.TGCcablingServerSvc.forcedUse = True
ServiceMgr.TGCcablingServerSvc.useMuonTGC_CablingSvc = True
from TGC_CondCabling.TGC_CondCablingConf import TGCCablingDbTool
ToolSvc += TGCCablingDbTool()
from IOVDbSvc.CondDB import conddb
conddb.addFolderSplitMC('TGC','/TGC/CABLING/MAP_SCHEMA','/TGC/CABLING/MAP_SCHEMA')
import MuonCnvExample.MuonCablingConfig

# RPCcablingServerSvc
from MuonCablingServers.MuonCablingServersConf import RPCcablingServerSvc
ServiceMgr += RPCcablingServerSvc()
theApp.CreateSvc += [ "RPCcablingServerSvc" ]
ServiceMgr.RPCcablingServerSvc.Atlas = True
ServiceMgr.RPCcablingServerSvc.forcedUse = True
ServiceMgr.RPCcablingServerSvc.useMuonRPC_CablingSvc = True
#from RPCcablingSim.RPCcablingSimConf import RPCcablingSimSvc ??
from MuonRPC_Cabling.MuonRPC_CablingConf import MuonRPC_CablingSvc
import MuonRPC_Cabling.MuonRPC_CablingConfig
ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool = True
conddb.addFolderSplitMC("RPC","/RPC/CABLING/MAP_SCHEMA"     , "/RPC/CABLING/MAP_SCHEMA")
conddb.addFolderSplitMC("RPC","/RPC/CABLING/MAP_SCHEMA_CORR", "/RPC/CABLING/MAP_SCHEMA_CORR")
conddb.addFolderSplitMC("RPC","/RPC/TRIGGER/CM_THR_ETA", "/RPC/TRIGGER/CM_THR_ETA")
conddb.addFolderSplitMC("RPC","/RPC/TRIGGER/CM_THR_PHI", "/RPC/TRIGGER/CM_THR_PHI")
from RPC_CondCabling.RPC_CondCablingConf import RPCCablingDbTool
ToolSvc += RPCCablingDbTool()
ToolSvc.RPCCablingDbTool.MapConfigurationFolder = "/RPC/CABLING/MAP_SCHEMA"
ToolSvc.RPCCablingDbTool.MapCorrectionFolder = "/RPC/CABLING/MAP_SCHEMA_CORR"

# TrigEffJpsiTools
from TrigEffJpsiTools.TrigEffJpsiToolsConf import TrigEffJpsiTools
ToolSvc += TrigEffJpsiTools("TrigEffJpsiTools")

# TrigMuonCoinHierarchy
from TrigMuonCoinHierarchy.TrigMuonCoinHierarchyConf import Trigger__MuonRoiChainFindTool
MuonRoiChainFindTool = Trigger__MuonRoiChainFindTool("Trigger::MuonRoiChainFindTool")
MuonRoiChainFindTool.skipRoiThresholdComparison = True
ToolSvc += MuonRoiChainFindTool

# MuonSelectionTool
from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
MuonSelectionTool = CP__MuonSelectionTool("MuonSelectionTool")
MuonSelectionTool.MaxEta = 2.5
ToolSvc += MuonSelectionTool

# Event Filter
from L1TGCNtuple.L1TGCNtupleConf import L1TgcSkim
skim = L1TgcSkim()
skim.DumpAll = True    # True: all events dumped, False: skim

# events are skimmed with '(PassThroughTrigPattern) || (TrigPattern && (Tandp || Muons))'
skim.PassThroughTrigPattern = '(HLT_noalg_L1[0-9]*MU.*)' # regular expression for triggers (e.g. HLT_mu.*)
skim.TrigPattern = '(HLT_[0-9]*mu.*)|(HLT_noalg_L1MU.*)|(HLT_noalg_L1LowLumi)|(L1_[0-9]*MU.*)' # regular expression for triggers (e.g. HLT_mu.*)
skim.Tandp_tag_pt = 25000 # pT threshold (MeV) of tag muons
skim.Tandp_tag_type = 0   # type of tag muons
skim.Tandp_mass = 55000   # mass threshold (MeV) of tag ans probe system
skim.Muons_n = 1      # number of required muons
skim.Muons_type = -1      # type of muons (-1 for any)
skim.Muons_pt = 5000      # pT threshold (MeV) of muons
skim.Muons_prescale = 10       # prescale

ToolSvc += skim

# L1TGCNtuple
from L1TGCNtuple.L1TGCNtupleConf import L1TGCNtuple

ntuple = L1TGCNtuple('L1TGCNtuple', OutputLevel = INFO)

if gf.DataSource() == 'geant4':
    ntuple.isData = False

ntuple.isESD = rec.readESD()
ntuple.isAOD = rec.readAOD()
ntuple.isCosmic = False
ntuple.doTileCell = True ## TileCell
ntuple.doNSW = False
ntuple.TMDBConstants = "TMDB_ADC_to_MeV_20171127.txt"
job += ntuple

print job
#-----------------------------------------------------------------------------
