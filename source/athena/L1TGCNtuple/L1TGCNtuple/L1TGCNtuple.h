#ifndef L1TGCNTUPLE_H 
#define L1TGCNTUPLE_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

class ISvcLocator;
class ITHistSvc;
class TTree;

class L1TGCNtuple_event;
class L1TGCNtuple_vertex;
class L1TGCNtuple_tgcPrd;
class L1TGCNtuple_rpcPrd;
class L1TGCNtuple_mdtPrd;
class L1TGCNtuple_sTgcRdo;
class L1TGCNtuple_coin;
class L1TGCNtuple_murcv;
class L1TGCNtuple_tileCell;
class L1TGCNtuple_roi;
class L1TGCNtuple_muon;
class L1TGCNtuple_museg;
class L1TGCNtuple_trigInfo;
class L1TGCNtuple_truth;
class L1TGCNtuple_trkRecord;
class L1TGCNtuple_muctpi;
class L1TGCNtuple_hierarchy;
class L1TGCNtuple_ext;

class L1TgcSkim;

class L1TGCNtuple : public AthAlgorithm {
 
  public:
    L1TGCNtuple(const std::string& name, 
                ISvcLocator* pSvcLocator);
    ~L1TGCNtuple();

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();
    void clear();

   private:
    ServiceHandle<ITHistSvc> m_thistSvc;
    TTree*                   m_ttree;
    std::string              m_message;
    bool                     m_isData;
    bool                     m_isCosmic;
    bool                     m_isESD; 
    bool                     m_isAOD; 
    bool                     m_doTileCell; 
    bool                     m_doNSW; 
    std::string              m_TMDBConstants;

    L1TGCNtuple_event* m_ntupleEvent;
    L1TGCNtuple_vertex* m_ntupleVertex;
    L1TGCNtuple_tgcPrd*   m_ntupleTgcPrd;
    L1TGCNtuple_rpcPrd*   m_ntupleRpcPrd;
    L1TGCNtuple_mdtPrd*   m_ntupleMdtPrd;
    L1TGCNtuple_sTgcRdo*  m_ntupleSTgcRdo;
    L1TGCNtuple_coin*  m_ntupleCoin;
//    L1TGCNtuple_murcv* m_ntupleMurcv;
    L1TGCNtuple_tileCell* m_ntupleTileCell;
    L1TGCNtuple_roi*   m_ntupleRoi;
    L1TGCNtuple_muon*  m_ntupleMuon;
    L1TGCNtuple_museg*  m_ntupleMuseg;
    L1TGCNtuple_trigInfo*  m_ntupleTrigInfo;
    L1TGCNtuple_truth*  m_ntupleTruth;
    L1TGCNtuple_trkRecord*  m_ntupleTrkRecord;
    L1TGCNtuple_muctpi* m_ntupleMuctpi;
    L1TGCNtuple_hierarchy* m_ntupleHierarchy;
//    L1TGCNtuple_ext* m_ntupleExtBias;
//    L1TGCNtuple_ext* m_ntupleExtUnbias;

    ToolHandle<L1TgcSkim> m_l1TgcSkim;
};

#endif // L1TGCNTUPLE_H
