
# parameters
scope   = 'group.det-muon'
filters = '*data18*L1TGCNtuple*'

# main
import datetime
from rucio.client import Client
client   = Client()

containers  = client.list_dids(scope=scope, 
                               filters={'name':filters}, 
                               type='container')

outputs    = {}
totalbytes = 0

for container in containers:
    contents = client.list_content(scope=scope, 
                                   name=container)
    print 'inf> process %s' % container
 
    bytes = 0
    for content in contents:
        meta = client.get_metadata(scope=scope,
                                   name=content['name'])
        if meta['bytes']:
            bytes += int(meta['bytes'])
            totalbytes += int(meta['bytes'])
  
    outputs[container] = bytes

outfile = open('availability.txt', 'w')
outfile.write('Last update: %s\n' % datetime.date.today())
outfile.write('---------------------------------------------\n')
outfile.write('<size> <Container name>\n')
outfile.write('---------------------------------------------\n')

for key, value in sorted(outputs.items(),key=lambda x:x[0]):
    if value != 0:
        size = int(value/1024/1024/1024)
        outfile.write('%s[GB] %s/\n' % ('{0:4d}'.format(size), key))

totalsize = totalbytes/1024/1024/1024
outfile.write('Total %s[GB]' % '{0:4d}'.format(totalsize))
outfile.close()
