import os

user    = 'group.det-muon'
version = '00-01-02'
athena  = '21.0.69'
retry   = '1'

listfile = open('MCP.list.tmp')
ESDs = listfile.readlines() 
listfile.close()

for ESD in ESDs:
    ESD = ESD.rstrip()
    command =  'pathena runL1TGCNtuple.py'
    #command += ' --athenaTag=%s' % athena
    #command += ' --nGBPerJob=MAX'
    command += ' --nFilesPerJob=1'
    command += ' --forceStaged'
    command += ' --inDS=%s' % ESD
    
    output = ESD.replace('DESDM_MCP', 'NTUP_MCP.' + retry)
    output = output.replace('DESDM_ZMUMU', 'NTUP_ZMUMU.' + retry)
    output = output.replace('DESDM_TILEMU', 'NTUP_TILEMU.' + retry)
    output = output.replace('ESD', 'NTUP.' + retry)
    
    command += ' --outDS=%s.%s.%s' % (user, output, version)

    if 'group.det-muon' in user:
        command += ' --official --voms atlas:/atlas/det-muon/Role=production'
        #command += ' --destSE TOKYO-LCG2_DET-MUON'
        command += ' --destSE CERN-PROD_DET-MUON'
    
    print command
    os.system(command)
