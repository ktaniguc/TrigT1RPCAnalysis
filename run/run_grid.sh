#!/bin/sh
DATE=$(date '+%Y%m%d%H%M')

source $TestArea/../build/*/setup.sh
LIST=$1
PRE_EXEC='"all:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODFULL\");"'
NFILES_PER_JOB='5'
NEVENTS_PER_FILE='1000'
NEVENTS_PER_JOB='5000'
lowMinDS='mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.simul.HITS.e4981_s3087_s3111/'
highMinDS='mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.simul.HITS.e4981_s3087_s3111/'
DESTSE='TOKYO-LCG2_LOCALGROUPDISK'
EXCLUDEDSITE=BNL,ANALY_BNL_LONG,ANALY_BNL_SHORT,ANALY_CERN,ANALY_BU_ATLAS_Tier2_SL6,ANALY_CONNECT,ANALY_OU_OCHEP_SWT2,ANALY_AUSTRALIA,ANALY_SLAC,INFN,ANALY_RHUL_SL6,ANALY_TAIWAN,ANALY_TOKYO_ARC
MAXEVENTS=-1

######################
#mc16_13TeV.300201.Pythia8BPhotospp_A14_CTEQ6L1_bb_Jpsimu4mu4.simul.HITS.e4397_s3126
#mc16_13TeV.300203.Pythia8BPhotospp_A14_CTEQ6L1_bb_Jpsimu3p5mu3p5.simul.HITS.e4889_a875
######################
GEOMETRY='ATLAS-R2-2016-01-00-01'
CONDITION='OFLCOND-MC16-SDR-14'

######################
#mc16_13TeV.300201.Pythia8BPhotospp_A14_CTEQ6L1_bb_Jpsimu4mu4.simul.HITS.e4397_s2997
######################
#GEOMETRY='ATLAS-R2-2016-00-01-00'
#CONDITION='OFLCOND-MC16-SDR-09-01'

for INPUT_HITS in `cat $LIST`;
do
  OUTPUT=$INPUT_HITS
  OUTPUT=`echo ${OUTPUT} | sed -e "s:mc16_13TeV:user.ktaniguc:g"`
  OUTPUT=${OUTPUT/\//}_$2 
  echo ""
  echo "INPUT_RDO =    "$INPUT_HITS
  echo "OUTPUT_AOD =   "$OUTPUT
  echo ""

  SUBMIT_AREA="/gpfs/fs7001/ktaniguc/grid_L2SA4Close-by/$INPUT_HITS/$DATE"
  mkdir -p $SUBMIT_AREA
  cp *.sh $SUBMIT_AREA
  cp -r *.py $SUBMIT_AREA
  cd $SUBMIT_AREA

  echo ${0} 2>&1 | tee -a jediTaskID.info
  echo ""

  pathena \
  --inDS=${INPUT_HITS} \
  --outDS=${OUTPUT} \
  --nEventsPerFile=${NEVENTS_PER_FILE} \
  --nEventsPerJob=${NEVENTS_PER_JOB} \
  --nFilesPerJob=${NFILES_PER_JOB} \
  --excludedSite=${EXCLUDEDSITE} \
  --maxCpuCount=86400 \
  --mergeOutput \
  --destSE=${DESTSE} \
  --trf="Reco_tf.py --autoConfiguration everything --geometryVersion ${GEOMETRY} --conditionsTag ${CONDITION} --maxEvents ${MAXEVENTS} --jobNumber %RNDM:0 --outputAODFile %OUT.AOD.pool.root --inputRDOFile %IN --postInclude outputLevelConfig.py RecJobTransforms/UseFrontier.py --preExec ${PRE_EXEC}"

  echo "" | tee -a jediTaskID.info
  echo "LOG: " | tee -a jediTaskID.info

  eval $COMMAND 2>&1 | tee -a jediTaskID.info

  JEDITASKID=$(sed -n '/new jediTaskID=/s/INFO : succeeded. new jediTaskID=//p' jediTaskID.info)
  echo ""
  echo ""
  echo "JEDITASKID: ${JEDITASKID}" 2>&1 | tee -a jediTaskID.info
  echo "JEDI TASK URL: https://bigpanda.cern.ch/task/${JEDITASKID}/" 2>&1 | tee -a jediTaskID.info
  echo ""
  echo ""

  echo "INFO: Create jediTaskID.info"
  echo ""

  cd -
done
