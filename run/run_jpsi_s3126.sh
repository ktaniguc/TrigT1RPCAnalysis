#!/bin/sh

DATE=$(date '+%Y%m%d%H%M')
dateMMDD=$(date '+%m%d')
bque=1d
source $TestArea/../build/*/setup.sh
INPUT_LIST="./lists/Jpsimu4mu4_s3126.list"
#INPUT_LIST="./lists/Jpsi.300901.full.list"
#INPUT_LIST="./lists/Jpsi.300901.1-200.list"
GEOMETRY="ATLAS-R2-2016-01-00-01"
CONDITION="OFLCOND-MC16-SDR-14"

if [ -f $INPUT_LIST ] ; then
  for INPUT_FILE in `cat $INPUT_LIST`
  do
    OLD_DIR=$PWD
    echo "INPUT_FILE = "$INPUT_FILE
    TMP_DIR=`echo "$INPUT_FILE" | sed -e "s:/:_:g"`
    SUB_TAR="/gpfs/fs7001/ktaniguc/outputfile/L2SA4Close-By_newAlg/Jpsimu4mu4/separateMDTs_ismore_woddRPC/$DATE/$TMP_DIR/processing"
    mkdir -p $SUB_TAR
    cp ./*.sh $SUB_TAR
    cd $SUB_TAR
    COMMAND_FORCLEAR="mv AOD* ../ && cd ../ && rm -r ./processing"
    echo $COMMAND_FORCLEAR > clear.sh
    bsub -q ${bque} -o log.out "./HITS2AOD.sh ${INPUT_FILE} ${GEOMETRY} ${CONDITION} && source clear.sh"
    cd $OLD_DIR
  done
fi
